# Formations développeur, axe « Développer en Java »

## Formation « Initiation à l'algorithmique avec Java »

### Prérequis

Bases d'informatique. Avoir déjà fait, même dans le cadre d'une formation initiale, un peu d'un langage de programmation.

### But

Savoir résoudre un problème simple en Java.

Connaissance de la syntaxe structurée de Java.

### Contenu
1. Qu'est-ce que l'algorithmique&nbsp;?
    **algorithme**, **programme**, **application**, **langage**, **interprété**, **compilé**, **variable**, **affectation**, **condition logique**, **structure de contrôle**, **type de données**, **opérateur**, **fonction**.
2. Premier programme en Java
    **programme exécutable**, **exécution sous éclipse**, **exécution sous IntelliJ**, **fonction main**, **fonction System.out.println**
3. Variables et types de données
    **variable**, **affectation**
    **boolean**, **char**, **byte**, **short**, **int**, **long**, **float**, **double**
   **tableaux**, **String**, **collections**
4. Opérateurs
    **opérateur logique**, **comparaison**, **affectation**, **incrément/décrément**, **cast**
5. Structures de contrôle
    **`if ... else ...`**, **`switch`**, **`for (...) ...`**, **`while (...) ...`**, **`do ... while (...)`**, **`break`**, **`continue`**, **`continue`**
6. Commentaires
    **commentaire sur une ligne**, **commentaire multiligne**, **javadoc**
7. Fonctions
    **`fonction`**, **`fonction statique`**
8. Coder proprement
    **factoriser**, **convention de nommage**, **return**, **complexité**
    **javadoc**, **annotations javadoc**, **utilisation**

## Module « Initiation à la Programmation orientée objet avec Java »

### Prérequis

Connaissances Java de base.

Idéalement : module « Initiation à l'algorithmique avec Java ».

### But

Architecturer un programme Java en modules, packages et classes.

Comprendre la modélisation objet et l'implémenter dans les cas simples (sans généricité, sans *design patterns*).

### Contenu

1. Paradigmes de programmation
    *(justification, approche historique)*
    **programmation assembleur**, **programmation impérative**, **programmation structurée**, **programmation objet**, **programmation fonctionnelle**
2. Principes de la POO
    _(présentation rapide, les points importants en Java sont précisés dans les sections qui suivent)_
    **objet**, **donnée/attribut**, **comportement/méthode**, **effet de bord**, **type**, **interface**
    **classe**, **prototype**
    **dynamic dispatch**, **envoi de messages**
    **encapsulation**, **polymorphisme**
    **composition**, **héritage**, **délégation**
    **clonage**, **mutation**, **constructeur**
3. Modélisation avec le diagramme de classes UML
    **modélisation**, **UML**, **diagramme de classes**, **diagrammes UML**
4. Classes et objets en Java
    **nommage**, **déclaration d'attribut**, **déclaration de méthode**, **constructeur**, **constructeur par défaut**, **accesseur**, **mutateur**
    **instanciation `new ...`**, **appel de méthode**
    **déclaration vs instanciation**, **null**, **NullPointerException**
5. Héritage
    **classe abstraite**, **méthode abstraite**, **spécialisation/classe concrète**, **implémentation**
6. Notion de contrat&nbsp;: l'interface
    **interface**, **implémentation**, **utilisation**
7. Structure du programme
    **nommage**, **module**, **package**, **package par défaut**, **import de package**, **import de classe**
8. Visibilité et durée de vie des classes, méthodes et attributs
    **public**, **package**, **protected**, **private**
    **static**
    **bloc static**
8. Classes enveloppantes
    **boxing**, **Boolean**, **Character**, **Byte**, **Short**, **Integer**, **Long**, **Float**, **Double**
    **autoboxing**
    **NullPointerException**
9. Les packages et classes et interfaces les plus importantes
    **java.lang**
    **java.util**
    **java.text**
    **Object**
    **Math**, **Random**
    **System**, **Thread**
    **Scanner**
    **collections**
    **chemins**, **fichiers**
    **Comparable**, **Closeable**
    **Reader**, **Writer**
    **Exceptions**, **throw**, **catch**, **finally**, **try-with-resources**
## Formation « Algorithmique avancée avec Java »

### Prérequis

Formation « Introduction à l'algorithmique avec Java ».

Formation « Introduction à la programmation orientée objet avec Java ».

### But

Bien choisir et utiliser les fonctionnalités avancées de Java.

Comprendre les aspects importants du compilateur Java.

Éviter les bugs.

Maîtriser la complexité du *workflow*.

### Contenu

1. Notion de complexité algorithmique
    *(nécessaire au début)*
    **temps d'exécution**, **notation de Landau**
2. Retour sur les collections
    *(quel choix de collection)*
    **Collection**, **Map**
    **List**, **Set**, **Queue**, **Dequeue**
    **ArrayList**, **HashSet**, **HashMap**
    **accès**, **insertion**
    **`Iterator<E>`**
    **Arrays**, **Collections**
3. Programmation fonctionnelle
    **interface fonctionnelle**, **`@FunctionalInterface`**, **default**
    **lambda**
    **`Consumer`**, **`Predicate`**, **`Function`**, **`Supplier`**
    **`BiConsumer`**, **`BiPredicate`**, **`BiFunction`**, **`BiSupplier`**, **`UnaryOperator`**, **`BinaryOperator`**, ...
    **référence de méthode**
4. Les `Stream<T>`
    **`stream`**, **`forEach`**, **`map`**, **`collect`**, **`reduce`**, **`filter`**, **`findFirst`**, **`findAny`**, ...
    **IntStream**
5. Compilateur Java
    **compilateur**, **interpréteur**
    **étapes de la compilation**
    *(principales sources d'erreurs liées à une mauvaise connaissance du compilateur)*
6. JVM
    **class loader**, **Byte Code Verifier**, **Execution Engine**
    **JVM**, **method area**, **heap**, **stack**, **register**, ...
    **garbage collector**, **mark**, **sweep**, **choix du GC**
    *(principales sources d'erreurs liées à une mauvaise connaissance de la JVM)*
7. Gestion des exceptions
    **Exception**, **RuntimeException**
    **try**, **catch**, **finally**
    **intégration dans le workflow**
8. Déboguage
    **debug mode**, **breakpoints**, **vue « expressions »**, **step into/over**, **step filters**
    **exceptions**, **stacktrace**, **logging**
9. Logging
    **log4j**, **logger**, **appender**
10. Tests
    *(présentation rapide, à approfondir par soi-même ou dans une autre formation)*
    **tests unitaires**
    **tests fonctionnels**
11. Introduction aux expressions régulières
    **syntaxe**
    **`String#matches`**
    **Pattern**, **Matcher**, ...

## Formation « Environnement du développeur Java, du compilateur au CI/CD »

### Prérequis

Connaissance basique de l'univers informatique.

### But

Savoir ce qu'est un programme, batch ou IHM.

Savoir utiliser l'IDE Éclipse. *(et IntelliJ ?)*

Savoir se repérer dans l'écosystème des *frameworks* Java.

Avoir les jalons pour monter en compétence sur les étapes du projet, de la modélisation à l'intégration continue.

### Contenu

1. Introduction à la programmation
    **programme**, **compilation**, **bytecode**, **exécution**
2. Outils et guides de référence
    *(Outils de dév sur Gitlab)*
3. JRE et JDK
    **JRE**, **JDK**
    **exécution en ligne de commande**
4. Structure du programme Java
5. Découverte d'Eclipse
    **IDE**, **lowercase**, **uppercase**
    **structure du projet « Hello World »**, **build path**
    **structure du projet Maven** *(sans entrer dans le détail de Maven)*
    **vues**, **perspectives**
    **Eclipse MarketPlace**, **plugins**
6. Fonctionnalités avancées d'Eclipse
    **Templates**, **génération de code**, **refactor**
    **niveau du compilateur**, **formatteur de code**
    **deboguage**
    **navigation dans le code**, **call hierarchy**, **open declaration/implementation**
    **choix du compilateur**
    **paramétrage avancé** *(à définir et préciser)*
7. Introduction rapide à Maven
    **librarie**, **dependance**
    **archétype**, **pom**
    **livrable**
8. Applications Web
    **structure du projet web**
    **tomcat**
9. Applications multimodules
    **module**
    **gestion avec maven**
1. Frameworks importants
    *(présentation rapide, sans volonté de rentrer dans le détail)*
    **Spring**, **Spring MVC**, **Springboot**
    **Struts 2** *(il existe encore des applis sous Struts2 à l'Insee)*
    **log4j**, **slf4j**, ...
10. Versioning avec Gitlab
    *(présentation rapide)*
11. Livraison dans les différents univers
    **plateforme batch**, **plateforme IHM**
    **ftp**
    **applishare**
12. CI/CD
    *(présentation rapide)*

## Formation « POO avancée »

### Prérequis

Formation « Initiation à la Programmation orientée objet avec Java ».

Formation « Initiation à l'algorithmique avec Java ».

### But

Écrire des programmes avec une faible dette technique.

Savoir utiliser les fonctionnalités objet avancées de Java.

Structurer un programme de façon lisible et évolutive.

### Contenu

1. Annotations
    **cas d'utilisation**
2. Polymorphisme avancé
    **surcharge**, **surdéfinition**, **principe de substitution de Liskov**
3. Généricité
    **declaration**
    **utilisation**
    **compilation**
    **Producer extends, Consumer super**
4. Bad smells : signes qu'un programme est mal conçu
    *(introduction et motivation des sections suivantes)**
    **exemples**
5. Principes d'architecture des programmes
    **module**, **package**
6. Design pattern
    **DAO**, ...
5. Principes SOLID
    **Single Responsibility Principle**
    **Open-Closed Principle**
    **Liskov Substitution Principle**
    **Interface Segregation Principle**
    **Dependency Inversion Principle**

## Ressources
https://openjdk.java.net/groups/compiler/doc/compilation-overview/index.html
https://www.javatpoint.com/compiler-phases
https://gitlab.insee.fr/animation-developpement/outils-de-dev/-/wikis/Outils/_index
# Introduction à l'approche objet
<a aria-hidden="true" href="#oop-intro" class="anchor" id="oop-intro"></a>

## Description rapide

Le principe de l'approche objet est de découper l'application d'une façon conceptuellement aussi proche que possible de celle dont notre esprit conçoit les problèmes et leurs solutions (tout en restant compatible avec une implémentation sur un ordinateur).

> Dans l'approche objet, le programme est découpé en entités qui s'appellent les *objets*. Ces objets ont un état codé sous forme de variables, un comportement codé sous forme de fonctions et échangent entre eux en appelant leurs fonctions.

> Les variables d'un objet sont appelées **attributs**.

> Les fonctions d'un objet sont appelées **méthodes**, et on peut justifier ce nom par l'idée qu'un objet réalise une partie d'un contrat selon une méthode qui lui est propre.

Les langages qui implémentent la démarche objet permettent au développeur de définir des objets à l'aide de _templates_ de code comme dans cet exemple dans un langage algorithmique&nbsp;:

```
Human {
    has name: string
    has age: integer
    has dreams: list of Dream

    can speak: function(Context context) {
        % blah blah blah blah
    }

    can sleep: function(Context context) {
        % Zzzz zzzz zzzz zzzz
    }

    can laugh: function(Context context) {
        % Haaa haaa haaa haaa
    }
}

Dream {
    has owner: Human

    ...
}

Context {
    % So many things...
    ...
}
```

Dans cette approche, le développeur définit le **workflow** non pas en assignant des valeurs à des variables mais en définissant des objets, leur état initial et les interactions entre les objets sous forme d'appel de méthodes. C'est le compilateur qui fait le travail de transcription d'un **workflow** éclaté en objets vers un **workflow** structuré comme nous en avons l'habitude en algorithmique.

Ces &laquo;&nbsp;objets&nbsp;&raquo; sont un peu étranges, il peut être difficile de voir à quoi ils correspondent lorsque notre perspective est celle d'un découpage séquentiel des solutions.

## L'approche objet est analogique

Pourtant cette approche correspond parfaitement au monde dans lequel on vit&nbsp;: lorsque nous utilisons une cafetière électronique, nous n'avons pas besoin de connaître son fonctionnement interne. Nous savons qu'elle a un bouton qui permet de lancer un programme (par exemple, « café court »). Comme un objet (au sens de cette formation) la cafetière a&nbsp;:
- un état interne&nbsp;: la quantité d'eau, l'ordre qu'elle a reçu de l'utilisateur, _et cetera_&nbsp;;
- des méthodes (du comportement) cachées&nbsp;: par exemple si un tuyau est obstrué la cafetière le repère et un voyant d'erreur s'allume&nbsp;;
- des méthodes (du comportement) visibles&nbsp;: chaque bouton sur lequel l'utilisateur peut appuyer correspond à un comportement mis à sa disposition.

Une cafetière électronique remplit un **contrat**&nbsp;: faire du café. Pour cela elle exhibe une **interface**&nbsp;: l'ensemble des fonctionnalités offertes à l'utilisateur.

De plus un exemplaire de cafetière a un **modèle**, elle n'est jamais unique.

Enfin, un modèle se décline souvent en variantes qui partagent la plupart des fonctionnalités mais présentent des spécificités.

Le langage objet reprend cette distinction entre modèle et exemplaires en nommant le modèle « classe » et l'exemplaire « objet » ou « instance de classe ».

Il introduit par ailleurs une notion de hiérarchie entre les classes, nommée « héritage ». On pourrait dire par exemple que « cafetière » désigne un concept abstrait, défini par un contrat&nbsp;: faire du café. « cafetière italienne » désigne un concept plus précis, mais abstrait, tout comme « cafetière électronique à capsule ». Enfin, le modèle Vénusse 8 tasses de Bioletta, compatible avec les plaques à induction, sont un exemple concret de cafetière italienne. Tout comme le modèle Laetissia Delangui de Mehspresso désigne un modèle concret de cafetière électronique à capsules.

Le contrat évoqué précédemment est généralement appelé « interface » en programmation objet. Les concepts abstraits mais qui décrivent plus précisément le concept pourraient (nous verrons comment et pourquoi) correspondre à une notion de « classe abstraite »&nbsp;: un modèle qui fige certaines choses mais qui nécessite de faire des choix supplémentaires pour pouvoir être construit. Enfin, les cafetières nommées, construites et commercialisées correspondent à des « classes concrètes ». On pourrait ajouter (d'ailleurs, on va le faire) que le modèle avec la variante (par exemple « Laetissia Delangui B52 de Mehspresso », qui se distingue du modèle de base par des néons fluorescents sur le côté) correspond à une classe finale, qui ne peut pas avoir de sous-variantes.

## Les 4 grands principes objets

Cette approche a été théorisée de bien des manières. Plusieurs séries de principes la décrivent ou la caractérisent.

Mais il y a quatre principes qui sont reconnus comme fondateurs et caractéristiques de cette approche&nbsp;:

### Abstraction

C'est le plus important des quatre principes. L'utilisateur d'un objet n'a pas besoin de savoir comment l'objet fonctionne.

L'abstraction désigne le fait que l'objet n'expose à l'utilisateur que son interface, et cache les détails de son état et de son fonctionnement.

La cafetière à capsules possède des boutons qui forment sont interface, et cache son état et son fonctionnement interne sous sa coque.

L'objet expose des méthodes publiques et garde secrets, privés, son état et son fonctionnement.

### Héritage

C'est le moins important des quatres principes. Il désigne le fait qu'un modèle, une classe, peut avoir des variantes avec des spécificités propres.

Prenons l'exemple du modèle « Laetissia Delangui de Mehspresso », qui implémente tout ce qu'un buveur de capsules peut espérer. Ce modèle peut donner lieu à beaucoup de variantes, qui offriront les mêmes fonctionnalités et plus encore, comme le modèle « Laetissia Delangui B52 de Mehspresso ».

L'héritage fait référence à l'extension d'une classe (d'un modèle) par des fonctionnalités dans une autre classe (un autre modèle).

Il fait aussi référence au lien qui existe entre un modèle incomplet (une classe abstraite), comme par exemple « Cafetière italienne », et un modèle complet (une classe concrète) comme par exemple « Vénusse 8 tasses de Bioletta ».

Ce concept implique une hiérarchie, mais existe dans un but de factorisation, de mutualisation. Il n'est pas intrinsèquement construit dans les implémentations de l'approche objet, mais doit être permis par ces implémentations.

### Encapsulation

Ce principe fait référence à la manière dont sont organisés les objets.

Un objet est une structure qui comporte de la donnée et du comportement. Ces éléments sont intégrés à cet objet. La communication entre objets se fait par l'appel de méthodes publiques de ces objets.

Lorsqu'une méthode est appelée, l'objet gère le processus à sa manière.

Si je demande à Albert de me faire un café, on peut supposer que j'invoque sa méthode « me rendre service » avec un paramètre « du café ». Albert va choisir lui-même la façon dont il me rend se service&nbsp;: en allant au distributeur, en réchauffant un vieux café ou en utilisant une cafetière. Je n'ai pas accès à cela. Il va utiliser l'interface du distributeur et appuyer sur « café long » puis sur « +++ » pour le sucre. En faisant cela il invoque une méthode du distributeur et le distributeur gère la suite du processus à sa manière.

Aucun des trois acteurs de ce scénario n'a accès aux rouages internes des deux autres acteurs.

Une classe (un modèle) implémente l'encapsulation en gardant privés ses données et son fonctionnement, à l'exception de ce qui doit être manifestement rendu public aux utilisateurs.

### Polymorphisme

Ce principe fait référence à la capacité du code (d'une classe, d'un objet, d'une méthode) à fonctionner avec des paramètres appartenant à différents modèles. Plus spécifiquement, à la capacité du code objet à exhiber le même contrat, mais à offrir la possibilité, au moment de l'invocation, d'utiliser des implémentations différentes.

Albert est hautement polymorphe. Lorsque je lui ai redemandé du café, les gens du troisième étage avaient caché le distributeur. Il est allé chez Bettina qui lui a prêté sa cafetière Mehspresso.

## Les types

Ce que deux objets partagent d'un modèle est appelé « type ». Le type c'est le nom donné à ce que l'objet encapsule comme données et comme comportement.

La classe d'un objet est donc son type en Java. Une variable `int i` est de type `int` puisque `int` décrit tout ce qu'on peut attendre de `i` (qui n'est pas un objet).

## Les implémentations du paradigme objet

### Avec l'utilisation de la notion de fermeture

Nous l'avons vu, un lagage fonctionnel qui implémente la notion de fermeture (_closure_) permet d'implémenter l'approche objet. Cette approche est notamment utilisée en JavaScript avec le _module pattern_, qui correspond exactement à l'utilisation décrite en R.

```R
monObjet <- (
    function() {
        .data <- ...

        returned <- list()

        .uneMethodeCachee <- function() { ... }

        returned$uneMethode <- function() { ... }

        return(returned)
    }
)()
```

Les principes objets sont gérés ainsi&nbsp;:
1. Abstraction&nbsp;: seuls les éléments qui font partie du type renvoyé par la fonction invoquée sont visibles par l'utilisateur. Le reste des données et du comportement sont cachés. C'est ce qui fait la popularité du _module pattern_.
1. Encapsulation&nbsp; par le mécanisme de fermeture l'encapsulation de la donnée et du comportement est parfaite.
1. Héritage&nbsp;: l'héritage peut être réalisé ici par délégation.
1. Polymorphisme&nbsp;: comme python, R est faiblement typé. Donc, dès lors que deux objets offrent la même interface (les mêmes méthodes publiques), ils sont substituables dans un code qui accepte l'un. C'est le principe du _duck typing_.

### Avec les prototypes

Dans les paragraphes précédents nous avons décrits la notion de modèle en utilisant le mot « classe », qui précède l'instance.

Mais certains langages comme JavaScript sont organisés autrements&nbsp;: quand un objet est créé, il définit un type, qu'on appelle donc prototype.

Pour reprendre la métaphore de la cafetière, on peut imaginer qu'un italien a, il y a bien longtemps, dans les années 80, créé une cafetière unique au monde qu'il a brevetée et appelée « cafetière italienne ». Toute les cafetières italiennes sont créées à partir de ce modèle et des ses variantes.

Un prototype peut être modifié, ce qui donne naissance à un nouveau prototype. Dans cette approche&nbsp;:
1. L'héritage est géré par la capacité qu'à le développeur à enrichir un objet après l'avoir obtenu de son prototype.
2. L'encapsulation est inhérente à la notion de prototype.
3. Le polymorphisme découle de l'approche _duck typing_.
4. L'abstraction est généralement imparfaite.

### Avec les classes

C'est l'approche de Java, qui consiste à définir une classe comme modèle de l'objet et un mécanisme de construction de l'objet à partir de la classe.
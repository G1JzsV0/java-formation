# Introduction à la programmation objet avec Java

[[_TOC_]]

## Introduction

### Prérequis

Cette formation requiert des connaissances de base dans le langage Java, couverte par la formation &laquo;&nbsp;Introduction à l'algorithmique avec Java&nbsp;&raquo;. En particulier il est impératif de bien connaître la notion de fonction.

Du point de vue logiciel, les prérequis sont :

1. Eclipse, IntelliJ, ou un autre IDE installé et fonctionnel.
2. JDK 8 ou supérieur installé.
3. Pour suivre les exercices : client git.

### But

À l'issue de cette formation, vous connaîtrez&nbsp;:
1. La définition et l'intérêt des paradigmes de programmation impérative, structurée et objet.
2. Les bases du langage Java vous permettant de faire des programmes simples en Java, structurés avec l'approche objet.
3. Les notions de bases liées à l'approche objet.

### Initialisation

1. Créez un répertoire où vous voulez importer le projet qui contient les exercices.
2. Ouvrez un terminal comme Git Bash ou un équivalent (faites un clic droit à l'intérieur du répertoire et cliquez sur _Git bash here_).
3. Dans le terminal, tapez ou copiez (`[Shift] + [Insert]`) la ligne de code suivante&nbsp;: `git clone git@gitlab.com:G1JzsV0/java-formation-oop-beginners.git`.

Sous Eclipse&nbsp;:


Sous IntelliJ&nbsp;:

## Paradigmes de programmation

### Impératif vs déclaratif

Dans la formation précédente &laquo;&nbsp;Introduction à l'algorithmique avec Java&nbsp;&raquo;, nous avons parlé de la programmation assembleur comme étant la plus proche de la machine, et la plus fastidieuse à l'humain. Nous avons vu que même les problèmes les plus simples, comme &laquo;&nbsp;effectuer une action conditionnellement au résultat d'une comparaison&nbsp;&raquo;, faisait l'objet d'instructions trop nombreuses et trop illisibles pour permettre à l'humain de générer des programmes modernes (plusieurs millions de lignes de code) pérennes, maintenables, lisibles, évolutifs.

Une chose qui a pu vous frapper lors de cette formation, c'est le fait que l'assembleur est un langage très &laquo;&nbsp;autoritaire&nbsp;&raquo;, dans le sens où les seules instructions disponibles sont des ordres qui peuvent être effectués d'une seule manière&nbsp;:

- charger des valeurs dans des emplacements mémoire,
- faire des opérations arithmétiques,
- brancher à des emplacements du code (instruction `GOTO` ou `JUMP`).

Pourtant, si nous prenons n'importe quel langage que nous avons déjà pu manipuler un petit peu, nous voyons que nous ne savons pas comment fonctionnent les instructions que nous utilisons. Par exemple, en Java, la fonction `Math.pow(double, double)`, qui renvoie la valeur du premier opérande puissance la valeur du deuxième opérande (a<sup>b</sup>), pourrait être écrite de plusieurs manières&nbsp;:

- en utilisant exclusivement la formule exponentielle a<sup>b</sup> = e<sup>b _ln_ a</sup>,
- en gérant différemment les cas où le deuxième opérande est entier (exponentiation rapide),
- en gérant les différents cas d'exception ou en les déléguant aux méthodes appelées…

Peu nous importe que dans les faits, la fonction qui permet ce calcul fasse 150 lignes (c'est le cas) et nécessite l'usage de plus de 10 variables intermédiaires, nous faisons confiance à la fonction pour renvoyer le résultat souhaité sans nous soucier de la façon dont c'est fait.

Nous voyons donc apparaître une distinction entre des éléments (de langage de programmation) autoritatifs, qui imposent à l'algorithme la méthode de résolution d'un problème, et des éléments contractuels, qui imposent un résultat.

> On dit qu'un langage est **impératif** lorsque la méthode de résolution des problèmes est en plus grande partie déléguée à l'humain. Dans ce cas, le programme décrit la méthode de résolution des problèmes.

> On dit qu'un langage est **déclaratif** lorsque la méthode de résolution des problèmes est déléguée au compilateur ou à l'interpréteur. Dans ce cas, le programme ressemble à une description du résultat souhaité.

Dans les faits, il est très rare qu'un langage soit purement déclaratif ou impératif. On peut donner quelques exemples&nbsp;:

1. Le langage SQL est réputé déclaratif, puisque la plupart du temps l'humain se contente de décrire la donnée qu'il souhaite obtenir ou altérer. Un ordre simple de type `SELECT * FROM A INNER JOIN B ON A.id = B.id_a;` peut donner lieu à des exécutions très différentes suivant que les tables sont indexées, partitionnées, suivant les différents paramétrages de la base. La méthode utilisée par le SGBD est accessible grâce à l'instruction `EXPLAIN [ANALYZE] ...`, qui fait le bonheur des développeurs lorsqu'il faut optimiser.
2. Le langage assembleur est purement impératif. Le développeur fait tout le travail, les seules surprises à l'exécution sont dûes aux défaillances du développeur.
3. Toute fonction est par définition déclarative, puisqu'elle spécifie un contrat&nbsp;: les paramètres, le traitement qu'elle fait et ce qu'elle renvoie.
4. La plupart des langages modernes sont impératifs avec une part de déclaratif lié à l'aspect fonctionnel.

### Programmation structurée

Nous avons vu, dans la formation précédente &laquo;&nbsp;Introduction à l'algorithmique avec Java&nbsp;&raquo;, à quel point l'assembleur était fastidieux, illisible et statique (peu évolutif). Nous avons vu également en quoi les structures de contrôle permettaient d'alléger ce fardeau de complication.

Ces structures marquent l'introduction d'un nouveau paradigme, où le branchement (passage d'un endroit du code à un autre, matérialisé par une étiquette ou par un numéro de ligne) est interdit, et remplacé par l'utilisation de structures de contrôle (`if ...`, `for (...)`, `while ...`, _et cetera_).

> On dit qu'un langage est **structuré** si les structures de contrôles remplacent les branchements. Dans un tel langage plus précisément, le flot d'instruction est séquentiel, et structuré par des alternatives (portions de code s'exécutant sous certaines conditions) et des répétitions (portions de codes s'exécutant plusieurs fois).

Contrairement à son nom, le SQL n'est en fait pas un langage structuré (pas dans le sens évoqué dans cette section).

🧐 Java n'est pas complètement structuré, dans le sens où tous les branchements n'ont pas été complètement abolis. Pouvez-vous trouver des exemples&nbsp;?

<details>
<summary>Réponse</summary>

Les instructions `return`, `break` et `continue` sont des branchements à peine déguisés&nbsp;!
</details>

<!--
### Programmation procédurale

Toujours dans la formation précédente &laquo;&nbsp;Introduction à l'algorithmique avec Java&nbsp;&raquo;, nous avons vu à quel point la fonction permettait de structurer efficacement un programme. En réalité, la fonction implémente (réalise) la notion d'une portion de code exécutable de façon autonome, sans que le reste du système d'information soit modifié, et de façon déterministe (deux invocations de la même fonction avec les mêmes arguments produisent le même résultat).

Cette notion de fonction a donné naissance au paradigme procédural&nbsp;:

> On dit qu'un langage est **procédural** s'il permet le découpage du code en entités autonomes, qui fonctionnent sans effet de bord, et peuvent être appelées à n'importe quel endroit du programme.
-->

### Programmation fonctionnelle

Cette section peut être omise en première lecture.

Le paradigme fonctionnel a émergé très tôt, dès les années 1950, avec des langages comme LISP. Il est basé sur les notions décrites dans les sections qui suivent.

#### _First-class_ et _higher-order_

Les fonctions sont des &laquo;&nbsp;first-class citizens&nbsp;&raquo;, c'est-à-dire que les fonctions peuvent être passées en arguments d'autres fonctions, et être renvoyées par des fonctions.

#### Fonctions _pures_

Les fonctions n'ont pas d'état et ne changent pas l'état du système d'information par effet de bord (pas de changement des variables qui existent en dehors de la fonction).

#### Fonctionnement récursif plutôt qu'itératif

Les boucles `for` n'existent pas et sont remplacées par des appels récursifs.

#### Évaluation stricte (_eager_) ou non stricte (_lazy_)

Dans les langages non purement fonctionnels comme Java, l'appel à une fonction `myFunction(expr1, expr2)` se fait après évaluation de ses arguments `expr1` et `expr2`.

L'évaluation non stricte, présente par exemple dans R, permet de n'évaluer les arguments d'une fonction que si c'est strictement nécessaire, au moment où c'est nécessaire.

Les langages fonctionnels bien souvent proposent une évaluation paresseuse (au dernier moment) des arguments passés aux fonctions, mais ce n'est pas universel.

#### Transparence référentielle

Dans les langages fonctionnels purs, il doit être possible de remplacer toute invocation d'une fonction par sa valeur, sans que le comportement du programme n'en soit altéré. Cela exclue bien sûr, en particulier, les modifications par effet de bord à l'intérieur des fonctions.

#### Structures de données fonctionnelles

En paradigme fonctionnel, les structures de données sont idéalement immutables, de façon à limiter les problématiques d'accès concurrents. Ainsi, une fonction dont le but est de trier un tableau ne changera pas l'ordre des éléments du tableau mais en créera un nouveau, avec les éléments triés.

#### _Closures_

L'une des fonctionnalités importantes des langages fonctionnels est la capacité pour certaines fonctions d'embarquer une partie de l'environnement dans lequel elles sont exécutées, soit par valeur soit par référence. On appelle ce mécanisme la **fermeture** (_closure_ en anglais) et il est présent par exemple en R et en JavaScript.

#### _Monades_

Les monades sont des structures capables d'encapsuler des traitements d'une manière bien particulière.
<!-- TODO détailler ? raffiner ? -->

### Limites de la programmation structurée

Un projet comme Mozilla Firefox fait 21 millions de lignes en 2022, et c'est un exemple assez modeste au regard de certains autres… En regard, les capacités humaines sont très limitées&nbsp;:

1. Notre mémoire de travail nous permet de garder entre 5 et 7 objets (chiffres, mots, concepts) à l'esprit.
2. D'expérience, on voit bien qu'un fichier de code de 1 000 lignes est difficile à lire, naviguer, comprendre, modifier.

Il est donc crucial de structurer (au sens naïf) encore plus fortement, cohésivement les programmes, et en particulier de fonctionner au maximum avec les limitations de notre cerveau. Nous avons vu que le concept de fonction, à cet égard, est très pratique, puisqu'il permet d'abstraire une partie des détails du code pour fournir un service (ce que fait la fonction) duquel le développeur ou la développeuse n'a besoin de connaître que les entrants (arguments), les sortants (le retour éventuel) et les exceptions (comportement lorsque les entrants sont invalides).

Prenons l'exemple de la fonction puissance en Java&nbsp;: la signature est suffisamment explicite et sa [documentation](https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html#pow-double-double-) extrêmement précise permet au développeur de l'utiliser avec confiance dans tous les cas.

Abstraire les détails (calculer une puissance) derriere un nom (`Math.pow`) est très puissant mais ne suffit pas à rendre des projets de taille moderne (plusieurs milliers à millions, voire milliards de lignes) gérables par des humains. Instaurer des bonnes pratiques et des conventions (regrouper les fonctions selon une sémantique dans des fichiers) repose essentiellement sur la bonne volonté des humains…

Ce qui est vrai pour les fonctions (le comportement) du programme est aussi vrai pour son état (ses variables)&nbsp;: plus un système d'information est complexe et plus il est difficile d'organiser et maintenir les variables qui stockent son état à un moment donné.

Tout comme la structure de contrôle aidait le développeur en réduisant sa capacité à faire des branchements incontrôlables, l'approche objet prétend aider le développeur en réduisant sa capacité à utiliser de façon non structurée les fonctions ainsi que les variables.

### Un &laquo;&nbsp;nouveau&nbsp;&raquo; paradigme&nbsp;: la programmation orientée objet

La première sous-section peut être omise si vous ne connaissez aucun autre langage de programmation. Si c'est le cas, vous pouvez passer à la section [Introduction à l'approche objet](#oop-intro), moins technique.

#### Une approche basée sur la fermeture des fonctions en R

Dans un langage qui implémente la notion de fermeture, une fonction peut faire référence à une variable définie dans un contexte plus global.

```R
add <- function(x) {
    
    .x <- x
    return function(y) y + .x

}

add1 <- add(1)
add1(5) // returns 6
add1(0) // returns 1

add9 <- add(9)
add9(100) // returns 109
add9(4)  // returned 13
```

Ce fonctionnement est extrêmement pratique pour définir des opérateurs, comme on le voit. Mais il permet de faire bien plus que cela&nbsp;:

```R
person <- function(surname, name) {
    .surname <- surname
    .name <- name
    .height <- NULL
    
    ret1 <- list()
    
    ret1$getSurname <- function() .surname
    ret1$setSurname <- function(surname) { .surname <<- surname }
    ret1$getName <- function() .name
    ret1$setName <- function(name) { .name <<- name }
    ret1$getHeight <- function() .height
    ret1$setHeight <- function(name) { .height <<- height }
    
    return(ret1)
}
```

La fonction `person` renvoie une liste de fonctions dont chacune, par fermeture, embarque une variable de l'environnement parent. Les valeurs de ces variables subsistent en dehors de l'appel de la fonction `person`. Donc l'appel aux fonctions `setXXXX` modifient effectivement l'une des variables de leur environnement&nbsp;:

```R
me <- person("Stefan", "Liet")
me$getSurname() # Stefan
me$setSurname("Robert")
me$getSurname() # Robert
```

Il devient donc possible de regrouper dans une même fonction à la fois des données (des variables) et du comportement (des fonctions) relatives à une même entité sémantique. C'est très structurant pour le code, très lisible, et très sécurisant car les données et le code relatifs à une même entité y sont encapsulées. On peut raffiner le concept&nbsp;:

```R
Person <- (function() {
  
    .count <- 0
    
    ret0 <- list()
    
    ret0$new <- function(surname, name) {
        .count <<- .count + 1
        
        .surname <- surname
        .name <- name
        .height <- NULL
        
        ret1 <- list()
        
        ret1$getSurname <- function() .surname
        ret1$setSurname <- function(surname) { .surname <<- surname }
        ret1$getName <- function() .name
        ret1$setName <- function(name) { .name <<- name }
        ret1$getHeight <- function() .height
        ret1$setHeight <- function(name) { .height <<- height }
        
        return(ret1)
    }
    
    ret0$getCount <- function() .count
    
    return(ret0)
})()

me <- Person$new("Abel", "Belard")

Person$count() # 1

me$getSurname() # Abel
```

La fonction `new` joue ici un rôle particulier, qui est de construire des entités, on l'appellera donc **constructeur**.

Ce tour de passe-passe, en essence, c'est la naissance de la programmation objet.

#### Une approche basée sur la factorisation

Ayant suivi la formation &laquo;&nbsp;Introduction à l'algorithmique avec Java&nbsp;&raquo;, vous savez ce qu'est une fonction&nbsp;: une belle manière de factoriser du code, de façon paramétrable, grâce aux arguments qui peuvent lui être passés. Imaginons par exemple que souhaitiez créer un programme qui permet de gérer une bilbiothèque, ses livres, et ses utilisateurs.

Ce programme en mode console doit permettre aux bibliothécaires d'inscrire des clients, des livres, et de gérer les prêts, les retards, _et cetera_. On peut en imaginer une version naîve&nbsp;:

```Java
public class Bibliotheque {

    public static void main(String[] args) {
        int code = 0;
        while (code != 6) {
            System.out.println("Que voulez-vous faire&nbsp;?");
            System.out.println("1. Inscrire un nouveau lecteur.");
            System.out.println("2. Inscrire un nouveau livre.");
            System.out.println("3. Prêter un livre.");
            System.out.println("4. Retourner un livre.");
            System.out.println("5. Consulter les retards.");
            System.out.println("6. Quitter.");
            // du code pour récupérer le choix de l'utilisateur
            code = readChoix();
            switch (code) {
                case 1: inscrireNouveauLecteur();
                    break;
                case 2: inscrireNouveauLivre();
                    break;
                case 3: preterLivre();
                    break;
                case 4: retournerLivre();
                    break;
                case 5: inscrireNouveauLecteur();
                    break;
                case 6: quitter();
                    break;
            }
        }
    }
}
```

Chaque fonction code le fonctionnement que son nom décrit.

🧐 Cela semble simple, mais en réalité ce code est très problématique. Quelles sont les faiblesses de cette programmation&nbsp;?

<details>
<summary>Réponse</summary>

1. Si l'application s'enrichit de nouvelles fonctionnalités, le code de la classe `Bibliotheque` grossit, jusqu'à devenir ingérable. Pour le moment il comporte un petit nombre de fonctions car nous n'avons pas codé les détails, mais en réalité l'application telle qu'elle est décrite devrait faire déjà quelques centaines de lignes.
2. Certaines fonctionnalités sont très difficile à implémenter. Imaginons par exemple qu'on souhaite gérer aussi les bibliothécaires&nbsp;: c'est-à-dire qu'on souhaite pouvoir associer chaque action (inscrire un nouveau livre, un nouveau lecteur, prêter ou rendre un livre) au ou à la bibliothécaire qui l'a entreprise. Chaque fonction (`inscrireNouveauLecteur()`, `inscrireNouveauLivre()`, `preterLivre()`, `retournerLivre()` et `inscrireNouveauLecteur()`) est impactée et doit être modifiée.
</details>

De manière générale, nous voyons qu'un tel code peut vite devenir monstrueux, difficilement évolutif car chaque aspect de la logique métier est intriquée avec les autres. Par exemple, en ajoutant la traçabilité des actions des bibliothécaires, nous devons ajouter non seulement une authentification, ce qui peut être fait facilement, mais également modifier toutes les fonctions pour prendre en compte ce nouveau besoin.

Une première étape de simplification de ce code serait de regrouper entre elles les fonctions qui partagent une responsabilité particulière. Comme la notion de responsabilité n'est pas encore claire, on peut commencer par la définir de façon naïve comme un concept métier. Dans notre exemple on peut isoler&nbsp;:

1. Le lecteur, qui peut emprunter un livre, le rendre, et bien sûr s'abonner à la bibliothèque.
2. L'ouvrage, qui est identifié par son ISBN, a un auteur et un titre, existe en un certain nombre d'exemplaires.
3. L'exemplaire, qui peut être emprunté.
4. La bibliothèque, qui regroupe l'ensemble des ouvrages, des exemplaires présents ou non.
5. Le ou la bibliothécaire, qui peut inscrire un nouvel ouvrage ou un nouveau lecteur, et effectuer les prêts et les retours, ainsi que les relances pour retard.

On peut donc regrouper toutes les fonctions relatives au lecteur dans un fichier unique, de même pour ce qui concerne les livres, les bibliothécaires. Ceci résout une partie du problème lié au fait que tout le code soit disposé dans un même fichier, indépendamment de son but. Cependant l'état du système reste codé dans la classe principale (`Bibliotheque` dans notre cas). Cet état est assez compliqué, car il comprend toutes les variables qui permettent de stocker l'identité du ou de la bibliothécaire, du lecteur ou de la lectrice qui emprunte ou rend son livre, l'action effectuée en ce moment (restitution ou  emprunt, inscription, _et cetera_)&nbsp;:

```Java
// Fichier Lecteur.java
public class Lecteur {

    public static void emprunter() {
        // ...
    };

    public static void rendre() {
        // ...
    };

    // ...
}
// Fichier Bibliotheque.java
public class Bibliotheque {

    // Action en cours
    public static final int idAction;

    // Champs propres au lecteur en cours de traitement
    public static String idLecteur;
    ...

    // Champs propres au livre en cours de traitement
    public static String idLivre;

    public static void main(String[] args) {
        int code = 0;
        while (code != 6) {
            System.out.println("Que voulez-vous faire&nbsp;?");
            System.out.println("1. Inscrire un nouveau lecteur.");
            System.out.println("2. Inscrire un nouveau livre.");
            System.out.println("3. Prêter un livre.");
            System.out.println("4. Retourner un livre.");
            System.out.println("5. Consulter les retards.");
            System.out.println("6. Quitter.");
            // du code pour récupérer le choix de l'utilisateur
            idAction = readChoix();
            switch (idAction) {
                case 1: inscrireNouveauLecteur();
                    break;
                case 2: inscrireNouveauLivre();
                    break;
                case 3: preterLivre();
                    break;
                case 4: retournerLivre();
                    break;
                case 5: inscrireNouveauLecteur();
                    break;
                case 6: quitter();
                    break;
            }
        }
    }
}
```

Une première étape serait de ne plus maintenir les champs propres à un concept dans la classe principale, mais de les déplacer dans le fichier auquel ils appartiennent&nbsp;:

```Java
// Fichier Lecteur.java
public class Lecteur {

    public static String idLecteur;
    
    // autres champs ...

    public static void emprunter() {
        // ...
    };

    public static void rendre() {
        // ...
    };

    // ...
}
// Fichier Bibliotheque.java
public class Bibliotheque {

    // Action en cours
    public static final int idAction;

    public static void main(String[] args) {
        int code = 0;
        while (code != 6) {
            System.out.println("Que voulez-vous faire&nbsp;?");
            System.out.println("1. Inscrire un nouveau lecteur.");
            System.out.println("2. Inscrire un nouveau livre.");
            System.out.println("3. Prêter un livre.");
            System.out.println("4. Retourner un livre.");
            System.out.println("5. Consulter les retards.");
            System.out.println("6. Quitter.");
            // du code pour récupérer le choix de l'utilisateur
            idAction = readChoix();
            switch (idAction) {
                case 1: inscrireNouveauLecteur();
                    break;
                case 2: inscrireNouveauLivre();
                    break;
                case 3: preterLivre();
                    break;
                case 4: retournerLivre();
                    break;
                case 5: inscrireNouveauLecteur();
                    break;
                case 6: quitter();
                    break;
            }
        }
    }
}
```

Problème de taille… Le programme ne permet pas au bibliothécaire d'enregistrer les emprunts de livres autrement qu'un à la fois, puisque la classe `Livre` ne comporte qu'un champ `idLivre`. On voit à quel point il serait idéal de supposer que le template qui suit puisse être reproduit autant de fois que voulu&nbsp;:
```Java
public class Livre {

    public static String idLivre;
    public static String titre;
    public static String auteur;
}
```

Dans ce monde idéal, un `Lecteur` pourrait emprunter non pas un seul livre à la fois, mais plusieurs, en une seule manipulation. Sa fonction `emprunterLivre()` prendrait en argument la liste des livres qu'il désirerait emprunter&nbsp;:

```Java
public class Lecteur {
    public static String idLecteur;
    
    // autres champs ...

    public static void emprunter(List<Livre> livres) {
        // ...
    };

    public static void rendre(List<Livre> livres) {
        // ...
    };

    // ...
}
```
Le bibliothécaire n'aurait plus à effectuer 5 fois l'action &laquo;&nbsp;4. Retourner un livre.&nbsp;&raquo; pour récupérer 5 livres du même lecteur.

#### Introduction à l'approche objet
<a aria-hidden="true" href="#oop-intro" class="anchor" id="oop-intro"></a>

Le principe de l'approche objet est de découper l'application d'une façon conceptuellement aussi proche que possible de celle dont notre esprit conçoit les problèmes et leurs solutions (tout en restant compatible avec une implémentation sur un ordinateur).

> Dans l'approche objet, le programme est découpé en entités qui s'appellent les *objets*. Ces objets ont un état codé sous forme de variables, un comportement codé sous forme de fonctions et échangent entre eux en appelant leurs fonctions.

> Les variables d'un objet sont appelées **attributs**.

> Les fonctions d'un objet sont appelées **méthodes**, et on peut justifier ce nom par l'idée qu'un objet réalise une partie d'un contrat selon une méthode qui lui est propre.

Les langages qui implémentent la démarche objet permettent au développeur de définir des objets à l'aide de _templates_ de code comme dans cet exemple dans un langage algorithmique&nbsp;:

```
Human {
    has name: string
    has age: integer
    has dreams: list of Dream
    
    can speak: function(Context context) {
        % blah blah blah blah
    }

    can sleep: function(Context context) {
        % Zzzz zzzz zzzz zzzz
    }

    can laugh: function(Context context) {
        % Haaa haaa haaa haaa
    }
}

Dream {
    has owner: Human

    ...
}

Context {
    % So many things...
    ...
}
```

Dans cette approche, le développeur définit le **workflow** non pas en assignant des valeurs à des variables mais en définissant des objets, leur état initial et les interactions entre les objets sous forme d'appel de méthodes. C'est le compilateur qui fait le travail de transcription d'un **workflow** éclaté en objets vers un **workflow** structuré comme nous en avons l'habitude en algorithmique.

Ces &laquo;&nbsp;objets&nbsp;&raquo; sont un peu étranges, il peut être difficile de voir à quoi ils correspondent lorsque notre perspective est celle d'un découpage séquentiel des solutions.

Pour résumer, on peut représenter un programme structuré comme ceci&nbsp;:



&laquo;&nbsp;&nbsp;&raquo;
<details>
<summary>Réponse</summary>

xxxx
</details>


🧐
&#x26A0;

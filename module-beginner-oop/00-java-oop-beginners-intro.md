[[_TOC_]]

# Introduction

## Au sujet de ce document

Ce document est destiné au formateur et à toute personne curieuse des thèmes qu'elle aborde. En particulier, le contenu de ce document couvre largement le programme de la formation. Il ne peut pas être dispensé dans le détail, en deux jours, avec les exercices.

Il est conçu pour offrir au formateur l'information nécessaire pour dispenser la formation mais également le contexte et les détails qui lui permettent de maîtriser son sujet aussi pleinement que possible et éventuellement d'anticiper certaines questions.

Les détails qui devraient être omis en première approche sont précédés par l'émoji « &#129488; » mais certains autres détails non marqués pourraient et devraient l'être aussi, selon le contexte.

Les points d'attention sont précédés par l'émoji « &#9888;&#65039; ».

## Prérequis

### Obligatoire

Cette formation requiert une connaissance de base du langage Java, telle que présentée dans la formation « Introduction à l'algorithmique avec Java ».

Du point de vue logiciel, les prérequis sont&nbsp;:
1. Eclipse, IntelliJ, ou un autre IDE installé et fonctionnel.
2. JDK 17 ou supérieur installé.
3. Pour suivre les exercices&nbsp;: client git.

### Recommandé

Il n'y a pas de recommandation particulière pour cette formation.

## But

Le but de cette formation est de vous aider à comprendre et concevoir des applications écrites avec un langage objet, en particulier avec Java.

À l'issue de cette formation, vous connaîtrez&nbsp;:
1. Ce qu'est le paradigme objet en programmation, et le vocabulaire associé.
1. Une partie de la façon dont le langage Java implémente le paradigme objet&nbsp;:
    1. Les mots-clés `interface`, `abstract`, `class`, `final` associés aux classes et à l'héritage.
    1. Trois façons dont Java implémente le polymorphisme&nbsp;: l'héritage, la surdéfinition, la surcharge.
    1. Les mots-clés `private`, <i>package protected</i>, `public`, `protected` associés à l'encapsulation.
1. Les bases du langage UML pour modéliser un problème dans le paradigme objet.

La formation est axée autant sur la modélisation d'un problème que sur l'implémentation technique.

## Initialisation

1. Créez un répertoire où vous voulez importer le projet qui contient les exercices.
2. Ouvrez un terminal comme Git Bash ou un équivalent (faites un clic droit à l'intérieur du répertoire et cliquez sur _Git bash here_).
3. Dans le terminal, tapez ou copiez (`[Shift] + [Insert]`) la ligne de code suivante&nbsp;: `git clone git@<TODO>`.

Sous Eclipse&nbsp;: TODO


Sous IntelliJ&nbsp;: TODO
# Le paradigme objet en Java

## La classe est la brique de base

### La classe et `Object`

La classe la plus simple qu'on puisse imaginer est celle-ci&nbsp;:

```Java
package monpackage;

public class MaClasse {

}
```

Cette classe décrit un objet qui ne contient (presque rien) et qui ne fait pas grand chose.

Son nom est `MaClasse`, elle a été créée dans un package `monpackage` et elle doit exister dans un fichier qui s'appelle `MaClasse.java`. Son nom est écrit en _camel case_, avec une initiale en majuscule.

En réalité, le modèle par défaut de `MaClasse` est une classe un peut particulière, qui s'appelle `Object`, de laquelle héritent toutes les classes de Java, et qui permet de faire un certain nombre de choses&nbsp;:

1. Construire un nouvel objet (nous verrons comment).
1. Obtenir le nom de la classe d'un objet quelconque grâce à la méthode `Class<?> getClass()` qui renvoit un objet de type `Class`.
1. Obtenir une _hash-value_ propre à l'objet grâce à la méthode `int hashCode()`.
1. Répondre à la question « cet objet est-il égal à cet autre objet ? » grâce à la méthode `boolean equals(Object)`.
1. Renvoyer une description de cet objet grâce à la méthode `toString()`.
1. Et plusieurs autre choses en lien avec les threads, que nous ne verrons pas maintenant.

Donc en réalité, `MaClasse` encapsule également, par héritage, toutes ces fonctionnalités.

### Le constructeur est la fabrique de base

Mais comment créer un objet de type `MaClasse`&nbsp;?

Par chance, une classe vide est créée avec un constructeur, c'est-à-dire un moyen de créer de nouveaux objets. Cette classe vide pourrait déclarer explicitement ce même constructeur de cette manière&nbsp;:

```Java
package monpackage;

public class MaClasse {
    public MaClasse() {
        // Je ne sers à rien, j'existais déjà implicitement
    }
}
```

Et l'obtention d'un nouvel objet se fait de cette manière&nbsp;:

```Java
MaClasse monObjet = new MaClasse();
```

Ou encore&nbsp;:

```Java
var monObjet = new MaClasse();
```

On peut déjà constater que `monObjet` sait faire des choses, quoique pas très palpitantes&nbsp;:

```Java
var obj1 = new MaClasse();
var obj2 = new MaClasse();
System.out.println(obj1 == obj2);
System.out.println(obj1.getClass());
System.out.println(obj1.toString());
System.out.println(obj1);
```

`MaClasse` hérite de `Object`, donc `MaClasse` est un `Object`&nbsp;:

```Java
var obj1 = new MaClasse();
var obj2 = (Object) obj1; // upcast, se passe toujours bien
var obj3 = (MaClasse) obj2; // downcast, peut mal se passer
var str = (String) obj2; // Erreur à l'exécution, String ne peut s'obtenir à partir de MaClasse
```

### `null`

Lorsqu'un objet est déclaré mais pas assigné il prend un valeur particulière&nbsp;: `null`.

```Java
var obj1 = null; // Non !
MaClasse obj2 = null; // Oui !
System.out.println(obj2.toString()); // Non !
```

Cette valeur est en fait une absence de valeur. On se rappelle qu'un objet instancié est en fait une référence vers un emplacement mémoire qui comporte des méthodes et de la donnée.

Lorsqu'un objet est déclaré mais pas instancié il n'y a pas d'objet donc pas d'emplacement mémoire où mettre la donnée et le comportement. C'est ce que `null` signifie.

`null` n'a pas de type intrinsèque, donc `var obj1 = null;` ne compile pas.

Et comme `null` ne fait pas référence à un emplacement mémoire, il est impossible d'appeler des méthodes ou des attributs d'un objet déclaré mais non assigné.

### Une classe un peu plus riche

Avec tout ce que nous savons, il peut-être tentant de créer une classe `Individu` qui comporterait son nom, son prénom et sa date de naissance. Cette classe, idéalement, permettrait à l'individu de décliner son identité et de dire son âge&nbsp;:

```Java
// à partir de maintenant j'omets le package
import java.time.LocalDate;
import java.time.Period;

public class Individu {

    private String prenom;
    private String nom;
    private LocalDate dateNaissance;

    public Individu(String prenom, String nom, Date dateNaissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
    }

    public Period getAge() {
        return Period.between(dateNaissance, LocalDate.now());
    }

    public String getIdentite() {
        return prenom + " " + nom;
    }

    @Override
    public String toString() {
        return "Je m'appelle " + getIdentite() + " et j'ai " + getAge().getYears() " ans.";
    }
}
```

Examinons ensemble ce code&nbsp;:

1. Les mots-clés `public` et `private` seront expliqués plus tard.
1. Cette classe référence d'autres classes&nbsp;: `LocalDate` et `Period`. Ces classes sont codées dans un autre package que celui de `Individu`. En effet elles sont fournies par le langage proprement dit. Pour y faire référence, vous devez soit les appeler par leur nom complet, qualifié par le nom de package&nbsp;:
    ```Java
    private java.lang.LocalDate dateNaissance;
    ```
    Ou bien utiliser le mécanisme d'import qui permet à Java de signifier « lorsque je fais référence à la classe `LocalDate`, c'est à `java.time.LocalDate` que je fais référence. »&nbsp;:
    ```Java
    // avant le bloc class
    import java.lang.LocalDate;

    // ...
    // quelque part à l'intérieur du bloc class
    private LocalDate dateNaissance; // Java comprend de quoi il s'agit
    ```
1. Le constructeur de `Individu` est plus élaboré. Il prend les trois paramètres `prenom`, `nom`, et `dateNaissance` et valorise les attributs de `Individu` avec ces paramètres.
    De manière générale, le constructeur exécute l'intégralité de son code lorsqu'il est appelé, et résulte dans la création d'un objet.
1. Le constructeur explicite à trois paramètres élimine le constructeur implicite. Ici, tout appel à `new Individu()` provoque une erreur de compilation. Si l'utilisateur souhaite y faire appel il devra l'implémenter.
1. On remarque que plusieurs variables ont le même nom&nbsp;:
    ```Java
    this.prenom = prenom;
    this.nom = nom;
    this.dateNaissance = dateNaissance;
    ```
    Le mot-clé `this` désigne l'objet proprement dit, celui qui vient d'être créé. `this.prenom` désigne donc le prénom de l'objet qui vient d'être créé, tandis que `prenom` désigne le paramètre de la fonction.

    Le constructeur s'appelle ainsi&nbsp;:
    ```Java
    var dateNaissance = LocalDate.of(1922, 08, 25);
    var moi = new Individu("Sherlock", "Marple", dateNaissance);
    ```
1. `Period getAge()`, `String getIdentite()` et `String toString()` sont des méthodes. On peut les invoquer (les appeler) de cette manière&nbsp;:
    ```Java
    System.out.println(moi.getIdentite());
    System.out.println(moi);
    ```
    Le dernier appel `System.out.println(moi);` est ici équivalent à `System.out.println(moi.toString());`. Vous pouvez examiner le code de la fonction `println(Object)` pour comprendre pourquoi.
1. La méthode `String toString()` est déjà définie dans la classe `Object`. La définir de nouveau dans `Individu` est permis, et ce mécanisme s'appelle la surdéfinition (indiquée par le mot-clé non obligatoire, mais recommandé `@Override` avant la méthode).

TODO exercice arbre généalogique

#### Chaînage des constructeurs

Reprenons la classe `Individu`. On peut imaginer qu'un utilisateur de cette classe puisse ne pas connaître toutes les informations concernant un individu&nbsp;:

```Java
// Des imports...

public class Individu {

    private String prenom;
    private String nom;
    private LocalDate dateNaissance;

    public Individu(String prenom, String nom, Date dateNaissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
    }

    public Period getAge() {
        return Period.between(dateNaissance, LocalDate.now());
    }

    public String getIdentite() {
        return prenom + " " + nom;
    }

    @Override
    public String toString() {
        return "Je m'appelle " + getIdentite() + " et j'ai " + getAge().getYears() " ans.";
    }
}

// ... Ailleurs

public class Main {

    public static void main(String[] args) {
        Individu robert = new Individu("Robert", null, null);
    }

}
```

Je ne connais pas son nom ni sa date de naissance, donc je donne la valeur `null` à ces deux paramètres. Cette façon de faire n'est pas très pratique. Donc je préfère avoir à disposition des fabriques qui soient plus adaptées à mes connaissances&nbsp;:

```Java
// Des imports

public class Individu {

    private String prenom;
    private String nom;
    private LocalDate dateNaissance;

    public Individu(String prenom, String nom, Date dateNaissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
    }

    public Individu(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }

    public Individu(String prenom) {
        this.prenom = prenom;
    }

    public Individu(String nom) {
        this.nom = nom;
    }

    // le reste de la classe
}

// ... Ailleurs

public class Main {

    public static void main(String[] args) {
        Individu robert = new Individu("Robert");
    }

}
```

C'est beaucoup plus pratique, mais il y a une redondance dans le code. Si je veux que le prénom et le nom soient mis en forme&nbsp;:

```Java
// Des imports

public class Individu {

    private String prenom;
    private String nom;
    private LocalDate dateNaissance;

    public Individu(String prenom, String nom, Date dateNaissance) {
        this.prenom = miseEnForme(prenom);
        this.nom = miseEnForme(nom);
        this.dateNaissance = dateNaissance;
    }

    public Individu(String prenom, String nom) {
        this.prenom = miseEnForme(prenom);
        this.nom = miseEnForme(nom);
    }

    public Individu(String prenom) {
        this.prenom = miseEnForme(prenom);
    }

    public Individu(String nom) {
        this.nom = miseEnForme(nom);
    }
    private miseEnForme(String string) {
        if (string == null) {
            return "";
        }
        var lowString = string.toLowerCase();
        return lowString.subString(0, 1).toUpperCase()
            + lowString.subString(1);
    }

    // le reste de la classe
}

// ... Ailleurs

public class Main {

    public static void main(String[] args) {
        Individu robert = new Individu("Robert");
    }

}
```
C'est horrible. Je suis obligé de modifier le code partout. Pour éviter cela Java met à notre disposition un outil pratique&nbsp;:

```Java
// Des imports

public class Individu {

    private String prenom;
    private String nom;
    private LocalDate dateNaissance;

    public Individu(String prenom, String nom, Date dateNaissance) {
        this.prenom = miseEnForme(prenom);
        this.nom = miseEnForme(nom);
        this.dateNaissance = dateNaissance;
    }

    public Individu(String prenom, String nom) {
        this(prenom, nom, null);
    }

    public Individu(String prenom) {
        this(prenom, null, null);
    }

    public Individu(String nom) {
        this(prenom, null, null);
    }
    private miseEnForme(String string) {
        if (string == null) {
            return "";
        }
        var lowString = string.toLowerCase();
        return lowString.subString(0, 1).toUpperCase()
            + lowString.subString(1);
    }

    // le reste de la classe
}

// ... Ailleurs

public class Main {

    public static void main(String[] args) {
        Individu robert = new Individu("Robert");
    }

}
```

Le code ne change ainsi que dans un constructeur. L'appel `this(...)` transfère la responsabilité de la création de l'objet à un autre constructeur.

Les règles à respecter&nbsp;:

1. L'appel à `this(...)` doit être la première instruction du bloc.
    ```Java
    class A1 {
        A1() {
            // stuff
        }

        A1(String string) {
            this(); // Oui
        }

        A1(int int) {
            String string = "" + int;
            this(string); // Non !
        }
    }
    ```
1. Tant que l'appel à `this(...)` n'est pas résolu, il est interdit de faire référence à `this`&nbsp;:
    ```Java
    class A1 {
        A1() {
            // stuff
        }

        A1(String string) {
            this(this.toString()); // Non
        }
    }
    ```
1. Pas de références circulaires&nbsp;:
    ```Java
    // Non
    class A1 { // Non
        A1() {
            this("Coucou !"); // Non
        }
        // Non Non Non Non Non Non
        A1(String string) {
            this(); // Non
        }
    }
    ```
1. Les paramètres de `this(...)` peuvent être des appels à des méthodes de classe&nbsp;:
    ```Java
    class A1 {
        A1(String string) {
            this(mettreEnForme(string)); // Oui
        }

        static String mettreEnForme(String string) {
            return "<" + string + ">";
        }
    }
    ```
1. Le constructeur peut faire suivre l'appel à `this(...)` par autant d'instructions que nécessaire.

### Portée des attributs

#### Attribut de classe ou d'objet ? Le mot-clé `static`

Du point de vue sémantique, nous avons vu qu'une classe est un modèle sur lequel sont faits les objets. Un objet a des particularités mais il y a également des caractéristiques et des comportement qui s'appliquent à tous les objets.

Reprenons l'exemple de la cafetière Mehspresso « Laetissia Delangui B52 ». Chaque cafetière a un modèle commun (le type qui donne son nom à la classe) qui comporte le comportement et l'état de l'objet. Ces élements sont définis dans la classe, mais seul l'objet en a connaissance. La preuve&nbsp;: quand je lance le programme « café court », seule ma cafetière s'exécute&nbsp;:

```Java
Cafetiere maCafetiere = new LaetissiaDelanguiB52();
maCafetiere.cafeCourt(); // C'est ma cafetière qui fait le café pour moi
```

De même le numéro de série est propre à l'exemplaire et non au modèle&nbsp;:

```Java
maCafetiere.getSerialNumber(); // Aucune autre cafetière n'a le même
```

En revanche, le nom du modèle (« Laetissia Delangui B52 ») n'est pas propre à l'exemplaire mais bien au modèle. Je devrais donc pouvoir interroger la classe sans avoir besoin d'avoir un exemplaire pour connaître ce nom&nbsp;:

```Java
public class LaetissiaDelanguiB52 {

    public static final String NOM_MODELE = "Laetissia Delangui B52";

    // Plein de code qui fait le café
}

// Comment s'appelle la petite dernière de Mehspresso ?
System.out.println(LaetissiaDelanguiB52.NOM_MODELE);
```

Ce mot-clé `static` désigne exactement cela&nbsp;: un attribut ou une méthode qui est propre à la classe et non sur l'objet. Un attribut ou une méthode statiques s'invoquent de cette manière&nbsp;:

```Java
ClassName.attributeName;
ClassName.methodName(param1, ...);
```

Chez l'être humain, lesquels de ces attributs/méthodes sont statiques&nbsp;:

- le nombre d'yeux,
- le nom de l'espèce,
- le prénom,
- manger,
- le nombre d'humains sur la planète.
<details>
<summary>Réponse</summary>
- le nombre d'yeux d'un individu est non statique (il y a des personnes qui n'ont qu'un œil ou même pas du tout), mais le nombre d'yeux théorique de l'espèce est statique,
- le nom de l'espèce est statique,
- le prénom est propre à l'individu, donc non statique,
- manger est une méthode qu'un humain fait pour lui-même, donc non statique,
- le nombre d'humains sur la planète à un instant <i>t</i> ne dépend pas d'un humain en particulier, c'est donc un attribut statique.
</details>

Le nombre d'exemplaires de la cafetière « Laetissia Delangui B52 » augmente chaque jour. On pourrait supposer que ce nombre est un attribut de la classe LaetissiaDelanguiB52, et coder ainsi le constructeur&nbsp;:

```Java
public class LaetissiaDelanguiB52 {

    public static int count = 0;

    public LaetissiaDelanguiB52 {
        count++; // ou de manière équivalente LaetissiaDelanguiB52.count++
    }

    // Plein de code qui fait le café
}
```

Chaque fois qu'un nouvel exemplaire est créé, il ajoute 1 au nombre d'exemplaires créés. Ainsi le modèle connaît le nombre d'exemplaires.

Est-ce une bonne pratique de positionner le nombre d'exemplaires de cette classe, dans cette même classe&nbsp;? Si on raisonne en termes de responsabilité, quel objet devrait avoir la responsabilité de ce comptage&nbsp;?

<details>
<summary>Réponse</summary>
On pourrait imaginer que le nombre d'objets produits par une usine est plus de la responsabilité d'une partie de cette usine, par exemple un compteur en sortie de chaîne qui scanne les objets et effectue ce comptage.

Dans ce cas l'attribut `count` serait un attribut non statique, un attribut d'objet, propre au scanner.
</details>

#### Variable ou constante&nbsp;? Le mot-clé `final`

Nous avons vu que l'attribut `count`, s'il est positionné dans la classe `LaetissiaDelanguiB52`, est statique car il est propre non pas à l'objet mais à la classe.

L'attribut `NOM_MODELE` est également statique, mais il a une grosse différence&nbsp;: il n'a aucune raison de changer.

De ce fait on lui colle un mot-clé qui indique que sa valeur ne changera jamais&nbsp;: `final`&nbsp;:

```Java
public class LaetissiaDelanguiB52 {

    public static int count = 0;
    public static final String NOM_MODELE = "Laetissia Delangui B52";

    public LaetissiaDelanguiB52 {
        count++; // ou de manière équivalente LaetissiaDelanguiB52.count++
    }

    // Plein de code qui fait le café
}
```

Les attributs `static final` s'écrivent en `SCREAMING_SNAKE_CASE`, contrairement à tous les autres qui s'écrivent en `camelCase`.

#### Caché ou pas&nbsp;? Les mots-clés `private`, `protected`, `public` et la visibilité par défaut

La cafetière « Laetissia Delangui B52 » a un état interne complexe, qui lui permet par exemple de faire un cappuccino parfait et de saupoudrer mon breuvage d'un peu de cacao (d'où le nom B52).

Cet état complexe est gardé à l'intérieur de la machine. Tout ce que l'utilisateur voit c'est un écran LED et quelques boutons. En revanche l'état de bon fonctionnement de la machine est codé par une vingtaine de codes différents. Par ailleurs la machine traite chaque programme (« café court », « mocaccino », _et cetera_) exactement de la même façon&nbsp;: comme une suite de 12 étapes, effectuées ou non en fonction du programme&nbsp;:

1. Ajouter du sucre.
2. Ajouter du sucre.
3. Ajouter du sucre.
4. Ajouter du sucre.
5. Ajouter du sucre.
6. Passer l'eau dans la capsule de café.
7. Passer l'eau dans la capsule de café.
8. Ajouter un supplément d'eau.
9. Ajouter un supplément de lait.
10. Ajouter de la mousse de lait.
11. Ajouter du cacao.
12. Ajouter un mélangeur.

Par exemple un café court modérément sucré (3 sucres sur 5) conduit à l'exécution des étapes 1, 2, 3, 6 et 12. Un cappuccino non sucré conduit à l'exécution des étapes 6, 8, 9, 10 et 11.

On peut imaginer notre cafetière `LaetissiaDelanguiB52` codée ainsi&nbsp;:

```Java
public class LaetissiaDelanguiB52 {

    private int etapeProgramme = 0;
    private String programmeEnCours;
    private EtatEtape etatEtapeCourante;
    private EtatEtapeCourante[] etatProgramme;
    private Message message;

    public LaetissiaDelanguiB52() {
        // le constructeur
    }

    public void cafeCourt() {
        // du code qui fait le café
    }

    private void afficherMessage() {
        // affiche le message à l'écran, par exemple "Veuillez patienter"
    }
}
```

La méthode `cafeCourt` est publique&nbsp;: on veut que l'utilisateur puisse se faire un café. En revanche l'affichage du message qui informe l'utilisateur de l'avancement du programme (de « en cours » à « prenez votre boisson ») est géré par la machine et ne dépend pas de l'utilisateur, même si l'utilisateur déclenche l'opération qui conduit à des affichages de messages.

De même, l'intégralité de l'état de la machine est caché à l'utilisateur. L'écran est le seul moyen que la machine possède pour communiquer son état avec l'utilisateur. Ainsi, l'état `xU789` correspond à l'affichage du message `Il n'y a pas d'eau dans le réservoir. Veuillez en remettre.`.

Les attributs et méthodes cachés sont indiqués par le mot-clé `private`, tandis que les membres visibles sont indiqués par le mot-clé `public`.

Ces deux visibilités sont les plus simples à comprendre, mais il y en a deux autres.

On a vu que les classes sont toutes regroupées dans des packages. Ces packages sont structurés de sorte qu'un groupe de classes d'un même package participent à une responsabilité du package. Pour dire les choses autrement&nbsp;: un package regroupe des classes qui font la même chose ou bien qui travaillent étroitement entre elles pour remplir un objectif.

On peut imaginer par exemple que la classe `LaetissiaDelanguiB52` est située dans un package `com.mehspresso.products.laetissiadelangui`.

On peut aussi imaginer que la classe `LaetissiaDelanguiB52`, étant très compliquée, va déléguer une partie de son fonctionnement à des classes techniques qui serviront aussi au modèle de base `LaetissiaDelanguiBasic`, par exemple.

Ces classes techniques (comme par exemple la possibilité de revenir aux réglages d'usine), ont-elles vocation à être utilisées par d'autres modèles d'un autre package&nbsp;?

La réponse est non&nbsp;:

- si une classe technique est utilisée par un autre package que `laetissiadelangui`, par exemple `initial`, alors elle n'est pas positionnée dans un des deux packages mais dans un package transverse, par exemple `com.mehspresso.products.parts.screen`&nbsp;;
- si une classe technique est propre à un produit, par exemple `LaetissiaDelangui` et ses dérivés, alors il ne devrait pas être accessible des autres packages.

La visibilité par défaut d'une classe est la visibilité de package&nbsp;:
```Java
class ResetLaetissiaDelangui extends Reset {

    void accept(LaetissiaDelangui obj) {
        // code qui reset la machine
    }
}
```

Il existe une dernière visibilité qui s'appelle `protected`&nbsp;: elle désigne les membres qui sont visibles dans le package et dans les classes dérivées. On reviendra plus tard cette visibilité.

## La notion d'héritage

On peut imaginer une gamme de cafetières Mehspresso qui s'appellerait `Sitease`. Le modèle de base aurait donné naissance à plusieurs variantes qui seraient toutes obtenus par ajout de fonctionnalités par rapport au modèle de base.

On pourrait coder cela par délégation&nbsp;:

```Java
public class SiteaseBasic {

    public void cafeCourt() {
        // du code qui le fait
    }

    public void cafeLong() {
        // du code qui le fait
    }
}

public class SiteaseDoubleEspresso {

    private SiteaseBasic delegate = new SiteaseBasic();

    public void cafeCourt() {
        delegate.cafeCourt();
    }

    public void cafeLong() {
        delegate.cafeLong();
    }

    public void doubleCafe() {
        // du code qui le fait, et que ne sait pas faire delegate
    }
}
```

Mais Java simplifie les chose en proposant un mécanisme d'héritage qui permet d'étendre une classe&nbsp;:

```Java
public class SiteaseBasic {

    public void cafeCourt() {
        // du code qui le fait
    }

    public void cafeLong() {
        // du code qui le fait
    }
}

public class SiteaseDoubleEspresso extends SiteaseBasic {

    // tout le code public de SiteaseBasic est accessible ici

    public void doubleCafe() {
        // du code qui le fait, et que ne sait pas faire delegate
    }
}
```

C'est bien plus pratique car le code est ainsi factorisé. Il est également possible de redéfinir dans la classe fille des méthodes de la classe mère&nbsp;:

```Java
public class SiteaseBasic {

    public void cafeCourt() {
        // du code qui le fait
    }

    public void cafeLong() {
        // du code qui le fait
    }
}

public class SiteaseDoubleEspresso extends SiteaseBasic {

    // tout le code public de SiteaseBasic est accessible ici

    @Overrride
    public void cafeLong() {
        // du code qui le fait différemment de SiteaseBasic
    }

    public void doubleCafe() {
        // du code qui le fait, et que ne sait pas faire delegate
    }
}
```

## La notion d'interface

### Présentation

On a parlé d'interface pour les modèles de cafetière&nbsp;: il s'agissait de la part de l'état et du comportement d'un objet, visibles par l'utilisateur.

L'ensemble des fonctionnalités accessibles à l'utilisateur peut-être contractualisé&nbsp;: ce contrat c'est, au sens Java, l'interface.

On définit une interface de cette manière&nbsp;:

```Java
/**
 * Décrit l'ensemble des modèles de la gamme LaetissiaDelangui
 */
public interface LaetissiaDelangui {
    /**
     * Permet de faire un café court
     */
    void cafeCourt();
    /**
     * Permet de faire un café long
     */
    void cafeLong();
    // ... tous les cafés
}
```

Cette interface a vocation a être *implémentée* par tous les modèles de la gamme, par exemple `LaetissiaDelanguiB52` ou `LaetissiaDelanguiBasic`&nbsp;:

```Java
public class LaetissiaDelanguiBasic implements LaetissiaDelangui {
    /**
     * Permet de faire un café court de telle et telle manière
     */
    void cafeCourt() {
        // du code qui fait le café
    }
    /**
     * Permet de faire un café long de telle et telle manière
     */
    void cafeLong() {
        // du code qui fait le café
    }
}
```

L'interface décrit ce qui doit être fait, mais ne fait rien en elle-même. La classe qui implémente une interface comporte pour chaque méthode de l'interface, le code qui réalise la fonctionnalité.

Quel est l'intérêt d'une telle façon de faire ? C'est d'écrire un code qui ne préjuge pas de la façon dont seront faites les choses. On peut imaginer que des gammes différentes de cafetières utilisent des écrans différents. Chaque écran a une largeur, une hauteur différentes. Pour autant, on peut imaginer que l'interface offerte par l'écran à la cafetière est la même&nbsp:

```Java
interface EcranCafetiere {
    void accept(Message message);
}

public class EcranDeLuxe implements EcranCafetiere {

    void accept(Message message) {
        // du code qui affiche le message sur un écran de luxe
    }
}

public class EcranStandard implements EcranCafetiere {

    void accept(Message message) {
        // du code qui affiche le message sur un écran standard
    }
}

public class LaetissiaDelanguiB52 {

    private EcranCafetiere ecran;
    private Message message;

    // des attributs
    // ...

    public LaetissiaDelanguiB52() {
        this.ecran = new EcranDeLuxe();
    }

    // des méthodes
    // ...

    private void afficherMessage() {
        this.ecran.accept(message);
    }
}
```

Lorsque le message doit être affiché, la méthode `LaetissiaDelangui#afficherMessage()` est appelée. Cette méthode se contente de transférer le message à l'écran par l'appel `this.ecran.accept(message)`. Sans connaître le type d'écran, on sait que celui-ci saura afficher le message et on lui délègue la responsabilité de le faire de la manière dont il a été programmé.

Comme dans les faits, `ecran` est un `EcranDeLuxe`, le code appelé sera différent de celui appelé pour la classe `LaetissiaDelanguiBasic`&nbsp;:

```Java
public class LaetissiaDelanguiBasic {

    private EcranCafetiere ecran;
    private Message message;

    // des attributs
    // ...

    public LaetissiaDelanguiBasic() {
        this.ecran = new EcranStandard();
    }

    // des méthodes
    // ...

    private void afficherMessage() {
        this.ecran.accept(message);
    }
}
```

Le code ne dépend pas des détails de l'implémentation, ce qui a pour avantage de le rendre plus maintenable et plus évolutif.

### Points techniques

#### Généralités sur les interfaces

Reprenons le code de l'interface `LaetissiaDelangui`&nbsp;:

```Java
public class LaetissiaDelanguiBasic implements LaetissiaDelangui {
    /**
     * Permet de faire un café court de telle et telle manière
     */
    void cafeCourt() {
        // du code qui fait le café
    }
    /**
     * Permet de faire un café long de telle et telle manière
     */
    void cafeLong() {
        // du code qui fait le café
    }
}
```

1. Les méthodes n'ont pas de corps.
1. Les méthodes n'ont pas de modificateur de visibilité, puisque les méthodes seront nécessairement publiques.
1. La JavaDoc doit décrire précisément le contrat, c'est-à-dire ce qui est attendu de chaque méthode de l'interface. Des choses aussi précises que la concurrentialité, le déterminisme, l'idempotence, le caractère pur, la nécessité de redéfinir certaines méthodes (comme par exemple `hashcode()` et `equals(Object)`) _et cetera_ peuvent être spécifiés.
1. Une interface n'a pas d'attributs, mais peut avoir des constantes.

#### Méthodes par défaut&nbsp;: le mot-clé `default`

Par ailleurs, un interface peut implémenter du comportement avec des méthodes préfixées par le mot-clé `default`&nbsp;:

```Java
/**
 * Décrit l'ensemble des modèles de la gamme LaetissiaDelangui
 */
public interface LaetissiaDelangui {
    /**
     * Permet de faire un café court
     */
    void cafeCourt();

    /**
     * Permet de faire un café long
     */
    void cafeLong();
    // ... tous les cafés

    /**
     * Allume la cafetière
     */
    default void allumer() {
        // Tous les modèles LaetissiaDelangui s'allument de la même manière
    }

    /**
     * Éteint la cafetière
     */
    default void eteindre() {
        // Tous les modèles LaetissiaDelangui s'éteignent de la même manière
    }
}
```

#### Implémentations multiples

Une classe peut implémenter plusieurs interfaces. Simplement, séparer les noms des interfaces avec une virgule&nbsp;:

```Java
interface ModeleNeonFluo {
    void effetDisco();
}

public class LaetissiaDelanguiB52 implements LaetissiaDelangui, ModeleNeonFluo {
    /**
     * Permet de faire un café court de telle et telle manière
     */
    public void cafeCourt() {
        // du code qui fait le café
    }
    /**
     * Permet de faire un café long de telle et telle manière
     */
    public void cafeLong() {
        // du code qui fait le café
    }

    // pas besoin de réécrire le code pour allumer() et eteindre()

    public void effetDisco() {
        // du code qui fait du disco avec les néons
    }
}
```

L'interface `ModeleNeonFluo` peut être réutilisée pour tous les modèles B52, chaque gamme de Mehspresso en a un.

Remarquez que les méthodes implémentées sont précédées du modificateur `public`&nbsp;! C'est logique puisque ce sont des méthodes d'interfaces.

Une classe ne peut avoir qu'une méthode avec une signature donnée&nbsp;: donc une classe peut implémenter des interfaces qui ont des méthodes avec la même signature, mais dans ce cas&nbsp;:

1. Il est très fortement recommandé que la méthode commune ait le même sens dans les deux interfaces.
1. Le type de retour doit être le même.

```Java
interface I1 {
    void run();
}

interface I2 {
    void run();
}

interface I3 {
    int run();
}

class C1 implements I1, I2 { // autorisé
    void run() {
        // some code
    }
}

class C2 implements I1, I3 { // interdit
    void run() {
        // some code
    }
}

class C3 implements I1, I3 { // interdit
    int run() {
        // some code
    }
}

class C3 implements I1, I3 { // doublement interdit
    void run() {
        // some code
    }

    int run() {
        // some code
    }
}
```

## Classe abstraite&nbsp;: le mot-clé `abstract`

Les modèles « Laetissia Delangui Basic », « Laetissia Delangui B52 » et les autres ont beaucoup de points communs, et quelques différences.

Nous avons vus que le contrat de cette gamme est décrit dans l'interface `LaetissiaDelangui`. Mais cette interface n'implémente presque rien, hormis peut-être dans quelques méthodes par défaut.

On peut imaginer par exemple que la gestion d'un programme est exactement la même pour tous les modèles de la gamme&nbsp;: une file d'attente dans laquelle des items sont inscrits pour être exécutés, une communication avec l'écran sous forme de message, _et cetera_.

Cette gestion est implémentée par un état et un comportement, dans chacune des classes `LaetissiaDelangui`. C'est un vrai gâchis de code de le répéter ainsi dans chaque classe dérivée `LaetissiaDelanguiBasic`, `LaetissiaDelanguiB52`, _et cetera_.

La notion de classe abstraite répond à ce problème en fournissant un moyen simple de factoriser du code&nbsp;:

```Java
/**
 * Décrit l'ensemble des modèles de la gamme LaetissiaDelangui
 */
public interface LaetissiaDelangui {
    /**
     * Permet de faire un café court
     */
    void cafeCourt();
    /**
     * Permet de faire un café long
     */
    void cafeLong();
    // ... tous les cafés
}

abstract class LaetissiaDelanguiDefault implements LaetissiaDelangui {

    private int etapeProgramme = 0;
    private String programmeEnCours;
    private EtatEtape etatEtapeCourante;
    private EtatEtapeCourante[] etatProgramme;
    private Message message;

    protected LaetissiaDelanguiDefault() {
        // le constructeur
    }

    @Override
    public void cafeCourt() {
        // du code qui fait le café
    }

    @Override
    public void cafeLong() {
        // du code qui fait le café
    }

    /**
     * Programme de nettoyage de la cafetière
     */
    abstract void nettoyer();

    // ... tous les cafés

    protected void afficherMessage() {
        // affiche le message à l'écran, par exemple "Veuillez patienter"
    }
}

public class LaetissiaDelanguiClassic extends LaetissiaDelanguiDefault implements LaetissiaDelangui {
    public LaetissiaDelanguiClassic() {
        super();
    }

    public void nettoyer() {
        // du code
        afficherMessage(); // informer l'utilisateur du nettoyage en cours
        // du code
    }
}
```

La classe abstraite peut implémenter ou non tout ou partie des méthodes des interfaces qu'elle implémente. Elle comporte du code mais ne peut pas être instanciée&nbsp;:

```Java
LaetissiaDelanguiDefault laetissiaDelanguiDefault = new LaetissiaDelanguiDefault(); // Non !
LaetissiaDelanguiClassic laetissiaDelanguiClassic = new LaetissiaDelanguiClassic(); // Oui !
```

Une classe concrète `X` qui étend une classe abstraite ou dérive d'une classe abstraite `Y` est repérée par `class X extends Y`. Toute la donnée et tout le comportement de la classe abstraite se retrouvent dans la classe concrète, sous réserve que leur visibilité soit suffisante.

Une classe peut implémenter plusieurs interfaces mais ne peut étendre (hériter de) qu'une classe abstraite.

Les sections qui suivent détaillent les usages…

### Classes abstraites, concrètes, visibilités

##### Les méthodes privées sont inaccessibles aux classes filles

```Java
public abstract class A1 {
    private void printStuff() {
        System.out.println("Some stuff");
    }

    public void doStuff() {
        printStuff();
    }
}

public class ChildOfA1 extends A1 {

    public void doOtherStuff() {
        doStuff(); // oui, appelle A1#doStuff()
    }

    public void doForbiddenStuff() {
        printStuff(); // non, impossible d'y accéder
    }

}
```

##### Les méthodes avec une visibilité de package sont inaccessibles aux classes des autres package

```Java
// package a
public abstract class A1 {
    void printStuff() {
        System.out.println("Some stuff");
    }

    public void doStuff() {
        printStuff();
    }
}

public class ChildOfA1 extends A1 {

    public void doOtherStuff() {
        doStuff(); // oui, appelle A1#doStuff()
    }

    public void doFAuthorizeedStuff() {
        printStuff(); // oui, appelle A1#printStuff()
    }

}

// package b
public class ChildOfA1 extends A1 {

    public void doOtherStuff() {
        doStuff(); // oui, appelle A1#doStuff()
    }

    public void doForbiddenStuff() {
        printStuff(); // non, impossible d'y accéder
    }

}
```

##### Les méthodes protégées sont accessibles aux classes filles et aux classes du package

```Java
// package a
public abstract class A1 {
    void printStuff() {
        System.out.println("Some stuff");
    }

    public void doStuff() {
        printStuff();
    }
}

public class ChildOfA1 extends A1 {

    public void doOtherStuff() {
        doStuff(); // oui, appelle A1#doStuff()
    }

    public void doFAuthorizeedStuff() {
        printStuff(); // oui, appelle A1#printStuff()
    }

}

// package b
public class ChildOfA1 extends A1 {

    public void doOtherStuff() {
        doStuff(); // oui, appelle A1#doStuff()
    }

    public void doFAuthorizeedStuff() {
        printStuff(); // oui, appelle A1#printStuff()
    }
}

public class SomeCLass {

    public void doForbiddenStuff() {
        A1 a = new A1() {}; // Oui, on peut instancier une classe abstraite si on prend soin d'implémenter tout le comportement (les méthodes abstraites et les interfaces)
        a.printStuff(); // Non ! De quel droit ?
    }
}
```

##### Les méthodes publiques sont accessibles à tous, sans surprise

Pas besoin de code ici…

### Surdéfinition, `super`

#### Appel au constructeur parent

Reprenons le snippet de code de nos cafetières préférées&nbsp;:

```Java
abstract class LaetissiaDelanguiDefault implements LaetissiaDelangui {

    // des attributs

    protected LaetissiaDelanguiDefault() {
        // le constructeur
    }

    @Override
    public void cafeCourt() {
        // du code qui fait le café
    }
    
    // et cetera
}

public class LaetissiaDelanguiClassic extends LaetissiaDelanguiDefault implements LaetissiaDelangui {
    public LaetissiaDelanguiClassic() {
        super();
    }

    // et cetera
}
```

Le constructeur de la classe `LaetissiaDelanguiClassic` fait appel à `super()`. C'est le constructeur de la classe parente. Par défaut les constructeurs sans arguments sont implicites donc il est inutile de préciser ces deux constructeurs `LaetissiaDelanguiDefault()` et `LaetissiaDelanguiClassic()`.

En revanche&nbsp;:

1. Toutes les classes filles doivent faire référence à au moins un constructeur de la classe mère&nbsp;:
    ```Java
    abstract A1 {}
    SonOfA1 extends A1 {} // Oui : les constructeurs sans arguments sont implicites

    abstract A2 {
        A2(String string);
    }
    DaughterOfA2 extends A2 {} // Non : un moyen de construire DaughterOfA2, utilisant le seul constructeur de A2, doit être fourni
    SonOfA2 extends A2 {
        SonOfA2() {
            super("Une valeur par défaut"); // Oui !
        }
    }
    ```
1. Il n'est pas nécessaire qu'une classe fille fasse appel à tous les constructeurs de la classe mère&nbsp;: en effet la règle précédente existe pour assurer la possibilité d'obtenir un objet qui hérite d'une classe abstraite.

#### Surdéfinition

Une classe abstraite peut implémenter du comportement&nbsp;:

```Java
abstract class A1 {
    public String toString() {
        return "class A1";
    }
}
```

Ce comportement peut être redéfini dans une classe fille&nbsp;:

```Java
class A2 extends A1 {
    @Override
    public String toString() {
        return "class A2";
    }
}

// Ailleurs ...
class Main {

    public static void main(String args[]) {
        A1 a1 = new A1();
        System.out.println(a1.toString()); // class A1
        A2 a2 = new A2();
        System.out.println(a2.toString()); // class A2
    }
}
```

Mais il est également possible de faire appel à l'implémentation parente&nbsp;:

```Java
class A2 extends A1 {
    @Override
    public String toString() {
        return "class A2 of" + super.toString(); // class A2 of class A1
    }
}
```
### (émoji détail) Point technique sur `this` et `super`

Les mots clés `this` et `super` sont des attributs statiques qui font référence à l'instance courante de la classe (ou de la classe parente)&nbsp;:

```Java
class A {
    void uneMethode() {
        System.out.println(A.this.toString()); // Oui
        System.out.println(A.super.toString()); // Oui
    }
}
```

C'est important de le savoir lorsque vous utilisez des classes nichées.

### Aller plus haut&nbsp;: implémenter directement une interface ou une classe abstraite

Le principe de l'interface est de proposer un contrat qui sera implémenté. Le principe de la classe abstraite est d'implémenter une partie d'un contrat pour mutualiser le code de plusieurs classes.

Java propose la classe (abstraite ou non) comme moyen d'implémenter une interface, et propose la classe concrète comme moyen d'implémenter la classe abstraite.

Mais Java permet d'instancier une interface ou une classe abstraite de façon anonyme, c'est-à-dire sans déclarer `class XXXX`&nbsp;:

```Java
public interface I1 {
    void someFunction();
}

// Ailleurs ...

public class Main {
    public static void main(String[] args) {
        I1 anonymous = new I1() {

            @Override
            public void someFunction() {
                System.out.println("Some stuff");
            }
        };
    }
}
```

Toutes les fonctions abstraites de l'interface doivent être implémentées dans son… ben… dans son implémentation.

```Java
public abstract class A1 {

    private int someIntAttr;

    public A1(int someIntValue) {
        this.someIntAttr = someIntValue;
    }

    abstract void someFunction();

}

// Ailleurs ...

public class Main {
    public static void main(String[] args) {
        A1 anonymous = new A1(12) {
            @Override
            void someFunction() {
                System.out.println("Some stuff");
            }
        };
    }
}
```

Remarquons ici que le constructeur de la classe `A1` a été appelé avec un argument. C'est normal, la classe `A1` ne possède qu'un constructeur, qui a un argument.

Remarquons au passage que l'implémentation de `A1` ne peut faire appel aux membres privés de `A1`&nbsp;:

```Java
public abstract class A1 {

    private int someIntAttr;

    public A1(int someIntValue) {
        this.someIntAttr = someIntValue;
    }

    abstract void someFunction();

}

// Ailleurs ...

public class Main {
    public static void main(String[] args) {
        A1 anonymous = new A1(12) {
            @Override
            void someFunction() {
                System.out.println("someIntAttr vaut " + someIntAttr); // Non ! ne compile pas
            }
        };
    }
}
```

Et c'est bien normal. Une classe fille ne peut non plus accéder aux membres privés de sa classe mère.

## Packages, modules, principe de substitution

### Un principe important, le principe de substitution

Prenons un exemple de la vie réelle&nbsp;: la médecine. Supposons un monde où les experts en médecine ne la pratiquent pas forcément. Par exemple ils peuvent être chercheurs mais ne peuvent pas donner de consultations. Les médecins en revanche sont des experts qui louent des services de consultation.

On peut conceptualiser cela en Java par&nbsp;:

```Java
abstract class ExpertMedecine {

    public void mobiliseSavoir() {
        // des choses médicales
    }
}

public class ChercheurMedecine extends ExpertMedecine {

    public void faitDeLaRecherche() {
        // code de chercheur
        mobiliseSavoir(); // ça doit être utile à un moment donné
        // encore du code de chercheur
    }
}

public class MedecinDeFamille extends ExpertMedecine {

    public void loueConsultation() {
        // code de chercheur
        mobiliseSavoir(); // ça doit être utile à un moment donné
        // encore du code de chercheur
    }
}
```

Lorsque vous êtes malade, la personne que vous allez voir est un `ExpertMedecine` puisque c'est un `MedecinDeFamille`. En revanche vous recherchez dans l'annuaire un `MedecinDeFamille` et non pas un `ExpertMedecine`. Sinon, l'annuaire pourrait vous proposer Monsieur Unepierre, qui est `ChercheurMedecine`, très fort en médecine mais seulement dans le cadre de la recherche.

C'est un aspect important du code objet&nbsp;: concevez votre code de façon qu'à chaque endroit, on puisse substituer un type par n'importe lequel de ses sous-types sans que cela pose de problème.

Par exemple, vous avez recherché un `MedecinDeFamille` et vous êtes tombé sur Madame Ouidosonne qui est `MedecinDeFamilleDieteticien`. Elle est médecin et si en plus vous avez des problèmes liés à l'alimentation son aide sera doublement efficace.

### L'organisation en packages

L'approche objet telle que nous l'avons vue amorce un principe&nbsp;: avec la factorisation vient l'idée d'atomicité.

Concevez votre code de façon qu'il implémente une idée, une responsabilité. Nous avons vu dans la formation « Introduction à l'algorithmique avec Java » et les tests unitaires que les fonctions devaient le plus possible être unitaires et que cela servait la réutilisabilité du code et facilitait son débuggage.

Au niveau de la classe il est important de séparer les responsabilités&nbsp;: par exemple une cafetière est un agrégat complexe. Ça n'est pas un tout qui gère le café, l'affichage, le nettoyage mais un ensemble de composants (le circuit d'eau, l'écran, _et cetera_) qui communiquent entre eux. Ce qui permet aux composants de communiquer entre eux c'est leur interface. Si un composant dysfonctionne il est possible de le remplacer par n'importe quel composant qui a la même interface.

Mais de ce fait une classe est un objet restreint. Éventuellement une classe sert à réaliser une responsabilité d'un ordre supérieur, qu'on retrouve au niveau du `package`.

À n'importe quel niveau de package qu'on se situe, ce principe de responsabilité se retrouve&nbsp;:

```
java
 └─ util
     ├─ concurrent
     .
     .
     └─ function
         ├─ BiConsumer
         ├─ BiFunction
         ├─ BinaryOperator
         ├─ BiPredicate
         .
         .
         .
         └─ UnaryOperator
```

Le package de base est `java`, sans surprise. Le package `util` regroupe toutes les classes qui servent un but technique (utilitaire), éventuellement dans des sous-packages comme `concurrent` ou `function`.

Enfin le package de dernier niveau comporte des classes qui chacune remplissent un rôle très précis ou des interfaces dont le contrat est très très spécifique.

### Les modules

Java propose enfin (mais nous ne le détaillons pas ici) une organisation du code en module.

Un module est un ensemble de packages (avec des classes dedans) identifié par un nom unique. Il spécifie les packages qu'il contient et qui sont exportés pour(utilisables par) les autres modules.

Il implémente une responsabilité très vaste, réalisée dans l'ensemble des packages qu'il contient.
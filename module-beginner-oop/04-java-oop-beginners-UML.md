# Modélisation objet et _Unified Modeling Language_

La plupart des problèmes sont difficiles à conceptualiser et encore plus à implémenter. Le moindre projet comprend plusieurs centaines voire plusieurs milliers de classes. Par exemple, le framework _Spring_ comporte environ 1&nbsp;500&nbsp;000 lignes de code sur une vingtaine de modules (on verra ce que c'est) comportant chacun des centaines de classes, pour un total de plus de 8&nbsp;000 classes.

Il est donc très important de pouvoir décrire en amont ce qui sera fait&nbsp;:

1. Pour anticiper les raisons de changer du code.
1. Pour structurer la solution.
1. Pour choisir les solutions les plus simples.

Et en aval, pour documenter le code.

La structuration du code en packages et éventuellement en modules est très importante pour ne pas se perdre.

Une autre manière de faire est de décrire l'architecture et le comportement du code avec un langage. Il en existe plusieurs (LePUS3 en est un) mais le plus connu est UML&nbsp;: _Unified Modeling Language_.

Ce langage, comme les autres, ne doivent pas préjuger du langage dans lequel seront réalisés les processus. Un langage de modélisation n'est pas couplé à un langage de développement (de réalisation).

## _Unified Modeling Language_

### Le diagramme de classes

Le diagramme de classes permet de décrire les classes et interfaces d'un système d'information.

Explorons ensemble le diagramme suivant&nbsp;:

!["alt text"](/module-beginner-oop/img/class-moka.svg "Diagramme de classes simplifié d'une cafetière moka")

Il a été généré sur l'outil [plantuml](https://www.plantuml.com/plantuml/uml/) avec le code suivant&nbsp;:

```
@startuml
interface Cafetiere
Cafetiere : faitCafe()

abstract CafetiereItalienne

Cafetiere <|-- CafetiereItalienne

CafetiereItalienne : ReservoirEau reservoirEau
CafetiereItalienne : placerSurFeu(int temperature, int temps)


class CafetiereItalienneModele1

class CafetiereItalienneModele2

CafetiereItalienne <|-- CafetiereItalienneModele1
CafetiereItalienne <|-- CafetiereItalienneModele2

class Verseuse
CafetiereItalienne "1" o- "1" Verseuse : comporte

class Cheminee
Verseuse "1" *- "1" Cheminee : composé de
@enduml
```

Les éléments importants sont&nbsp;:

1. Les types de classes représentés&nbsp;: interfaces, classes abstraites et classes concrètes.
1. Les relations entre les objets&nbsp;:
    1. Héritage&nbsp;: représentée par une flèche avec une tête triangulaire (open headed arrow) ⇾, comme entre `CafetiereItalienne` et `CafetiereItalienneModele1`.
    1. Réalisation, implémentation&nbsp;: représentée par une flèche avec une tête triangulaire et un trait en pointillé, comme entre `Cafetiere` et `CafetiereItalienne`.
    1. Agrégation&nbsp;: l'objet `X` qui agrège des objets `Y` comporte ces objets mais il n'y a pas de dépendance entre les objets `Y` et les `X` qui les agrègent. Par exemple une page web comporte des articles, mais la suppression d'un article n'entraîne pas la destruction de la page web. Cette relation est dénotée par une flèche dont la pointe est un losange creux, comme entre `CafetiereItalienne` et `Nom`.
    1. Composition&nbsp;: c'est une agrégation forte, dans le sens où la suppression de l'objet composite entraîne la suppression de l'objet composant, comme entre un immeuble et les appartement qui le composent. Cette relation est dénotée par une flèche dont la pointe est un losange plein, comme entre `CafetiereItalienne` et `Verseuse`.
    1. D'autres relations comme l'association et la dépendance sont également représentables.

    Chaque relation qui n'est pas une relation d'extension (héritage, réalisation) doit être nommée par un groupe verbal unique, éventuellement suffixé d'un numéro pour éviter les doublons.
1. L'arité des relations indique, pour les relations de composition et d'agrégation, à combien de `X` chaque `Y` peut correspondre et réciproquement. Par exemple une cafetière italienne a un nom mais un nom peut correspondra à plusieurs cafetières italiennes.

### Le diagramme de séquences

Le diagramme de séquences permet de représenter la suite ordonnée des interactions entre des acteurs.

Prenons l'exemple suivant fait avec [mermaid](https://mermaid.live/edit)&nbsp;:

!["alt text"](/module-beginner-oop/img/sequence-dormir.svg "Diagramme de séquence de l'enfant qui veut une histoire")

Il a été généré avec ce code sous mermaid&nbsp;:

```mermaid
sequenceDiagram
    actor Adorable Enfant
    actor Moi
    loop Jusqu'à ce que je réponde
        Adorable Enfant ->>+ Moi: Tu dors ?
    end
    Moi ->> Adorable Enfant: Non...
    loop Jusqu'à ce que je n'en puisse plus
        Adorable Enfant ->> Moi: Raconte moi une histoire !
        Moi ->>+ Montre: Quelle heure est-il ?
        Montre ->>- Moi: Bien trop tard
        Moi ->> Adorable Enfant: D'accord...
        Moi ->> Adorable Enfant: Il était une fois...
    end
    Adorable Enfant ->> Moi: Oh merci ! Maintenant j'ai envie de dormir !
    Moi ->> Moi: Ben moi plus du tout
    Moi ->>- Adorable Enfant: Dors bien mon amour !
```

Les éléments importants sont&nbsp;:

1. Les participants, qui peuvent avoir différents profils (acteur, entité, base de données, _et cetera_).
1. Les messages, symbolisés par des flèches entre participants, avec le message véhiculé.
1. Les lignes de vies qui identifient l'espace durant lequel un participant est actif. Par exemple sur le diagramme la `Montre` est active pendant un bref moment.
1. Les séquences d'interactions peuvent être regroupées dans des cadres.

### Le diagramme d'activité

### Le diagramme états-transition

## Outils de modélisation

PlantUML, mermaid, draw.io
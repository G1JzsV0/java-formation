[[_TOC_]]

# Gestion des fichiers

## L'interface `java.nio.file.Path`

L'interface `Path` contractualise la notion de chemin vers un fichier ou un répertoire dans un système de fichier. L'utilisation de `Path` ne dépend pas du système mais l'implémentation choisie _at runtime_, elles, est dépend du système.

Pour obtenir un `Path`, on peut utiliser le _helper_ `Paths`&nbsp;:

```Java
// Sous windows
Path somePath = Paths.get("C:/")
// Sous Linux
Path someOtherPath = Paths.get("/dev");
```

Ou bien la fabrique `Path.of`&nbsp;:
```Java
URI someUri = new URI("file:///C:/Users");
Path somePath = Path.of(someUri);
```

Path étend `Iterable<Path>`, `Comparable<Path>`

Les opérations permises par les implémentations sont&nbsp;:

1. La concaténation de morceaux de chemins `C:/` + `users` → `C:/users`.
1. L'exploration de l'arborescence du chemin (parents, …).
1. L'obtention de sous-chemins, du chemin relatif, …

Et d'autres, contractualisées de façon non exhaustive par ces méthodes&nbsp;:

| Méthodes | Type de retour | Rôle |
|-|-|-|
| `startsWith`<br>`endsWith` | `boolean` | Est-ce que ce chemin commence (finit) par tel chemin&nsbp;? |
| `toString`<br>`toUri`<br>`toFile` | `String`<br>`URI`<br>`File` | Renvoit une chaîne de caractère (une URI, un fichier) associé à cet objet |
| `resolve`<br>`resolveSibling` | `Path` | Concatène ce chemin avec le nom fourni en paramètre<br>Remplace la fin de ce chemin par le nom fourni en paramètre |
| `getParent`<br>`getRoot` | `Path` | Renvoie le chemin parent (racine) |
| `getName`<br>`subpath` | `String`<br>`Path` | Renvoie le _i_-ième nom du chemin<br>Renvoie le sous chemin entre les indices _i_ et _j_ |

## La classe `java.io.File`

`File` contractualise et implémente une représentation d'un fichier dans un système de fichiers. Cette représentation permet d'interagir avec toutes sortes de fichiers, sans offrir de services particuliers.

Par exemple&nbsp;:

1. Vous pouvez savoir si le fichier `e5qf45qg.txt` existe, parce que c'est une chose qu'on veut potentiellement savoir de tout fichier.
1. Vous ne pouvez pas dézipper le fichier `q9f1vvqs74.zip` parce que c'est un service spécifique aux fichiers zip.

Les fonctionnalités offertes sont les suivantes&nbsp;:

| Méthodes | Type de retour | Rôle |
|-|-|-|
| `canExecute`<br>`canRead`<br>`canWrite` | `boolean` | Teste les droits de l'appli sur le fichier |
| `setExecutable`<br>`setReadable`<br>`setReadOnly`<br>`setWritable` | `boolean` | Attribue les droits sur ce fichier |
| `createNewFile`<br>`delete`<br>`deleteOnExit` | `boolean` | Crée (détruit) le fichier s'il n'existe pas (s'il existe, s'il existe et à lorsque cette JVM s'arrêtera) |
| `static createTempFile` | `File` | Crée un fichier temporaire |
| `exists` | `boolean` | Existence du fichier |
| `getAbsoluteFile`<br>`getAbsolutePath` | `File`<br>`String` | Renvoie le chemin absolu vers ce fichier |
| `getCanonicalFile`<br>`getCanonicalPath` | `File`<br>`String` | Renvoie le chemin canonique vers ce fichier |
| `getTotalSpace`<br>`getusableSpace` | `long` | Donne l'espace total/disponible |
| `isAbsolute`<br>`isDirectory`<br>`isFile`<br>`isHidden` | `boolean` | Cet objet est-il absolu (un répertoire, un fichier, caché) |
| `lastModified` | `boolean` | Date de dernière modification |
| `setLastModified` | `boolean` | Change la date de dernière modification |
| `length` | `long` | Taille en octets |
| `list`<br>`listFiles` | `String[]`<br>`File[]` | Liste (sous forme de _array_) les fichiers de cet objet s'il dénote un répertoire |
| `renameTo` | `boolean` | Renomme le fichier |
| `putAll` | `void` | Ajoute le contenu d'une `Map<K, V>` à cet objet |
| `toPath`<br>`toString`<br>`toURI` | `Path`<br>`String` | Construit un chemin ou une URI qui dénote cet objet |

Comme on le voit, c'est une interface d'assez bas niveau qui ne permet même pas, par exemple, de lire un fichier. C'est normal&nbsp;: « lire un fichier » ne veut rien dire en soi. Je peux lire un fichier zip avec Notepad++, ou avec LibreOffice, ou avec 7-zip, chacune de ces applications me donnera un résultat appréciable (mais pas également appréciable).

C'est pourquoi Java mets à disposition de puissantes API pour lire et écrire de nombreux types de fichiers.

## Lire

### Lire des fichiers texte

#### `InputStreamReader`

La classe `InputStreamReader` permet de lire un flux d'octets et de le convertir en un flux de caractères, en utilisant un `Charset` donné.

Pour obtenir un `InputStreamReader` à partir d'un fichier, il est nécessaire de lui fournir un `InputStream` associé à ce fichier, c'est-à-dire un `FileInputStream`&nbsp;:

```Java
try (
  var fis = new FileInputStream("<some path to a file>");
  var isr = new InputStreamReader(fis)
) {
  isr.read(); // reads one character, very handy for your 1 TB big data file
  char buffer = new char[1024 * 1024];
  isr.read(buffer, 0, 1024 * 1024); // reads up to 1024*1024 character, that's more handy but still ugly
} catch (FileNotFoundException e) {
  // do something
} catch (IOException e) {
  // do something
}
```

Remarques&nbsp;:

1. `InputStreamReader` est closeable, donc il est nécessaire de gérer cette ressource (_try-with-resources_).
1. Cette classe offre deux méthodes de lecture, `read()` qui lit un caractère et `read(char[], int, int)` qui lit les caractères pour les écrire dans un tableau. C'est une classe de bas niveau, à n'utiliser que dans les cas suivants&nbsp;:
    1. Aucune autre classe ne vous permet de lire le fichier comme vous l'entendez.
    1. Vous avez besoin de _cette_ classe pour des raisons d'optimisation.

#### `FileReader`

La classe `FileReader` permet de lire un fichier texte. Elle étend `InputStreamReader` et la rend un peu plus facile à utiliser&nbsp;:
```Java
try (
  var fr = new FileReader("<some path to a file>")
) {
  fr.read(); // reads one character, very handy for your 1 TB big data file
  char buffer = new char[1024 * 1024];
  fr.read(buffer, 0, 1024 * 1024); // reads up to 1024*1024 character, that's more handy but still ugly
} catch (FileNotFoundException e) {
  // do something
} catch (IOException e) {
  // do something
}
```

L'interface est la même, l'instanciation est facilitée.

##### Exercice

Lire un fichier texte et l'écrire tel quel dans la console. Choisissez un petit fichier, de moins de 1 ko.



### Lire des fichiers compressés




## Écrire

### Écrire des fichiers texte




### Écrire des fichiers compressés






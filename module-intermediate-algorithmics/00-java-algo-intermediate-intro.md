[[_TOC_]]

# Introduction

## Au sujet de ce document

Ce document est destiné au formateur et à toute personne curieuse des thèmes qu'elle aborde. En particulier, le contenu de ce document couvre largement le programme de la formation. Il ne peut pas être dispensé dans le détail, en deux jours, avec les exercices.

Il est conçu pour offrir au formateur l'information nécessaire pour dispenser la formation mais également le contexte et les détails qui lui permettent de maîtriser son sujet aussi pleinement que possible et éventuellement d'anticiper certaines questions.

Les détails qui devraient être omis en première approche sont précédés par l'émoji « &#129488; » mais certains autres détails non marqués pourraient et devraient l'être aussi, selon le contexte.

Les points d'attention sont précédés par l'émoji « &#9888;&#65039; ». 

## Prérequis

### Obligatoire

Cette formation requiert une connaissance de base du langage Java, telle que présentée dans la formation « Introduction à l'algorithmique avec Java ».

Du point de vue logiciel, les prérequis sont&nbsp;:
1. Eclipse, IntelliJ, ou un autre IDE installé et fonctionnel.
2. JDK 17 ou supérieur installé.
3. Pour suivre les exercices&nbsp;: client git.

### Recommandé

Il est vivement recommandé d'avoir une connaissance de la programmation objet telle que présentée dans la formation « Introduction à la programmation objet avec Java ». Cette connaissance est recommandée non pas dans un but de modélisation mais pour comprendre le vocabulaire de cette formation et l'utilisation qui y sera faite des objets.

## But

Le but de cette formation est de vous aider à écrire, proprement et avec uniquement les fonctionnalités natives du langage, des programmes qui interagissent avec l'extérieur de la JVM&nbsp;: fichiers, bases de données. Pour cela, vous devrez vous approprier des notions qui vous aideront à structurer vos programmes.

À l'issue de cette formation, vous connaîtrez&nbsp;:
1. La JVM et en particulier le fonctionnement de la pile → nécessaire pour comprendre les exceptions.
1. Les types `Throwable`, `Exception`, `Error`, nécessaire pour gérer correctement le _control flow_ lors d'opération complexes comme par exemple des interactions avec l'extérieur de la JVM.
1. Les collections, indispensables pour faire des séries d'opérations sur des objets d'un même type.
1. Les fichiers, parce que tout est fichier.
1. La structure d'un projet, le `classpath`, les ressources du projet, la configuration du projet.
1. JDBC (Java EE Database Connectivity), pour communiquer avec les bases de données.

La formation est axée autant sur l'apprentissage des notions techniques que sur la compréhension de ce qui fait un code de qualité.

## Initialisation

1. Créez un répertoire où vous voulez importer le projet qui contient les exercices.
2. Ouvrez un terminal comme Git Bash ou un équivalent (faites un clic droit à l'intérieur du répertoire et cliquez sur _Git bash here_).
3. Dans le terminal, tapez ou copiez (`[Shift] + [Insert]`) la ligne de code suivante&nbsp;: `git clone git@<TODO>`.

Sous Eclipse&nbsp;: TODO


Sous IntelliJ&nbsp;: TODO
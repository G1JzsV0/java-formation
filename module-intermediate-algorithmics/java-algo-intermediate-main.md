# Java intermédiaire

[[_TOC_]]

## Le _framework_ Collection

Il est difficile de parler de programmation sans parler de collections d'objets, c'est-à-dire de structures qui contiennent des objets et qui permettent d'y accéder.

Le diagramme ci-dessous présente de façon succincte et partielle la structure du _framework Collection_&nbsp;:

![alt text](module-intermediate-algorithmics/img/collection-framework-light.svg "Diagramme de classes simplifié du framework Collection")

1. Toutes les classes étendent `Object`, et en particulier les deux méthodes `int heshCode()` et `boolean equals()`.
1. L'interface `Map<K, V>` couvre les besoins des dictionnaires ou des tables d'association. Ce sont des structures conçues pour stocker des paires clé, valeur.
1. L'interface `Collection<T>` couvre les besoins des conteneurs sur lesquels il est possible de stocker des objets, d'y accéder, et de les parcourir.

### L'interface `Map<K, V>` et ses principales implémentations

#### Présentation rapide

Une `Map<K, V>` est une structure qui contient des paires d'objets. Le premier élément de la paire est appelée **clé** et la deuxième, **valeur**. Le type générique `K` est celui des clés et le type générique `V` est celui des valeurs&nbsp;:

```Java
Map<K, V> m = Map.of("MVCCC", 1800); // K = String, V = Integer
```

Une clé correspond à une valeur unique&nbsp;:
```Java
Map<String, String> m = new HashMap<>(); // une implémentation de Map est HashMap
m.put("Une clé", "Une valeur");
System.out.println(m.get("Une clé")); // affiche "Une valeur"
m.put("Une clé", "Une autre valeur");
System.out.println(m.get("Une clé")); // affiche "Une autre valeur"
```

`Map<K, V>` permet également d'itérer sur les paires (clé, valeur), appelées entrées et de type `Map.Entry<K, V>`. L'ordre dans lequel cette itération se fait dépend de l'implémentation et peut ne pas être déterministe&nbsp;:

```Java
Map<String, String> m = Map.of(
    "Une clé", "Une valeur",
    "Une autre clé", "Une autre valeur",
    "Une dernière clé", "Une dernière valeur"
);
for (Map.Entry<String, String> e: m.entrySet()) {
    System.out.println(e.getKey() + " → " + e.getValue());
}
```
```
// Affiche dans cet ordre ou un autre
Une autre clé → Une autre valeur
Une clé → Une valeur
Une dernière clé → Une dernière valeur
```

#### &#129488; Présentation rapide des méthodes de l'interface

| Méthodes | Type de retour | Rôle |
|-|-|-|
| `clear` | `void` | Enlève toutes les entrées |
| `size` | `int` | Renvoie la taille de la `Map<K, V>` |
| `compute`<br>`computeIfAbsent`<br>`computeIfPresent` | `V` | Modifie une valeur associée à une clé |
| `isEMpty` | `boolean` | Est-ce que la `Map<K, V>` est vide&nbsp;? |
| `containsKey`<br>`containsValue` | `boolean` | Interroge la `Map` sur son contenu |
| `keySet`<br>`entrySet`<br>`values` | `Set<K>`<br>`Set<Map.Entry<K, V>`<br>`Collection<V>` | Récupère les clés, les entrées, les valeurs |
| `get`<br>`getOrDefault` | `V` | Récupère la valeur associée à une clé |
| `of`<br>`ofEntries` | `Map<K, V>` | Génère une `Map<K, V>` avec les clés et valeurs spécifiées, avec les entrées spécifiées.<br>L'objet renvoyé est immutable. |
| `merge`<br>`put`<br>`putIfAbsent`<br>`replace` | `V` | Associe une valeur à une clé ou remplace une association |
| `remove` | `V` | Enlève une clé et sa valeur associée |
| `entry` | `Map.Entry<K, V>` | Génère une entrée |
| `putAll` | `void` | Ajoute le contenu d'une `Map<K, V>` à cet objet |

#### L'implémentation `HashMap<K, V>`

`HashMap<K, V>` est une implémentation de `Map<K, V>` qui permet un accès aléatoire (direct) aux associations qu'elle stocke.

> &#129488; Pour y parvenir `HashMap<K, V>` stocke les entrées dans une table de hachage&nbsp;:
>
> | Hash | Clé | Valeur |
> |-|-|-|
> | `28` | `Clé 1` | `Valeur 1` |
> | `111` | `Clé 4` | `Valeur 4` |
> | `53` | `Clé 3` | `Valeur 3` |
> | `48` | `Clé 2` | `Valeur 2` |
>
> L'implémentation de la table de hachage repose sur un tableau (nommé `table`) `Node<K, V>[]` et une fonction `hash`.
>
> Pour insérer la valeur associée à la clé `Clé 1`, la fonction `hash` calcule l'index de cette clé, ici `28`. L'assocation (un objet `Map.Entry<K, V>`) `Clé 1 → Valeur 1` sera donc positionnée dans un tableau, à l'index 28.
>
> C'est ainsi qu'un accès aléatoire (direct) et non séquentiel est permis sur des objets qui ne sont pas des entiers.

Si beaucoup d'éléments sont stockés dans la `HashMap<K, V>`, elle doit être redimensionnée et dans ce cas un nouveau tableau `table` est créé et les éléments de l'ancien, repositionnés dans le nouveau tableau.

##### Avantages

Accès dynamique aux éléments (pas besoin de parcourir une structure).

##### Inconvénients

Nécessité d'agrandir la `Map` de temps en temps, ce qui est coûteux en temps.

##### Exemple de ca d'utilisation

TODO

#### L'implémentation `TreeMap<K, V>`

`TreeMap<K, V>` est une implémentation de `Map<K, V>` dans laquelle les entrées sont triées selon un `Comparator<K>` sur les clés. La structure sous-jacente est un arbre bicolore, qui est une structure facilitant la recherche d'éléments ordonnés.

les méthodes suivantes sont ajoutées&nbsp;:

| Méthodes | Type de retour | Rôle |
|-|-|-|
| `ceilingKey`/`higherKey`<br>`floorKey`/`lowerKey` | `K` | Trouve la plus petite (resp. la plus grande) clé plus grande (resp. plus petite) que celle passée en paramètre |
| `firstKey`<br>`lastKey` | `K` | Retrouve la plus petite (la plus grande) clé |
| `pollFirstEntry`<br>`pollLastEntry` | `V` | Retire et retourne la plus petite (resp. plus grande) entrée |
| `comparator` | `Comparator<K>` | Le comparateur sous-jacent |
| `headMap`<br>`subMap`<br>`tailMap` | `NavigatableMap<K, V>` | Retourne une portion de cet objet |
| `descendingMap`<br>`descendingKeySet` | `NavigatableMap<K, V>`<br>`Set<K>` | Retourne une vue de cet objet (resp. des clés de cet objet) ordonnés selon l'ordre inverse |
| `ceilingEntry`<br>`firstEntry`<br>`floorEntry`<br>`higherEntry`<br>`lastEntry`<br>`lowerEntry` | `Map.Entry<K, V>` | Comme les méthodes suffixées avec `key`, mais retrouve une entrée plutôt qu'une clé |

##### Avantages

Il est très facile de rechercher des éléments selon leur ordre.

##### Inconvénient

Les opérations d'insertion et de suppression sont plus coûteuses que pour la `HashMap<K, V>`. L'accès à tout élément se fait par un parcourt de structure, peu coûteux certes mais pas immédiat.

##### Exemple de cas d'utilisation

TODO

### `Collection<T>` et ses implémentations

#### Qu'est-ce qu'une collection&nbsp;?

Une collection répond à plusieurs besoins&nbsp;:
1. Stocker des objets (d'un même type).
1. Insérer des objets.
1. Trouver des objets.
1. Enlever des objets.
1. Parcourir les objets.

Selon le cas d'utilisation, d'autres fonctionnalités peuvent être requises&nbsp;:
1. Accès aléatoire (direct) ou séquentiel.
1. Ordre d'accès aux éléments.
1. Unicité des éléments.
1. Éventuellement d'autres aspects comme l'accès concurrent, que nous ne verrons pas ici.

Pour chaque cas d'utilisation il existe au moins une implémentation de `Collection<T>` conçue pour offrir les meilleures performances.

Dans tous les cas, les implémentations de `Collection<T>` étendent `Iterable<T>` pour garantir le parcours des élements, et `Collection<T>`, bien entendu, qui spécifie les opérations précitées sur les collections.

#### L'interface `Iterable<T>`

L'interface `Iterable<T>` décrit le contrat d'un objet qui permet de parcourir des éléments d'un type `T`. Elle spécifie trois opérations&nbsp;:

| Méthodes | Type de retour | Rôle |
|-|-|-|
| `forEach(Consumer<T>)` | `void` | Parcourt l'ensemble des éléments de l'objet et sur chacun d'eux exécute l'action du `Consumer<T>` |
| `iterator()` | `iterator<T>` | Retourne un moyen d'itérer sur les éléments de cet objet |
| &#129488; `splitarator()` | `Splitarator<T>` | Retourne un moyen plus polyvalent d'itérer sur les éléments de cet objet |

L'utilisation d'un `Iterator<T>` se fait ainsi&nbsp;:

```Java
Iterable<String> iterable = List.of("a", "b", "c", "d", "e");
Iterator<String> iterator = iterable.iterator();
while (iterator.hasNext()) { // hasNext répond à la question « y a-t-il encore un élément à parcourir ? »
    var element = iterator.next(); // next récupére l'élément suivant
    System.out.println(element);
}
```

L'interface `Iterable<T>` permet également l'utilisation de la syntaxe suivante&nbsp;:

```Java
Iterable<String> iterable = List.of("a", "b", "c", "d", "e");
for (String element : iterable) { // merci Iterable
    System.out.println(element); // affiche un élément par ligne
}
```

#### L'interface `Collection<T>`

L'interface `Collection<T>` décrit le contrat d'un objet qui contient des éléments d'un type `T`. Elle spécifie les méthodes suivantes&nbsp;:

| Méthodes | Type de retour | Rôle |
|-|-|-|
| `add(T t)`<br>`addAll(Collection&lt;T&gt; t)` | `boolean` | Ajouter un ou des éléments à cette collection |
| `boolean isEmpty()`<br>`int size()` | `boolean`<br>`int` | La collection est-elle vide ?<br>Combien a-t-elle d'éléments ?  |
| `contains(Object o)`<br>`containsAll(Collection<?> c)` | `boolean` | La collection contient-elle l'élément (les éléments) donné(s) en paramètre(s) ? |
| `clear()` | `void` | Vider la liste |
| `parallelStream()`<br>`stream()` | `Stream<T>` | Renvoie un stream des éléments de cette collection |
| `remove(Object o)`<br>`removeAll(Collection<?> c)`<br>`removeIf(Predicate<? super T> filter)` | `boolean` | Enlève un élément (des éléments) d'une collection |
| `retainAll(Collection<?> c)` | `boolean` | Enlève tous les éléments sauf ceux passés en paramètre |
| `toArray()`<br>`toArray(IntFunction<T[]> generator)`<br>`toArray(T[] a)` | `Object[]`<br>`T[]`<br>`T[]` | Renvoie un tableau des éléments non typés (resp. typés, resp. typés) de cette collection |

Cette interface spécifie juste que ces opérations peuvent être faites, pas _comment_ elles peuvent être faites. Le choix de l'implémentation dépend de ce qu'on veut faire avec la collection, et a un impact sur&nbsp;:

1. L'unicité des éléments.
1. Le temps d'ajout, d'obtention par son index, de suppression d'un élément.
1. L'ordre de parcours des éléments.
1. Le temps de recherche d'un élément par sa valeur.

Le diagramme suivant résume, selon les fonctionnalités souhaitées, l'implémentation de la collection que vous pouvez choisir&nbsp;:

![alt text](module-intermediate-algorithmics/img/collection-choice.svg "Choix de l'implémentation de la collection")

Ce choix est détaillé dans les sections qui suivent.

#### L'interface `List<T>`

L'interface `List<T>` contractualise une collection avec un accès aléatoire à ses éléments pour l'ajout, la modification et la suppression.

#### La classe `ArrayList<T>`

`ArrayList<T>` implémente ce contrat en stockant les éléments dans un `T[]`. De ce fait il offre les fonctionnalités suivantes&nbsp;:

1. Accès immédiat au _i_-ième élément avec `get(int)` (par accès au _i_-ième élément du tableau).
1. Modification immédiate du _i_-ième élément (par mutation du _i_-ième élément du tableau).
1. Insertion et suppression d'un élément lente (tous les éléments suivants doivent être décalés d'un cran vers la gauche).
1. Recherche par valeur lente (parcours du tableau nécessaire).
1. Par ailleurs l'ajout d'un élément en fin de tableau peut nécessiter un redimensionnement de celui-ci, occasionnant une perte de temps (allocation d'un nouveau tableau, réécriture de l'ancien tableau dans le nouveau).
1. L'ordre d'itération est celui de l'insertion.

Le `ArrayList<T>` est préconisé lorsqu'un accès séquentiel, déterminé est souhaité et qu'il n'y aura pas de recherche à faire sur son contenu.

##### Exemple programmatique

```Java
var list = new ArrayList<String>();
list.add("first");
list.set(0, "First");
list.add("Second");
list.add(0, "Before the first"); // Never do that! ArrayList has to move First to second place and Second to third place
list.indexOf("First"); // Never do that ! It takes forever to find "First"
// yields 2 as list is basically equal to ["Before the first", "First", "Second"]
```

##### Exemples de cas d'utilisation

1. Mon service récupère les commandes d'un client identifié par son numéro de client. Le service me fournit les commandes triées selon l'ordre chronologique. Ces commandes sont traitées par un autre service, dans l'ordre chronologique. Très vraisemblablement c'est une `List<T>` implémentée par `ArrayList<T>`.
1. Mon processus de traitement de fichiers est composé de 8 étapes. Elles sont probablement stockées dans une `ArrayList<T>`.

#### La classe `LinkedList<T>`

C'est une implémentation de `List<T>` qui offre un accès privilégié au premier et au dernier élément. Par ailleurs son implémentation rend l'ajout et la suppression d'éléments en tête en en queue très rapide.

Le schéma suivant résume l'implémentation d'une `LinkedList<T>`&nbsp;:

![alt text](module-intermediate-algorithmics/img/collection-linkedlist.svg "Implémentation d'une LinkedList<T>")

Les deux seuls attributs de LinkedList sont `first` et `last`. Ils suffisent à assurer le fonctionnement de la classe&nbsp;:

1. Opérations sur le premier et dernier élément immédiates (accès direct aux attributs `first` et `last`).
1. Recherche par valeur lente (parcourt de la chaîne obligatoire).
1. Accès aléatoire (au _i_-ième élément) possible mais lent (parcourt de la chaîne obligatoire).
1. Contrairement à `ArrayList<T>`, aucun redimensionnement n'est nécessaire mais chaque ajout d'un élément nécessite la création d'un objet `Node<T>` (donc l'appel à un constructeur).
1. L'ordre d'itération est déterminé par les insertions.

##### Exemple programmatique

TODO

##### Exemples de cas d'utilisation

1. Mon application permet d'écouter des listes de morceaux de musique. Ces listes sont vraisemblablement stockées dans une `LinkedList<T>` une fois qu'elles sont générées.
1. Mon navigateur (écrit en Java&nbsp;) stocke les pages que j'ai visitées et me permet de passer d'une page à la précédente ou à la suivante. Elles sont vraisemblablement stockées dans une `LinkedList<T>`.
1. Mon application permet d'éditer des photos. Elle est dotée d'une fonctionalité incroyable qui permet d'annuler une modification, ou même de rétablir une modification annulée. Les modifications sont probablement stockées dans une `LinkedList<T>`.

#### L'interface `Set<T>`

L'interface `Set<T>` contractualise une collection qui assure l'unicité de ses éléments.

#### La classe `TreeSet<T>`

La classe `TreeSet<T>` assure un tri sur ses éléments, si une des deux conditions suivante est assurée&nbsp;:

- les éléments implémentent `Comparable<T>` pour être comparés entre eux,
- le `TreeSet<T>` est initialisé avec un `Comparator<T>` pour comparer les éléments.

Cette classe permet de naviguer parmi les éléments de façon déterministe. La classe assure le fonctionnement suivant&nbsp;:

1. Les opérations de base (ajout, suppression et recherche) se font en un temps raisonnable (O(_log_(n))).
1. L'ordre d'itération est déterminé par l'ordre intrinsèque aux objets (`Comparable<T>`) ou par l'ordre intrinsèque au `TreeSet<T>` (`Comparator<T>`).

##### Exemple programmatique
Je possède des trucs, qui sont des objet qui possèdent une forme, une couleur et une date de création. Mon but ici est de les trier selon l'appréciation que je leur porte.

Le critère déterminant pour savoir si j'aime un objet `Thing` c'est sa couleur. Je les préfère bleus. En cas d'égalité je considère la forme. Les ronds sont parfaits et je déteste les étoiles.

On voit ici que deux objets peuvent ne pas être égaux au sens `equals` mais malgré tout être « égaux » au sens de `compareTo`. Nous verrons que ça a une importance.

```Java
static class Thing implements Comparable<Thing> {

  LocalDateTime dateOfCreation = LocalDateTime.now();
  Colour colour;
  Shape shape;

  public Thing(Colour colour, Shape shape) {
    this.colour = colour;
    this.shape = shape;
  }

  @Override
  public int compareTo(Thing other) {
    log.info("Comparing " + this + " with " + other);
    int cmp = this.colour.compareTo(other.colour);
    return cmp == 0 ? this.shape.compareTo(other.shape) : cmp;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Thing thing)) return false;

    if (!dateOfCreation.equals(thing.dateOfCreation)) return false;
    if (!colour.equals(thing.colour)) return false;
    return shape.equals(thing.shape);
  }

  @Override
  public String toString() {
    return "Thing{" +
            "dateOfCreation=" + dateOfCreation +
            ", colour=" + colour +
            ", shape=" + shape +
            '}';
  }
}
```

Décrivons un peut plus la forme et la couleur. J'ai dit préférer le bleu au vert, une `enum` avec trois couleurs (vert, rouge, bleu) fera l'affaire. Une `enum` de type `T` implémente naturellement l'interface `Comparable<T>`, l'ordre étant celui dans lequel les éléments sont énumérés.
```Java
enum Colour {
  GREEN("green", 5),
  RED("red", 8),
  BLUE ("blue", 10);

  int likeability;
  String name;

  Colour(String colour, int likeability) {
    this.name = colour;
    this.likeability = likeability;
  }

  public int likeability() {
    return likeability;
  }

  public String toString() {
    return this.name;
  }
}

enum Shape {
  STAR("star", 2),
  SQUARE("square", 6),
  PENTAGON("pentagon", 7),
  ROUND("round", 10);

  int likeability;
  String name;

  Shape(String shape, int likeability) {
    this.name = shape;
    this.likeability = likeability;
  }

  public int likeability() {
    return likeability;
  }

  public String toString() {
    return this.name;
  }
}
```



Enfin je popule mon `TreeSet<Thing>` avec des `Thing`&nbsp;:
```Java
var blueSquareThing = new Thing(Colour.BLUE, Shape.SQUARE);
var greenStarThing = new Thing(Colour.GREEN, Shape.STAR);
var anotherBlueSquareThing = new Thing(Colour.BLUE, Shape.SQUARE);
System.out.println("blueSquareThing.equals(anotherBlueSquareThing) == " + blueSquareThing.equals(anotherBlueSquareThing));
System.out.println("blueSquareThing.compareTo(anotherBlueSquareThing) == " + blueSquareThing.compareTo(anotherBlueSquareThing));
var redPentagonThing = new Thing(Colour.GREEN, Shape.PENTAGON);
var set = new TreeSet<Thing>();
set.add(blueSquareThing);
set.add(greenStarThing);
set.add(anotherBlueSquareThing);
set.add(redPentagonThing);
for (var thing : set) {
  System.out.println(thing);
}
```

Ce programme affiche…

```
blueSquareThing.equals(anotherBlueSquareThing) == false
blueSquareThing.compareTo(anotherBlueSquareThing) == true
Comparing Thing{dateOfCreation=2024-01-29T17:45:25.650451800, colour=blue, shape=square} with Thing{dateOfCreation=2024-01-29T17:45:25.650451800, colour=blue, shape=square}
        (Comparing blue with blue)
        (Comparing square with square)
Comparing Thing{dateOfCreation=2024-01-29T17:45:25.653466100, colour=green, shape=star} with Thing{dateOfCreation=2024-01-29T17:45:25.650451800, colour=blue, shape=square}
        (Comparing green with blue)
Comparing Thing{dateOfCreation=2024-01-29T17:45:25.653466100, colour=blue, shape=square} with Thing{dateOfCreation=2024-01-29T17:45:25.650451800, colour=blue, shape=square}
        (Comparing blue with blue)
        (Comparing square with square)
Comparing Thing{dateOfCreation=2024-01-29T17:45:25.663170400, colour=green, shape=pentagon} with Thing{dateOfCreation=2024-01-29T17:45:25.650451800, colour=blue, shape=square}
        (Comparing green with blue)
Comparing Thing{dateOfCreation=2024-01-29T17:45:25.663170400, colour=green, shape=pentagon} with Thing{dateOfCreation=2024-01-29T17:45:25.653466100, colour=green, shape=star}
        (Comparing green with green)
        (Comparing pentagon with star)
Thing{dateOfCreation=2024-01-29T17:45:25.653466100, colour=green, shape=star}
Thing{dateOfCreation=2024-01-29T17:45:25.663170400, colour=green, shape=pentagon}
Thing{dateOfCreation=2024-01-29T17:45:25.650451800, colour=blue, shape=square}
```

Plusieurs remarques&nbsp;:

1. L'élément `anotherBlueSquareThing` n'a pas été ajouté car il est considéré comme égal à `blueSquareThing` au sens.
1. Chaque insertion d'un élément autre que le premier occasionne des comparaisons pour placer cet élément au bon endroit dans le `Set<Thing>`. En particulier le `greenPentagonThing` a été comparé à chacun des deux autres éléments. La gestion du `TreeSet<T>` a donc un coût.
1. À la fin les éléments sont triés par ordre croissant.

##### Exemples de cas d'utilisation

1. Je récupère une liste de noms que je veux fournir triée à mon client. J'utilise un `TreeSet<T>`.
1. Je veux travailler sur une liste d'objets selon un tri particulier (par exemple le nom, puis la couleur, puis le poids). J'utilise un `TreeSet<T>` avec un comparateur _ad hoc_.

#### La classe `HashSet<T>`

La classe `HashSet<T>` assure l'unicité de ses éléments en les stockant dans une `HashMap<T, Object>`. Ce fonctionnement permet&nbsp;:

1. Un ajout, recherche, suppression d'éléments immédiat (O(1)).
1. Un ordre d'itération non déterministe.
1. Des opérations ensemblistes optimisées (puisque la recherche est immédiate).

L'égalité de deux objets (au sens de `HashSet<T>`) est exactement celui de l'égalité au sens de `HashMap<T>`. Les opérations faites sur `HashSet<T>` sont _déléguées_ à la `HashMap<T>`.

> &#129488; Plus précisément, un objet `t` est stocké comme clé dans la `HashMap<T, Object>` qui implémente le comportement du `HashSet<T>`&nbsp;:
> ```Java
> //--- Classe HashSet
> private Map<T, Object> map = new HashMap<T, Object>();
> private static final Object PRESENT = new Object();
> //--- Utilisons la classe HashSet ailleurs
> var set = new HashSet<String>();
> String s = "some string";
> set.add(s);
> //--- Ce qui se passe réellement dans HashSet
> map.put(s, PRESENT);
> ```
> Seule la clé est utilisée, la valeur de la map comporte un objet sans intérêt.

##### Exemple programmatique

```Java
var set = new HashSet<String>();
set.add("One");
set.add("Two");
set.add("Three");
set.add("One");
for (var s : set) {
    System.out.println(s); 
}
```
Ce code affichera par exemple (notez le `One` qui n'apparaît qu'une fois)&nbsp;:
```
Two
One
Three
```
##### Exemples de cas d'utilisation

1. Je veux trouver les `X` qui sont aussi des `Y`. `HashSet<T>` me permet de faire l'intersection des `X` et des `Y`.
1. J'ai une liste de toutes les visites de clients de l'année. Je veux obtenir les clients qui ont effectué une visite. `HashSet<Client>` est la collection idéale.

## Gestion des fichiers

### L'interface `java.nio.file.Path`

L'interface `Path` contractualise la notion de chemin vers un fichier ou un répertoire dans un système de fichier. L'utilisation de `Path` ne dépend pas du système mais l'implémentation choisie _at runtime_, elles, est dépend du système.

Pour obtenir un `Path`, on peut utiliser le _helper_ `Paths`&nbsp;:

```Java
// Sous windows
Path somePath = Paths.get("C:/")
// Sous Linux
Path someOtherPath = Paths.get("/dev");
```

Ou bien la fabrique `Path.of`&nbsp;:
```Java
URI someUri = new URI("file:///C:/Users");
Path somePath = Path.of(someUri);
```

Path étend `Iterable<Path>`, `Comparable<Path>`

Les opérations permises par les implémentations sont&nbsp;:

1. La concaténation de morceaux de chemins `C:/` + `users` → `C:/users`.
1. L'exploration de l'arborescence du chemin (parents, …).
1. L'obtention de sous-chemins, du chemin relatif, …

Et d'autres, contractualisées de façon non exhaustive par ces méthodes&nbsp;:

| Méthodes | Type de retour | Rôle |
|-|-|-|
| `startsWith`<br>`endsWith` | `boolean` | Est-ce que ce chemin commence (finit) par tel chemin&nsbp;? |
| `toString`<br>`toUri`<br>`toFile` | `String`<br>`URI`<br>`File` | Renvoit une chaîne de caractère (une URI, un fichier) associé à cet objet |
| `resolve`<br>`resolveSibling` | `Path` | Concatène ce chemin avec le nom fourni en paramètre<br>Remplace la fin de ce chemin par le nom fourni en paramètre |
| `getParent`<br>`getRoot` | `Path` | Renvoie le chemin parent (racine) |
| `getName`<br>`subpath` | `String`<br>`Path` | Renvoie le _i_-ième nom du chemin<br>Renvoie le sous chemin entre les indices _i_ et _j_ |

### La classe `java.io.File`

`File` contractualise et implémente une représentation d'un fichier dans un système de fichiers.

## Accès aux bases de données avec JDBC

## API time


&laquo;&nbsp;&nbsp;&raquo;
&#129488;
&#x26A0;
&#9888;&#65039;

## Annexes

### Diagrammes mermaid

#### Synthétique

```Mermaid

classDiagram

    class Iterable["Iterable&lt;T&gt;"]
    <<Interface>> Iterable
    class Object
    class Map["Map&lt;K, V&gt;"]
    <<Interface>> Map
    class HashMap["HashMap&lt;K, V&gt;"]
    class TreeMap["TreeMap&lt;K, V&gt;"]

    Map <|-- HashMap
    Map <|-- TreeMap

    class Collection["Collection&lt;T&gt;"]
    <<Interface>> Collection
    class List["List&lt;T&gt;"]
    <<Interface>> List
    class AbstractList["AbstractList&lt;T&gt;"]
    <<Abstract>> AbstractList
    class ArrayList["ArrayList&lt;T&gt;"]
    class Vector["Vector&lt;T&gt;"]
    class LinkedList["LinkedList&lt;T&gt;"]
    class Set["Set&lt;T&gt;"]
    <<Interface>> Set
    class AbstractSet["AbstractSet&lt;T&gt;"]
    <<Abstract>> AbstractSet
    class HashSet["HashSet&lt;T&gt;"]
    class TreeSet["TreeSet&lt;T&gt;"]
    class Queue["Queue&lt;T&gt;"]
    <<Interface>> Queue
    class Deque["Deque&lt;T&gt;"]
    <<Interface>> Deque
    Iterable <|-- Collection
    Collection <|-- List
    List <|-- AbstractList
    AbstractList <|-- ArrayList
    AbstractList <|-- Vector
    AbstractList <|-- LinkedList
    Deque <|-- LinkedList
    Collection <|-- Set
    Set <|-- AbstractSet
    AbstractSet <|-- HashSet
    AbstractSet <|-- TreeSet
    Collection <|-- Queue
    Queue <|-- Deque

    
```

#### Détaillé

```Mermaid
classDiagram
    class Iterable["Iterable&lt;T&gt;"]  {
        + default void forEach(Consumer&lt;? super T&gt; action)
        + Iterator&lt;T&gt; iterator()
        + default Spliterator&lt;T&gt; spliterator()
    }
    <<Interface>> Iterable
    class Object {
        + boolean equals(Object o)
        + int hashCode()
    }
    class Collection["Collection&lt;T&gt;"] {
        + boolean add(T t)
        + boolean addAll(Collection&lt;T&gt; t)
        + void clear()
        + boolean contains(Object o)
        + boolean containsAll(Collection&lt;?&gt; c)
        + boolean isEmpty()
        + default Stream&lt;T&gt; parallelStream()
        + boolean remove(Object o)
        + boolean removeAll(Collection&lt;?&gt; c)
        + boolean removeIf(Predicate&lt;? super T&gt; filter)
        + boolean retainAll(Collection&lt;?&gt; c)
        + int size()
        + default Stream&lt;T&gt; stream()
        + Object[] toArray()
        + default &lt;T&gt; T[] toArray(IntFunction&lt;T[]&gt; generator)
        + &lt;T&gt; T[] toArray(T[] a)
    }
    <<Interface>> Collection
    class List["List&lt;T&gt;"] {
        + default void addFirst(T t)
        + default void addLast(T t)
        + T get(int index)
        + default T getFirst()
        + default T getLast()
        + int indexOf(Object o)
        + int lastIndexOf(Object o)
        + ListIterator&lt;T&gt; listIterator()
        + ListIterator&lt;T&gt; listIterator(int index)
        + T remove(int index)
        + default T removeFirst()
        + default T removeLast()
        + default void replaceAll(UnaryOperator&lt;T&gt; operator)
        + default List&lt;T&gt; reversed()
        + T set(int index, T element)
        + default void sort(Comparator&lt;? super T&gt; c)
        + List&lt;T&gt; subList(int fromIndex, int toIndex)
        + static &lt;T&gt; List&lt;T&gt; of()
        + static &lt;T&gt; List&lt;T&gt; of(T t1)
        + static &lt;T&gt; List&lt;T&gt; of(T t1, ..., T t10)
        + static &lt;T&gt; List&lt;T&gt; of(T... elements)
    }
    <<Interface>> List
    class AbstractList["AbstractList&lt;T&gt;"]
    <<Abstract>> AbstractList
    class ArrayList["ArrayList&lt;T&gt;"]
    class LinkedList["LinkedList&lt;T&gt;"]
    class Set["Set&lt;T&gt;"] {
        + static &lt;T&gt; Set&lt;T&gt; of()
        + static &lt;T&gt; Set&lt;T&gt; of(T t1)
        + static &lt;T&gt; Set&lt;T&gt; of(T t1, ..., T t10)
        + static &lt;T&gt; Set&lt;T&gt; of(T... elements)
    }
    <<Interface>> Set
    class AbstractSet["AbstractSet&lt;T&gt;"]
    <<Abstract>> AbstractSet
    class HashSet["HashSet&lt;T&gt;"]
    class TreeSet["TreeSet&lt;T&gt;"] {
        + T ceiling(T t)
        + Iterator&lt;T&gt; descendingIterator()
        + NavigableSet&lt;T&gt; descendingSet()
        + SortedSet&lt;T&gt; headSet(T toElement)
        + NavigableSet&lt;T&gt; headSet(T toElement, boolean inclusive)
        + T higher(T t)
        + T pollFirst()
        + T pollLast()
        + NavigableSet&lt;T&gt; subSet(T fromElement, boolean fromInclusive, T toElement, boolean toInclusive)
        + SortedSet&lt;T&gt; subSet(T fromElement, T toElement)
        + SortedSet&lt;T&gt; tailSet(T fromElement)
        + NavigableSet&lt;T&gt; tailSet(T fromElement, boolean inclusive)
    }
    class Queue["Queue&lt;T&gt;"] {
        T element()
        boolean offer(T t)
        T peek()
        T poll()
        T remove()
    }
    <<Interface>> Queue
    class Deque["Deque&lt;T&gt;"] {
        + Iterator&lt;T&gt; descendingIterator()
        + boolean offerFirst(T t)
        + boolean offerLast(T t)
        + T peekFirst()
        + T peekLast()
        + T pollFirst()
        + T pollLast()
        + T pop()
        + void push(T t)
        + boolean removeFirstOccurrence(Object o)
        + boolean removeLastOccurrence(Object o)
        + default Deque&lt;T&gt; reversed()
    }
    <<Interface>> Deque
    Iterable <|-- Collection
    Collection <|-- List
    List <|-- AbstractList
    AbstractList <|-- ArrayList
    AbstractList <|-- LinkedList
    Deque <|-- LinkedList
    Collection <|-- Set
    Set <|-- AbstractSet
    AbstractSet <|-- HashSet
    AbstractSet <|-- TreeSet
    Collection <|-- Queue
    Queue <|-- Deque 
```

#### Map

```Mermaid

classDiagram
    class Object {
        + int hashCode()
        + boolean equals(Object o)
    }
    class Map["Map&lt;K, V&gt;"] {
        + void clear()
        + default V compute(K key, BiFunction<? super K,? super V,? extends V> remappingFunction)
        + default V computeIfAbsent(K key, Function<? super K,? extends V> mappingFunction)
        + default V computeIfPresent(K key, BiFunction<? super K,? super V,? extends V> remappingFunction)
        + boolean containsKey(Object key)
        + boolean containsValue(Object value)
        + static <K, V> Map&lt;K,V&gt; copyOf(Map<? extends K,? extends V> map)
        + static <K, V> Map.Entry&lt;K,V&gt; entry(K k, V v)
        + Set<Map.Entry&lt;K,V&gt;> entrySet()
        + default void forEach(BiConsumer<? super K,? super V> action)
        + V get(Object key)
        + default V getOrDefault(Object key, V defaultValue)
        + boolean isEmpty()
        + Set&lt;K&gt; keySet()
        + default V merge(K key, V value, BiFunction<? super V,? super V,? extends V> remappingFunction)
        + static <K, V> Map&lt;K,V&gt; of()
        + static <K, V> Map&lt;K,V&gt; of(K k1, V v1)
        + static <K, V> Map&lt;K,V&gt; of(K k1, V v1, ..., K k10, V v10)
        + static <K, V> Map&lt;K,V&gt; ofEntries(Map.Entry<? extends K,? extends V>... entries)
        + V put(K key, V value)
        + void putAll(Map<? extends K,? extends V> m)
        + default V putIfAbsent(K key, V value)
        + V remove(Object key)
        + default boolean remove(Object key, Object value)
        + default V replace(K key, V value)
        + default boolean replace(K key, V oldValue, V newValue)
        + default void replaceAll(BiFunction<? super K,? super V,? extends V> function)
        + int size()
        + Collection&lt;V&gt; values()
    }
    <<Interface>> Map
    class HashMap["HashMap&lt;K, V&gt;"]
    class TreeMap["TreeMap&lt;K, V&gt;"] {
        + Comparator<? super K> comparator()
        + K ceilingKey(K key)
        + K firstKey()
        + K floorKey(K key)
        + K higherKey(K key)
        + K lastKey()
        + K lowerKey(K key)
        + Map.Entry&lt;K,V&gt; ceilingEntry(K key)
        + Map.Entry&lt;K,V&gt; firstEntry()
        + Map.Entry&lt;K,V&gt; floorEntry(K key)
        + Map.Entry&lt;K,V&gt; higherEntry(K key)
        + Map.Entry&lt;K,V&gt; lastEntry()
        + Map.Entry&lt;K,V&gt; lowerEntry(K key)
        + Map.Entry&lt;K,V&gt; pollFirstEntry()
        + Map.Entry&lt;K,V&gt; pollLastEntry()
        + NavigableMap&lt;K,V&gt; descendingMap()
        + NavigableMap&lt;K,V&gt; headMap(K toKey, boolean inclusive)
        + NavigableMap&lt;K,V&gt; subMap(K fromKey, boolean fromInclusive, K toKey, boolean toInclusive)
        + NavigableMap&lt;K,V&gt; tailMap(K fromKey, boolean inclusive)
        + NavigableSet&lt;K&gt; descendingKeySet()
        + NavigableSet&lt;K&gt; navigableKeySet()
        + SortedMap&lt;K,V&gt; headMap(K toKey)
        + SortedMap&lt;K,V&gt; subMap(K fromKey, K toKey)
        + SortedMap&lt;K,V&gt; tailMap(K fromKey)
        + V putFirst(K k, V v)
        + V putLast(K k, V v)
    }

    Map <|-- HashMap
    Map <|-- TreeMap
```

[[_TOC_]]

# Gestion des erreurs (`java.lang.Throwable`, `java.lang.Exception`, `java.lang.Error`)

## Qu'est-ce que c'est et pourquoi ça existe&nbsp;?

Un processus suffisamment complexe comporte nécessairement des cas problématiques. C'est également le cas de tout processus qui dépend de conditions qui ne sont pas garanties.

Java propose un mécanisme qui permet de gérer les problèmes&nbsp;: `Throwable` et la propagation des exceptions. Ce qu'on appelle typiquement exception est un cas qui met fin à l'opération en cours d'exécution.

L'exemple le plus simple qu'on peut donner est celui d'une expression arithmétique comme `(2 * (3 + 1 / 0))`, écrite en Java `produit(2, somme(3, quotient(1, 0)))`. On peut supposer qu'un moteur arithmétique simple lirait cette expression et la transcrirait en la pile suivante (N représente l'ordre d'ajout dans la pile)&nbsp;:

| N | Fonction | Opérandes |
|-|-|-|
| 3 | `quotient` | 1, 0 |
| 2 | `somme` | 3, ? |
| 1 | `produit` | 2, ? |

L'opération 2 dépend de la résolution de l'opération 3, et l'opération 1 de la résolution de l'opération 2.

Comme l'opération 3 échoue, l'opération 2 ne peut même pas être exécutée, ni l'opération 1. Pourtant la pile devrait être vidée et on peut envisager que le programme puisse continuer, éventuellement pour prévenir l'utilisateur qu'une erreur a eu lieu.

C'est le rôle du mécanisme des exceptions.

Java définit trois types d'exceptions&nbsp;:

1. `Exception`, qui est le type dédié aux problèmes qui devraient être gérés par le programme. Ces exception sont vérifiées (_checked exceptions_).
2. `RuntimeException`, qui est un sous-type de `Exception` qui décrit l'ensemble des problèmes qui surviennent à l'exécution et peuvent ne pas être gérés par le programme.
3. `Error`, qui est le type dédié aux problèmes graves, desquels un programme ne peut censément pas se rétablir.

`Exception` et `Error` héritent de la classe parent `Throwable`.

## Exceptions vérifiées à la compilation (_checked exceptions_)

Lorsqu'une fonction peut générer un problème lors de son exécution, comme par exemple la fonction `quotient(int, int)`, mais que ce problème n'est pas considéré comme bloquant pour l'exécution du programme, ce cas doit figurer dans la signature de la fonction&nbsp;:

```Java
public int quotient(int leftOp, int rightOp) throws DivideByZeroException
```

La classe `DivideByZeroException` est une sous-classe, écrite par nos soins, de la classe `Exception`&nbsp;:

```Java
public class DivideByZeroException extends Exception {
    public DivideByZeroException() {
        super(); // Bad code, we'll come back on this later
    }
}
```

Censément, cette erreur est prévue par le code&nbsp;:

```Java
public int quotient(int leftOp, int rightOp) throws DivideByZeroException {
    if (rightOp == 0) {
        throw new DivideByZeroException();
    }
    return leftOp/rightOp;
}
```

Si elle est exécutée, l'instruction `throw <exception>` déclenche la création d'une exception. Cette exception interrompt toutes les méthodes sur la pile d'appel (qui se vide donc à chaque fois de son premier élément) jusqu'à être _traitée_ (nous verrons comment plus loin). Elle embarque l'état de la pile d'appel au moment de sa création avec elle.

Le compilateur effectue une vérification sur chaque méthode. Une méthode qui génère une exception vérifiée, par elle-même ou parce qu'elle appelle une méthode qui génère une exception vérifiée, doit le signaler dans sa signature.

Prenons l'exemple du reste de la division euclidienne&nbsp;:

```Java
public int resteEuclidien(int leftOp, int rightOp) {// erreur ici !
    return leftOp - quotient(leftOp, rightOp); // génère une DivideByZeroException
}
```

Cette méthode ne compile pas&nbsp;: l'exception `DivideByZeroException` n'est ni traitée ni déclarée. Dans notre cas, il n'est pas légitime de traiter l'exception dans cette méthode. En effet, si le quotient ne peut pas être calculé, le reste de la division ne peut pas l'être non plus.

On peut donc corriger ce code ainsi&nbsp;:

```Java
public int resteEuclidien(int leftOp, int rightOp) throws DivideByZeroException {
    return leftOp - quotient(leftOp, rightOp); // génère une DivideByZeroException
}
```

<strong>Exercice&nbsp;:</strong> supposez les classes suivantes&nbsp;:
TODO

## Exceptions non vérifiées à la compilation (_unchecked exceptions_)

Certains problèmes sont dûs à des circonstances qu'il est difficile d'anticiper, et que le code client peut raisonnablement ne pas avoir prévu. Cela inclut des erreurs ou des faiblesses dans la programmation, par exemple&nbsp;:

1. Un appel de méthode sur une référence d'objet `null`.
1. L'accès à un index trop petit ou trop grand d'un tableau.
1. Une division par zéro.

Dans ce cas, ces problèmes doivent générer une exception qui n'est pas vérifiée à la compilation. C'est l'objet de la classe `RuntimeException` qui étend `Exception`.

C'est exceptions peuvent ne pas être gérées ni déclarées. Ainsi le code suivant est valide&nbsp;:

```Java
public class DivideByZeroException extends RuntimeException {

    public DivideByZeroException(String s) {
        super(s);
    }

}
```

```Java
public class Main {
    public static void suspectDivision(int leftOp, int rightOp) {
        if (rightOp == 0) {
            throw new DivideByZeroException("You just can't!");
        }
        return leftOp/rightOp;
    }
}
```

## La classe `Error`

Les erreurs sont des exceptions bien plus graves que la simple `Exception`. Il s'agit, contrairement à la `RuntimeException`, d'un type d'exception généré à cause de circonstances dont l'application ne peut pas se rétablir. Et donc, elles ne sont pas vérifiées à la compilation et il n'est pas nécessaire de les déclarer.

`RuntimeException` ne suppose pas que le code client ait prévu ce cas, tandis que `Error` suppose que le code client ne pourra pas s'exécuter en raison du problème.

## Gestion des exceptions

La plupart du temps, un code qui peut générer une exception est assorti de quelques instructions qui permettent au système de retrouver un état stable. De plus, nous avons vu qu'une exception, une fois générée, traversait la pile d'exécution en interrompant toutes les méthodes de la pile.

Donc si une exception n'est pas gérée, elle finit par interropre le programme et il est impossible de poursuivre le processus.

La gestion des exceptions se fait avec le code suivant&nbsp;:

```Java
try {
    // code générant une exception
} catch (<TypeException1> ex) {
    // instructions propres à la survenue de l'exception du type <TypeException1>
} finally {
    // instructions effectuées dans tous les cas, que le code à l'intérieur du try se soit exécuté correctement ou non
}
```

Le bloc `catch` et le bloc `finally` sont optionnels. Il est possible de chaîner plusieurs blocs `catch` consécutifs.

Supposons par exemple une classe `Client`&nbsp;:

```Java
public class Client {
    public Response contact(Message message) throws ClientNotHereException,
                    ClientDoesntWantToSpeakException, 
                    ClientDoesntUnderstandException {
        // some code
    }
}
```

Le type de l'exception générée par la méthode `contacte(Message)` conditionne l'action qui va suivre la tentative de contact&nbsp;:

```Java
public Message makeMessage() {
    // some code that generates a Message
}

public void store(Message message, boolean success) {
    // stores the message with the status : delivered or not
}

public void sendMessage(Client client) throws ClientNotHereException,
                    ClientDoesntWantToSpeakException, 
                    ClientDoesntUnderstandException {
    var message = makeMessage();
    var success = false;
    try {
        client.contact();
        success = true;
    } finally {
        store(message, success);
    }
}
```

Dans le code qui précède, les exceptions ne sont pas gérées directement dans `sendMessage(Client)`. La méthode appelante aura cette responsabilité, qu'elle pourra décliner en déclarant `throws ...`. En revanche, le code `store(message, success)` est exécuté&nbsp;:

1. Si `client.contact()` s'exécute avec succés, alors `success` est valorisé à `true` et le code exécuté est `store(message, true)`.
1. Si `client.contact()` s'exécute avec succés, alors `success` reste valorisé à `false` et le code exécuté est `store(message, false)`.

On peut imaginer une gestion plus fine des exceptions dans la méthode `sendMessage(Client)`&nbsp;:

```Java
public Message makeMessage() {
    // some code that generates a Message
}

public void store(Message message, boolean success) {
    // stores the message with the status : delivered or not
}

public void sendMessage(Client client) throws ClientNotHereException,
                    ClientDoesntWantToSpeakException, 
                    ClientDoesntUnderstandException {
    var message = makeMessage();
    var success = false;
    try {
        client.contact();
        success = true;
    } catch (ClientNotHereException | ClientDoesntWantToSpeakException ex) {
        // No need to try again just now, let's come back at it later
    } catch (ClientDoesntUnderstandException ex) {
        // Let's try again with another language !
    } finally {
        store(message, success);
    }
}
```

Le code `ClientNotHereException | ClientDoesntWantToSpeakException ex` permet de gérer les exceptions qui sont de type `ClientNotHereException` ou `ClientDoesntWantToSpeakException`.

## _try-with-resources_

Certains objets ont un fonctionnement particulier&nbsp;: une fois qu'ils sont utilisés, ils doivent être clos pour libérer les données qu'ils détiennent. C'est le cas par exemple des fichiers ou des connexions.

Tous ces objets implementent l'interface `AutoCloseable` et la libération des ressources se fait en appelant la méthode `close()` de ces objets. Examinons le code suivant&nbsp;:

```Java
public class Connection implements AutoCloseable {
    public void submit(Information information) throws InformationTransmissionFailedException {
        // sends information to the recipient or generates an exception
    }

    public void close() {
        // releases the connection
    }
}

public class Main {

    public static Connection makeConnection() throws ConnectionNotPossibleException {
        // generates a Connection or throws an exception
    }

    public static Information makeInformation() {
        // generates information
    }

    public static void main(String[] args) {
        Connection connection = makeConnection();
        connection.submit(makeInformation());
        connection.close();
    }
}
```

Plusieurs choses peuvent mal se passer&nbsp;:

1. `makeConnection()` peut générer une erreur. Dans ce cas il est inutile de faire l'appel `connection.close()`.
1. `connection.submit(Information)` peut générer une erreur. Dans ce cas il est nécessaire de faire l'appel à `connection.close()`.
1. `connection.close()` peut générer une erreur.

Si on souhaite qu'aucune exception ne soit propagée et que tous les cas soient gérés proprement, le code devient plus compliqué&nbsp;:

```Java
public class Connection implements AutoCloseable {
    public void submit(Information information) throws InformationTransmissionFailedException {
        // sends information to the recipient or generates an exception
    }

    public void close() {
        // releases the connection
    }
}

public class Main {

    public static Connection makeConnection() throws ConnectionNotPossibleException {
        // generates a Connection or throws an exception
    }

    public static Information makeInformation() {
        // generates information
    }

    public static void main(String[] args) {
        try {
            Connection connection = makeConnection();
            try {
                connection.submit(makeInformation());
            } catch (InformationTransmissionFailedException ex) {
                // here is what to do when submit fails
            } finally {
                connection.close();
            }
        } catch (ConnectionNotPossibleException ex) {
            // handle here this case
        }
    }
}
```

C'est super&nbsp;: pour trois lignes de code utiles, une dizaine qui ne servent qu'à gérer les erreurs. Imaginez maintenant qu'il faille gérer non plus la connexion, mais deux connexions&nbsp;! Le code devient encore plus illisible.

Pour cette raison, Java 7 introduit une syntaxe plus légère&nbsp;:

```Java
public class Connection implements AutoCloseable {
    public void submit(Information information) throws InformationTransmissionFailedException {
        // sends information to the recipient or generates an exception
    }

    public void close() {
        // releases the connection
    }
}

public class Main {

    public static Connection makeConnection() throws ConnectionNotPossibleException {
        // generates a Connection or throws an exception
    }

    public static Information makeInformation() {
        // generates information
    }

    public static void main(String[] args) {
        try (Connection connection = makeConnection()) {
                connection.submit(makeInformation());
        } catch (ConnectionNotPossibleException ex) {
            // handle here this case
        }
    }
}
```

Et voilà&nbsp;! Tous les cas sont gérés, le code ci-dessus est strictement équavalent à celui, pléthorique, écrit avec des `try ... catch ... finally ...` imbriqués.

## Zoom sur la classe `Throwable` et ses classes dérivées, chaînage

La classe `Throwable` possède trois constructeurs&nbsp;:

```Java
public Throwable(String message, Throwable cause) {...}

public Throwable(String message) {...}

public Throwable(Throwable cause) {...}
```

Le constructeur `Throwable(String, Throwable)` accepte deux arguments&nbsp;:

1. Le message qui sera disponible pour le code client de l'exception.
1. La cause de cette exception, qui est elle-même une exception.

La notion de cause est importante parce qu'elle permet au code client de générer une exception dont le type est compréhensible par les utilisateurs de ce code.

Prenons l'exemple d'un service de commandes en ligne. Dans notre exemple l'utilisateur clique sur le bouton de validation de son panier qui déclenche côté serveur la méthode `validerPanier`&nbsp;:

```Java
// Code incomplet du point de vue de la gestion des exceptions

public void validerPanier() {
    creerCommande();
    debiterMontantCommande();
}

public void creerCommande() {
    persisterCommande();
    notifierPreparateurCommande();
}

public void persisterCommande() throws ConnexionException { ... }
```
`persisterCommande` inscrit le contenu du panier dans une base de données avec le statut `à préparer`. Or, la base de données rencontre un problème donc `persisterCommande` génère une exception de type `ConnexionException`.

Que devrait faire `creerCommande`&nbsp;? Certainement pas étouffer l'exception avec un `catch` puisque dans ce cas `validerPanier` exécuterait `debiterMontantCommande()`.

Mais ne rien faire conduirait `creerCommande` à continuer l'exception `ConnexionException`. Cette exception n'est pas pertinente pour `validerPanier`. Il serait donc intéresant de corriger le code de façon à ce que `creerCommande()` génère une exception `CreationCommandeImpossibleException` qui sauvegarderait l'origine du problème (c'est_à-dire une erreur de connexion)&nbsp;:

```Java
public void validerPanier() {
    try {
        creerCommande();
        debiterMontantCommande();
    } catch (CreationCommandeImpossibleException ex) {
        log.info("Impossible de créer la commande", ex);
        // afficher le message approprié à l'écran de l'utilisateur
    }
}

public void creerCommande() throws CreationCommandeImpossibleException {
    try {
        persisterCommande();
        notifierPreparateurCommande();
    } catch (ConnexionException ex) {
        throw new CreationCommandeImpossibleException("La commande n'a pu être enregistrée", ex);
    }
}

public void persisterCommande() throws ConnexionException { 
    // du code
    // ... code qui génère une erreur
        throw new ConnexionException("Connexion impossible à obtenir");
    // à nouveau du code
}
```

Lorsque l'erreur survient, l'instruction `log.info("Imp... ...ande", ex)` va afficher un message qui ressemble à cela&nbsp;:

```
Exception in thread "????" my.package.CreationCommandeImpossibleException: my.otherpackage.ConnexionException: La commande n'a pu être enregistrée
	at my.package.UnControleur.validerPanier(UnControleur.java:137)
    at my.otherpackage.UnService.creerCommande(UnService.java:101)
Caused by: java.lang.Exception: Connexion impossible à obtenir
	at my.yetanotherpackage.UnAutreService.persisterCommande(UnAutreService.java:42)
    at some.database.driver.UneClasseDeConnexion.connect(UneClasseDeConnexion.java:42)
```

Ce message retranscrit pile des exécutions et des causes. Grâce à lui nous pouvons reconstituer l'enchaînement des appels&nbsp;: `validerPanier` → `creerCommande` → `persisterCommande` → `connect`.

## Bonnes pratiques sur les exceptions

### Utilisez des exceptions personnalisées

Ne levez jamais d'exception du type `Throwable`, `Exception`, `RuntimeException` ou `Error`. Ces types n'ont aucune signification métier et doivent être étendus par des exceptions d'un type signifiant.

<details>
<summary>&#129488; Pourquoi&nbsp;?</summary>

La cause d'une erreur est écrite littéralement sur la première ligne du message `Exception in thread... at... at`.

Donc avoir comme toute première ligne de ce message `Exception in thread "????" my.package.ImpossibleContacterClient` est beaucoup plus parlant qu'une ligne comme `Exception in thread "????" java.lang.RuntimeException` qui n'apporte aucune information sur le type de problème qui a été rencontré.
</details>

### Gérez les exceptions là où c'est pertinent

L'exemple de la méthode `validerPanier` est un peu artificiel à plusieurs points de vue. Du point de vue des exceptions, il convient de se demander s'il est nécessaire de capturer une exception pour en lever une autre.

En revanche&nbsp;:

1. Il ne faut jamais attraper les exceptions pour simplement les logger puis les renvoyer&nbsp;:
     ```Java
     public void methode() throws SomeException { ... }

     public void antiPattern() throws SomeException {
        try {
            methode();
        } catch (SomeException ex) {// un catch qui ne sert à rien
            log.error("Un log qui ne sert à rien", ex);
            throw ex;
        }
     }
     ```
1. Au contraire il faut gérer l'exception à l'endroit où une règle de gestion pertinente peut être appliquée.
1. N'étouffez jamais une exception par un bloc `catch` silencieux&nbsp;:
    ```Java
    public void methode() throws SomeException { ... }

    public void antiPattern() throws SomeException {
        try {
            methode();
        } catch (SomeException ex) {
            // Un jour vous passerez des heures à débugger un problème qui trouvera son origine ici
        }
    }
    ```

### Utilisez des types d'exception métier plutôt qu'`Exception`

Ce code gère les problèmes que vous avez anticipés. Si une autre exception est levée, elle sera gérée par une méthode appelante ou bien se propagera jusqu'à l'arrêt du thread.
```Java
// Oui
try {
    // some code
} catch (FileNotFoundException | IOException ex) {
    // some clever things to do when those occur
}
```

À l'inverse, considérons le code suivant&nbsp;:
```Java
// Non
try {
    // some code
} catch (Exception ex) {
    // some clever things to do when those occur
}
```
Un calcul est fait dans `some code` sur les éléments du fichier qui génère une `ArithmeticException`. Cette exception ne se propagera pas. Comme les seules erreurs qui ont été prévues sont les erreurs liées à la lecture du fichier, les instructions exécutées dans le `catch ...` ne seront pas adaptées.

### Loggez les erreurs mais n'appelez pas `ex.printStackTrace()`

Les applications utilisent des moyens de journalisation des évènements qui doivent être utilisés. La méthode `Throwable::printStackTrace` se contente d'écrire dans la console la pile d'exécution.

Préférez `log.error("This happened: " + ex)` qui va écrire l'évènement là où vous l'attendez.

### Documentez vos exceptions

Chaque déclaration d'exception doit être accompagnée d'une entrée de JavaDoc&nbsp;:

```Java
// Oui
/**
 * 
 * @throws CustomException si tel type de problème spécifique se pose
 */
public void uneMethode() throws CustomException { ... }

// Non : on ne sait pas quand CustomException est levée
public void uneAutreMethode() throws CustomException { ... }
```

### Explicitez le problème avec un message

Une exception devrait toujours être générée avec un message explicite.

```Java
// Oui
/**
 * @throws CustomException si tel type de problème spécifique se pose
 */
public void uneMethode() throws CustomException {
    if (... quelque chose...) throw new CustomException("Tel truc s'est passé");
    if (... autre chose...) throw new CustomException("Tel autre truc différent s'est passé");
}

// Non
/**
 * @throws CustomException si tel type de problème spécifique se pose
 */
public void uneMethode() throws CustomException {
    if (... quelque chose... ou ... autre chose...) throw new CustomException("Un truc s'est passé mais je ne peux pas être plus précis que ça");
}
```

### Enrichissez les exceptions personnalisées avec les informations adéquates

L'exception porte sur un objet qu'il sera utile de retrouver&nbsp;? Cet objet devrait, d'une manière ou d'une autre, se retrouver dans l'exception&nbsp;:

```Java
public class UserNotFoundException {
    private int userId;

    public UserNotFoundException(int userId) {
        super("User " + userId + " does not exist");
        this.userId = userId;
    }

    public int getUserId() { return this.userId; }
}
```

### N'utilisez les _checked exceptions_ que si nécesaire

Chaque méthode qui déclare une exception impose aux méthodes qui l'appellent de la déclarer également. Cela peut encombrer le code&nbsp;:

```Java
public void uneMethode throws TelleException, TelleAutreException, EncoreUneException, EtUneDerniereException { ... }
```

À la place, privilégiez les sous-types de `RuntimeException` à moins que l'usage d'une _checked exception_ ne soit vraiment justifié (le système doit pouvoir se rétablir de ce type de problème).

[[_TOC_]]

# La JVM

## Rappels sur Java, le JRE, le JDK, la JVM

### Atouts et inconvénients des langages compilés

<strong>Un compilateur qui compile vers du _code machine_ fonctionne pour une architecture physique et un système d'exploitation donnés.</strong>

Dans la formation « Introduction à l'algorithmique avec Java » nous avons vu que Java était un langage compilé. Cela veut dire que le code écrit par un humain, pour des humains, est transformé en un code (l'_exécutable_) que la machine peut comprendre (on peut parler de _langage machine_).

Beaucoup de langages compilés sont traduits par le compilateur dans un _langage machine_ propre à l'architecture physique de la machine. C'est super parce que cela permet au compilateur d'optimiser le code en fonction des spécificités de votre machine. Mais l'inconvénient c'est que si vous prenez l'exécutable ainsi généré, vous n'avez pas la certitude qu'il pourra fonctionner. On dit que le code généré n'est pas _portable_.

Pire ! Un compilateur fonctionne dans et avec un système d'exploitation précis. Cela veut dire en particulier que les exécutables générés par le compilateur le sont généralement pour un système d'exploitation. Essayez de lancer un fichier `*.exe` sous Linux&nbsp;: ça ne marche pas.

### Le bytecode pour avoir du code compilé et portable

Java aime la performance offerte par la compilation « à l'ancienne », mais n'aime pas l'absence de portabilité.

Donc Java adopte une solution un peu différente en fournissant deux outils&nbsp;:
1. Un environnement capable de converser avec la machine et qui s'appelle _Java Virtual Machine_.
1. Un compilateur qui traduit le langage Java vers un code que comprend la JVM. Le compilateur génère, pour chaque fichier *.java, un fichier *.class dans ce code compris par la JVM, et qui s'appelle du _bytecode_.

> &#129488; Selon les cas, le compilateur peut générer beaucoup plus de fichiers *.class qu'il n'y a de fichiers *.java au départ. De nombreux _frameworks_, qui enrichissent le langage Java de fonctionnalités, génèrent beaucoup de fichiers *.class.

Le _langage machine_ est très peu expressif&nbsp;: pour faire quelque chose de simple il faut beaucoup d'instructions élémentaires. Ces expressions sont les seules choses que le système d'exploitation comprenne. Le _bytecode_ est un peu plus expressif et n'est pas compris par le système d'exploitation (sauf si ce système d'exploitation est basé sur une JVM&nbsp;!). La JVM est capable de traduire ces instructions _bytecode_ vers des instructions élémentaires, que comprend le CPU (_Central Process Unit_). 

Grâce à ce fonctionnement le bytecode est portable d'un système d'exploitation à un autre et d'une architecture à une autre puisque c'est la JVM qui traduit ce bytecode en instructions machines pendant l'exécution du programme. On dit que la JVM est une abstraction de la machine sur laquelle elle tourne, dans le sens où elle rend au _bytecode_ le service que le système d'exploitation rend au _langage machine_.

Le langage Java est donc compilé mais également interprété, par la JVM, au moment de l'exécution.

On peut résumer tout cela de cette manière&nbsp;:

| Type de langage | Type de code | Portabilité du compilateur/interpréteur | Portabilité du code |
|-|-|-|-|
| Langage interprété | – | Un compilateur/interpréteur par architecture et par système d'exploitation | Pas d'exécutable, le code écrit fonctionne partout |
| Langage compilé | Code machine  | Un compilateur/interpréteur par architecture et par système d'exploitation | Un exécutable par architecture et par système d'exploitation |
| Langage à _bytecode_ | _bytecode_ | Un compilateur et une JVM par architecture et par système d'exploitation | Le _bytecode_ fonctionne sur toutes les architectures et tous les systèmes d'exploitation pour lesquels Java a été prévu |

Quels sont les intérêts de cette JVM&nbsp? Quel est l'intérêt d'avoir un code bas niveau et portable&nbsp;?

<details>
<summary>Réponse</summary>

1. On l'a dit, le code est indépendant de la plateforme.
1. C'est la JVM qui gère les spécificités du système d'exploitation. En particulier, la gestion des ressources telles que la mémoire, le réseau, les entrées-sorties, une fois écrite en Java, fonctionne partout.
1. La JVM embarque des fonctionnalités de sécurité. Par exemple il est impossible d'interagir directement avec les ressources du système d'exploitation directement. C'est la JVM qui le fait pour nous. En C, il est possible de détruire des données ou même du matériel avec quelques instructions hasardeuses (ou malicieuses).
1. Validation du code&nbsp;: la JVM effectue des vérifications du code pour valider le fait que le code est compilé par un compilateur approuvé.
1. La mémoire est gérée par la JVM et ça fait gagner un temps fou.
1. De nombreux langages fonctionnent sur JVM. Ils sont donc _interopérables_ (ils peuvent très facilement communiquer entre eux).
</details>

Et les défauts&nbsp;?

<details>
<summary>Réponse</summary>

1. La compilation ne se fait pas spécifiquement à une plateforme et il y a une interprétation lors de l'exécution donc, les performances sont moins bonnes qu'un langage comme C, par exemple.
1. La JVM est extrêmement compliquée. Et parfois elle cause des erreurs.
1. La JVM requiert beaucoup de mémoire pour fonctionner.
</details>

### Java Runtime Environment

La JVM est un traducteur entre le _bytecode_ et le système d'exploitation. Ce traducteur repose sur beaucoup, beaucoup d'outils. Ces outils comprennent des classes regroupées en _bibliothèques_ selon une cohérence de fonctionnalités offertes (langage de base, couche de sécurité, utilitaires, et cetera).

L'exécution du _bytecode_ repose également sur des outils&nbsp;:

1. Le _Java ClassLoader_ permet de charger les classes Java utilisées dans la JVM, au moment où c'est nécessaire.
1. Le vérificateur de _bytecode_ garantit que le code n'est pas corrompu en effectuant des séries de vérifications.
1. La JVM qui fournit une abstraction de la machine.
1. L'interpréteur Java enfin qui est appelé par la JVM pour traduire le _bytecode_ en code machine.

Cet assemblage de bibliothèques de classes, avec le vérificateur de _bytecode_, la JVM, l'interpréteur Java et d'autres ressources forment ce qu'on appelle le JRE&nbsp;: Java Runtime Environment, l'environnement d'exécution des programmes Java.

### Java Development Kit

Le JDK (Java Development Kit) est l'outil qui s'adresse aux développeurs pour développer et compiler leurs programmes. Il embarque la documentation, un ensemble de bibliothèques logicielles (tout comme le JRE), et bien entendu le compilateur Java.

Il existe plusieurs JDK adaptés aux différents besoins des développeurs&nbsp;: développement « standard », Web, mobile, et cetera.

Les JDK respectent les spécifications du langage Java mais ne sont pas tous produits par Oracle, propriétaire du langage. Quelques JDK alternatifs parmi les les plus populaires sont&nbsp;:

1. [AdoptOpenJDK](https://adoptium.net/fr/) publié par le groupe Adoptium qui appartient à Éclipse.
1. [OpenJDK](https://openjdk.org/) publié par OpenJDK.

> &#129488; Qu'est-ce « respecter les spécifications du langage Java ? »
> 
> Dans la formation « Introduction à l'algorithmique avec Java » nous avons vu que décrire un langage était équivalent, d'une certaine manière, à décrire le compilateur, puisque décrire le comportement des éléments de syntaxe revient à décrire ce que le compilateur en fait.
>
> Oracle met à disposition [ici](https://docs.oracle.com/javase/specs/) la description du comportement attendu par le compilateur et la JVM dans le traitement du code Java et du _bytecode_. Cette description spécifie ce que sont le langage, le compilateur, la JVM et d'autres choses. N'importe quel programme qui respecte parfaitement ces éléments est un JDK.

## Présentation générale de la JVM

Nous avons vu que la JVM est une machine virtuelle dans laquelle sont chargées les classes Java compilées et qu'elle permet également l'exécution du code.

Pour ce faire elle est structurée de façon à gérer différents aspects de l'exécution. Connaître l'existence de ses éléments constitutifs est important, car vous rencontrerez un jour une erreur émise à cause d'un problème dans un de ces éléments.

La JVM est composée de plusieurs éléments, décrits rapidement dans les sous-sections qui suivent.

### Le vérificateur de _bytecode_

Le vérificateur de _bytecode_ (_bytecode verifier_) vérifie que le _bytecode_ ne risque pas de faire planter la machine sur laquelle il tourne.

### l'interpréteur de _bytecode_ et le _JIT compiler_

L'interpréteur de _bytecode_ (_bytecode interpreter_) et le compilateur à la volée (_JIT compiler_ ou également en français _JIT_) travaillent en coordination pour produire et exécuter, à partir du _bytecode_, un code machine optimisé.

> &#129488; Plus précisément, sur la relation entre interpréteur de _bytecode_ et _JIT_&nbsp;:
>
> 1. L'interpréteur de _bytecode_ exécute le _bytecode_ en le convertissant en instructions machine.
> 2. Le _JIT_ est appelé par l'interpréteur pour convertir le _bytecode_ en instructions machines en ajoutant une optimisation.
> 
> Ce mécanisme d'optimisation est fait sur la base de statistiques générées par la JVM lors de l'interprétation du code.
>
> Il peut arriver, selon les JVM, que les choses se passent de façon légèrement différentes de celle présentée ici. Le fonctionnement décrit ici est celui de la JVM Oracle qui s'appelle Hotspot.

### La zone mémoire

La mémoire embarque tous les éléments qui seront appelés lors de l'exécution.

#### La _method area_
La _method area_ stocke les définitions d'interfaces et de classes chargées par le _class loader_. Cette zone est partagée entre tous les threads.

#### Le _heap_

Le _heap_ (tas) stoke les objets (instances de classes ou _arrays_). Cette zone est partagée entre tous les threads.

> &#129488; Chaque objet occupe un espace mémoire. Dans les langages comme C ou C++ le développeur doit gérer sa mémoire, c'est-à-dire&nbsp;:
>
> 1. Demander un espace mémoire pour la création d'un objet.
> 1. Restituer cet espace lorsque l'objet n'est plus utilisé.
>
> Java souhaite affranchir le développeur de cette contrainte qui peut être chronophage en terme de développement. Pour ce faire, Java permet à l'utilisateur de créer des objets grâce à l'opérateur `new` (qui réserve la place mémoire nécessaire), mais gère tout seul la libération de la mémoire quand un objet n'est plus utile.
> 
> Prenons l'exemple suivant&nbsp;:
> ```Java
> public static List<String> someSillyWork(String[] args) {
>     List<String> returned = new ArrayList<>();
>     for (int i = 0 ; i < args.length ; i++) {
>         String s = args[i].replaceAll("[0-9]+", "*");
>         if (!s.matches("\\*+")) {
>             returned.add(s);
>         }
>     }
>     return returned;
> }
> ```
> Dans cet exemple un tableau de `String` est passé comme paramètre de la méthode. Pour chaque élément une nouvelle chaîne est générée, obtenue par substitution des suites de chiffres par `*`. Par exemple `qsf554qsf5` devient `qsf*qsf*`. Ensuite, seules les chaînes de caractères qui ne ressemblent pas à une suite de `*` sont conservées. L'espace correspondant peut donc être rendu à la JVM.
>
> C'est le travail d'un outil qui s'appelle le _garbage collector_ (_gc_).
>
> Plus précisément, le _heap_ est organisé en _générations_ qui sont peuplées par les objets&nbsp;:
>
> 1. La _young generation_ est peuplée par les objets nouvellement créés.
> 1. La _old generation_ ou _tenured generation_ est peuplée par les objets qui ont dépassé une certaine durée d'existence. C'est le _gc_ qui les promeut de _young_ à _tenured_.
> 1. (Avant Java 8) La _permanent generation_ ou _permgen_ est peuplée par les métadonnées de la JVM, c'est-à-dire les informations qui lui permettent de décrire les ckasses et méthodes. Ces informations, une fois chargées, ont la durée de vie de la JVM (elles ne sont jamais déchargées).
> 1. (À partir de Java 8) Le _metaspace_ remplace _permgen_, et n'est plus stocké dans le _heap_. La taille mémoire de _metaspace_ peut s'agrandir dynamiquement et les classes peuvent être déchargées avant l'arrêt de la JVM, si besoin (_dynamic loading/unloading_).

> #### &#129488; Le _run-time constant pool_
>
> Le _run-time constant pool_ est une partie du _method area_ qui contient les références symboliques vers les noms de classes, d'interfaces, d'attributs, et de méthodes. Lorsqu'une classe ou une interface est chargée dans le _method area_, un _constant pool_ est créé pour cette classe ou interface. Cette zone est partagée entre tous les threads.
>
> Rentrer plus dans le détail nécessiterait de s'intéresser à ce qui constitue un compilateur.
>
> Lorsque la mémoire de la JVM est saturée lors de la création d'un _runtime constant pool_, une OutOfMemoryError est générée.

#### &#129488; Le _compteur ordinal_

Le _compteur ordinal_ (_program counter [register]_), propre à chaque _thread_, stocke l'adresse de l'instruction en cours d'exécution. Cette zone est propre à chaque thread.

#### La _JVM stack_

La _JVM stack_ (ou par abus _stack_) stocke les invocations de fonctions afin de garder le fil de l'exécution. Cette zone est propre à chaque thread.

Pour comprendre précisément comment cette zone fonctionne, on peut partir de l'exemple suivant&nbsp;: calculer l'expression `(2 + 3 * 5)` c'est calculer la somme de deux nombres, dont l'un est le produit de 3 et de 5. On voit que pour calculer la somme, on doit d'abord calculer le produit, tout en gardant en mémoire chacun des opérandes à chaque étape.

Java fait la même chose lorsque des fonctions s'appellent les unes les autres&nbsp;: on peut écrire cette expression `somme(2, produit(3, 5))`.

Pour évaluer cette expression, Java utilise une structure de pile&nbsp;:
| étape 1 | étape 2 | étape 3 | étape 4 |
|-|-|-|-|
| `produit 3 5`<br>`somme 2 ?` | `8`<br>`somme 2 ?` | `somme 2 8` | `10` |

Chaque fois qu'une fonction est appelée, Java l'ajoute à la pile, comme un dossier sur une pile de dossiers. Quand Java a fini d'exécuter une fonction, il retourne à la fonction précédente, là où il en était, en enlevant le premier élément de la pile.

Plus précisément, ce sont des _frames_ qui sont stockées sur la pile. Une _frame_ comporte tout le contexte qui permet à la méthode de s'exécuter. La plupart du contexte est en fait un ensemble de références vers des objets stockés à l'extérieur (dans le _heap_).

Lorsque trop de méthodes sont appellées sans être terminées, la taille maximale de la pile peut être atteinte et une `StackOverflowError` est générée, qui met fin à la _frame_ du dessus de la pile, puis à la suivante, et ainsi de suite, jusqu'à ce qu'elle soit gérée ou qu'elle termine le thread dans lequel elle a été générée.

&#9888;&#65039; Une `StackOverflowError` ne devrait pas être gérée&nbsp;!

&#129488; TODO préciser

#### &#129488; La _native method stack_

La _native method stack_ est propre aux méthodes natives (non écrites en Java) est également créée pour chaque _thread_. Elles ne sont pas compilées en Java donc leur invocation ne peut être gérée dans la _JVM stack_. Par exemple, les fonctions trigonométriques sont natives en Java 11 parce qu'elles font directement appel au CPU pour les calculer. Cette zone est propre à chaque thread.

De ce fait, Java ne peut pas absolument garantir que le comportement des méthodes natives est celui précisé dans la documentation.
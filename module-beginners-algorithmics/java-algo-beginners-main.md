# Introduction à l'algorithmique avec Java

[[_TOC_]]

## Introduction

### Prérequis

Cette formation ne requiert aucune connaissance autre que savoir se servir d'un ordinateur personnel. Des bases en informatique (utilisation préalable, même légère, d'un langage de programmation) sont cependant préférables.

Elle est très progressive et peut paraître très simple au début, mais en réalité chaque section a une importance vitale. En particulier, parfaitement comprendre ce que sont un algorithme et un programme est primordial pour comprendre de nombreux enjeux associés au code.

Du point de vue logiciel, les prérequis sont&nbsp;:
1. Eclipse, IntelliJ, ou un autre IDE installé et fonctionnel.
2. JDK 8 ou supérieur installé.
3. Pour suivre les exercices&nbsp;: client git.

### But

À l'issue de cette formation, vous connaîtrez&nbsp;:
1. La définition et l'intérêt d'un langage, d'un algorithme et d'un programme.
2. Les bases du langage Java vous permettant de faire des programmes simples en Java.
3. Les notions de bases sur la qualité du code et des algorithmes.

La formation est axée autant sur l'apprentissage des notions techniques que sur la compréhension de ce qui fait un code de qualité.

### Initialisation

1. Créez un répertoire où vous voulez importer le projet qui contient les exercices.
2. Ouvrez un terminal comme Git Bash ou un équivalent (faites un clic droit à l'intérieur du répertoire et cliquez sur _Git bash here_).
3. Dans le terminal, tapez ou copiez (`[Shift] + [Insert]`) la ligne de code suivante&nbsp;: `git clone git@gitlab.com:G1JzsV0/java-formation-algo-beginners.git`.

Sous Eclipse&nbsp;:


Sous IntelliJ&nbsp;:


## Algorithmes et programmes

### Qu'est-ce qu'un algorithme, qu'est-ce que l'algorithmique&nbsp;?

Supposons une planète, que nous appellerons Aï, tellement assistée par l'informatique que tous les besoins de ses habitants (que nous appellerons Aïs) sont réalisés, dès lors qu'ils y pensent. Ils savent marcher, prendre et manipuler des objets, parler, manger, et quelques petites choses encore, mais rien de plus.

Supposons que vous deviez expliquer à un de ces Aïs comment exécuter une action complexe, comme par exemple faire des courses. Cet Aï a juste besoin de faire un effort de pensée pour acquérir les objets de sa liste. En conséquent, prendre la voiture, de marcher et de saisir les items un à un en les rayant de sa liste sont des concepts assez compliqués, et leur ordonnancement est encore moins évident.

Pour ne rien arranger, supposons que les Aïs sont tellement idiots qu'à moins de leur donner des ordres absolument clairs, ils risquent de les interpréter de travers et de faire n'importe quoi.

**Comment leur expliquez-vous la manière de faire des courses&nbsp;?**

Vous pourriez commencer par leur dire de prendre la voiture, mais ils pourraient rétorquer qu'ils n'ont pas assez de force, ou de place dans leurs poches…

Vous en seriez finalement réduits à donner une multitudes d'instructions très simples, dépourvues d'ambiguïté, structurées à l'aide de centaines de vérifications… Et chacune de ces instructions se réfèrerait uniquement à ce que cet Aï pourrait comprendre et appliquer sans difficulté. Pensez par exemple à la décomposition de l'instruction &laquo;&nbsp;Dévérouillez la porte avant gauche de la voiture&nbsp;&raquo; en mouvements simples, pour un Aï qui ne sait pas ce qu'est une serrure…

Si nous regardons cet exemple, nous voyons que cette série d'instructions suppose plusieurs choses&nbsp;:
1. Des actions élémentaires, que l'Aï peut exécuter. Par exemple, &laquo;&nbsp;prendre un pot de moutarde&nbsp;&raquo;.
2. Un langage qui désigne chaque action à l'aide d'une phrase. Dans cette phrase, certains mots seront des mots-clés, porteurs d'un sens générique, comme par exemple &laquo;&nbsp;prendre&nbsp;&raquo;, et d'autres seront des symboles, qui désignent une variable, comme par exemple &laquo;&nbsp;un pot de moutarde&nbsp;&raquo;.
3. Une convention qui vous permet, tout comme à l'Aï, de savoir ce que chaque élément du langage signifie, c'est-à-dire comment chaque phrase sera exécutée. Ainsi, &laquo;&nbsp;prendre la voiture&nbsp;&raquo; ne peut être exécuté par un Aï autrement qu'en saisissant la voiture qui se trouve devant lui, si elle existe.
4. L'existence d'éléments en sortie. Faire les courses ne produit pas toujours le même résultat, heureusement.
5. L'existence d'éléments en entrée. Le résultat de la session de courses dépend de la liste de courses.
6. On peut ajouter que les courses doivent être effectuées à un moment donné, il est inacceptable que l'Aï erre sans fin dans les rayons du supermarché.

> Nous avons décrit ici ce qu'est un **algorithme**&nbsp;: une suite d'instructions dépourvues d'ambiguïté, qui permet de résoudre un type de problèmes donné.

À peu de choses près, les points ci-dessus sont donnés comme prérequis d'un algorithme par Donald Knuth (éminent _computer scientist_)&nbsp;:

1. **Finitude** : « un algorithme doit toujours se terminer après un nombre fini d’étapes ».
2. **Définition précise** : « chaque étape d'un algorithme doit être définie précisément, les actions à transposer doivent être spécifiées rigoureusement et sans ambiguïté pour chaque cas ».
3. **Entrées** : « quantités qui lui sont données avant qu'un algorithme ne commence. Ces entrées sont prises dans un ensemble d'objets spécifié ».
4. **Sorties** : « quantités ayant une relation spécifiée avec les entrées ».
5. **Rendement** : « toutes les opérations que l'algorithme doit accomplir doivent être suffisamment basiques pour pouvoir être en principe réalisées dans une durée finie par un homme utilisant un papier et un crayon ».

Tous ces points sont fondamentaux, pour deux raisons&nbsp;:
1. L'algorithme est un moyen de communiquer une solution entre humains. Cela rend les notions de **finitude** et de **rendement** cruciales pour la réalisabilité de l'algorithme, et la **définition précise** indispensable pour assurer le déterminisme et la répétabilité de l'exécution.
2. L'algorithme a vocation a être réalisé sur un ordinateur, qui est dépourvu de la capacité de penser. Cela rend toute possibilité d'ambiguïté dans les ordres inacceptable.

### Qu'est-ce qu'un programme&nbsp;?

Il est possible de voir le programme comme la réalisation d'un **algorithme**, dans un **langage** compréhensible par un **ordinateur**.

Mais du coup, qu'est-ce qu'un ordinateur et qu'est-ce qu'un langage&nbsp;?

#### Qu'est-ce qu'un ordinateur&nbsp;?

On peut voir l'ordinateur comme une machine capable d'exécuter des traitements de façon autonome (ne nécessitant pas d'intervention humaine pendant le traitement), sur la base d'éléments fournis par l'environnement (humain, mesures, données).

Charles Babbage et Ada Lovelace, par exemple, ont respectivement conçu et amélioré un ordinateur entièrement mécanique, capable de résoudre n'importe quel problème pourvu qu'on lui en donne le temps. Cet ordinateur devait fonctionner avec des cartes perforées et du courage pour faire tourner des poulies.

Il possédait une mémoire, un ensemble d'instructions et la capacité de transformer des cartes perforées en suite ordonnée de ces instructions.

> Pour notre propos, un **ordinateur** c'est exactement ça&nbsp;:
> 1. Une mémoire qui permet de stocker des données.
> 2. Un **jeu d'instructions** très simples, définies de manière univoque, que l'ordinateur est capable d'exécuteur avec une fiabilité parfaite.
> 3. Une **interface** avec l'utilisateur, qui permet à celui-ci de fournir des **entrées** et de récupérer des **sorties**.

On voit que ces points répondent aux prérequis de Knuth pour la définition de l'algorithme.

Le jeu d'instructions de l'ordinateur (on parle d'assembleur) constitue son langage primaire (pour faire simple). Un humain suffisamment fou, courageux (ou contraint) pourrait s'adresser à l'ordinateur dans ce langage – et cela a été le cas pendant des décennies.

Pour faire compliqué, l'assembleur est un langage très fortement analogue à celui d'un microprocesseur, mais lisible par l'humain.

Les instructions de l'assembleur sont très spécifiques et très limitées&nbps;: chargement d'une valeur dans un registre, déplacement d'une valeur, opérations arithmétiques, sauts à un emplacement du programme, comparaison, et éventuellement quelques autres.

On pourrait comparer ces instructions à des instructions de très bas niveau pour un Aï&nbsp;: supination ou pronation, fléchissement ou extension de muscles, et cetera… Faire faire les courses à un Aï en utilisant exclusivement ces instructions en nécessiterait des centaines de milliers, et le guide ainsi rédigé comporterait beaucoup de répétitions et serait illisible.

Un humain doit donc pouvoir communiquer avec un ordinateur en utilisant un **langage de plus haut niveau**.

#### Qu'est-ce qu'un langage&nbsp;?

Nous avons vu dans la section précédente qu'un ordinateur fonctionnait en exécutant des instructions très simples dans un langage rudimentaire, son assembleur, et qu'un programme en assembleur se présentait comme une suite généralement illisible et répétitive de ces instructions très simples.

Un programme en assembleur peut, par exemple, parcourir un fichier texte et l'afficher à l'écran, en ayant pris soin de mettre une majuscule au début de chaque mot.

Les informaticiens ont remarqué très tôt que certaines suites d'instructions avaient une structure similaire, et en ont déduit qu'elles pouvaient être compactées sous forme de patron (comme en couture).

Par exemple exécuter une action conditionnée par le résultat de la comparaison de deux nombres s'écrit ainsi dans certains assembleurs&nbsp;:

```
cmp 5, 8
je EQUAL       ; BL = BH
jg GREATER     ; BL > BH
jmp LESS       ; BL < BH
EQUAL:
  ; code executed if 5 = 8
  jmp END
GREATER:
  ; code executed if 5 > 8
  jmp END
LESS:
  ; code executed if 5 < 8
END:
  ; end of the statement !
```

Cette structure revient des milliers de fois dans un programme, il est donc naturel de tenter de la rendre plus lisible sous une forme qui ressemble à&nbsp;:

```
SI 5 = 8 ALORS ...
SINON SI 5 > 8 ALORS ...
SINON ALORS ...
```

Ce texte ne peut être exécuté comme du code par l'ordinateur, mais on peut imaginer un super programme, qui le parcourerait et le transformerait lui-même en code machine&nbsp;! En effet, la traduction du patron `SI ... ALORS ...` en code assembleur est aussi facile que l'inverse.

Ce super programme de traduction est appelé un compilateur, et l'ensemble des patrons d'instructions constituent un **langage de programmation**, de plus haut niveau que l'assembleur, parce que plus abstrait et plus proche de l'humain.

Pourquoi plus abstrait&nbsp;? Parce que tous les détails des instructions de bas niveau sont cachés par l'expressivité des instructions du langage.

> On peut donc voir un **langage de programmation** de deux manières différentes&nbsp;:
> 1. C'est un ensemble de règles (univoques, déterministes) qui permettent de transformer des phrases de haut niveau (`SI ... ALORS ...`) en instructions machines (`CMP ... JMP ...`). Le langage peut alors être assimilé au compilateur.
> 2. Du point de vue de l'humain, c'est une représentation textuelle dépourvue d'ambiguïté d'instructions de traitement.

Le point 2 fait abstraction du fonctionnement du langage, il est suffisant en première approche. Dans la réalité, lors de l'apprentissage d'un langage, il est nécessaire de se rappeler du premier point, car *in fine* pour maîtriser un langage il est nécessaire de connaître, au moins en partie, les règles du compilateur.

Par exemple, les mécanismes d'échappement dans le langage SAS (résolution des macrovariables, quoting), dans R (quoting, ...), dans les différents langages Shell ne peuvent être expliqués que grâce au fonctionnement du programme de traduction, même si ce mot est tû&nbsp;: par exemple on nous dit que SAS remplace, jusqu'à ce que ce soit impossible, chaque fragment `&&` par `&` et chaque `&i` par la valeur courante de `i`, dans un certain ordre… Il s'agit bien d'un fonctionnement très spécifique du programme de traduction.

Nous avons utilisé ici le terme de _programme de traduction_, car SAS, R et shell ne sont pas compilés. Le programme qui les traduit ne génère pas un fichier exécutable dans le langage machine, mais fait plutôt une traduction à la volée qu'il passe à la machine. C'est un processus un peu plus complexe, mais généralement plus riche en possibilités, qu'on appelle **interprétation**.

Il y a donc deux types de langages&nbsp;: les langage **compilés** et les langages **interprétés**. Et certains langages, comme Java, sont les deux à la fois&nbsp;!

## Présentation du langage Java

## Structure d'un programme simple sous Java&nbsp;: `Hello World`

Il y a une loi universelle qui dicte que le premier programme de tout développeur dans un nouveau langage se doit d'afficher une phrase simple dans la console. Un appendice à cette loi stipule que cette phrase devrait être un salut très général, sans quoi la honte s'abattrait sur le développeur.

Ouvrez votre IDE (_Integrated Development Environment_) préféré et créez un nouveau projet que vous appellerez `HelloWorld`. Vous pouvez consulter les sections correspondantes de la formation &laquo;&nbsp;Environnement du développeur Java, du compilateur au CI/CD&nbsp;&raquo;. <!-- TODO renommer le module -->

Créez un package `fr.insee` dans le projet et une classe `HelloWorld` dans le package nouvellement créé.

Comment se présente cette classe&nbsp;?

```java
package fr.insee;

public class HelloWorld {
    
}
```

Elle est bien vide, cette classe. Décorons-là ainsi&nbsp;:

```java
package fr.insee;

/**
 * Ceci est de la Javadoc ! Ça sert à documenter les entités du code !<br/>
 * Passez la souris sur le nom de la classe pour voir...
 */
public class HelloWorld {

    /**
     * Cette méthode {@code main} permet d'exécuter du code.
     */
    public static void main(String[] args) {
        /*
         * Ceci est un commentaire multiligne.
         * Il permet d'expliquer, par exemple, que la ligne du dessous demande l'affichage, dans la console, de la chaîne de caractères passée en paramètres.
         */
        System.out.println("Hello, world!"); // Le monde nous en est reconnaissant
    }
}
```

Passons en revue les différents élements de ce code&nbsp;:
1. La ligne `package fr.insee;` indique que la classe appartient à un **package**. C'est une sorte de répertoire qui structure le programme. Pour le moment ce n'est pas nécessaire de savoir ce que c'est, juste que cette ligne est obligatoire.
2. La ligne `/**` marque le début d'une documentation. La documentation est une forme de commentaire (inutile à l'exécution du code) qui documente les packages, classes, fonctions et attributs de votre code (il n'est pas nécessaire de connaître ces mots pour le moment). C'est indispensable à tout projet qui se respecte, mais là encore, n'anticipons pas.
La documentation s'étale sur autant de lignes que souhaitées, chacune d'entre elle doit commencer par `*`.
Elle se termine par `*/`.
3. La ligne `public class HelloWorld` indique que ce fichier définit une classe (pour le moment il suffit de savoir que c'est un bout de code) qui s'appelle `HelloWorld`.
Les deux accolades `{` et `}` servent à délimiter le code de la classe `HelloWorld`.
4. La ligne `/*` marque le début d'un commentaire multiligne. C'est comme la javadoc, sans l'aspect documentaire.
5. La ligne `public static void main(String[] args)` déclare une fonction qui contient le code exécutable de la classe.
6. Les deux accolades `{` et `}` qui suivent délimitent le code de la fonction `main`.
7. La ligne `System.out.println("Hello, World!");` demande l'affichage de la phrase `"Hello, World!"` dans la console (qui s'appelle `System.out`).

Il y a plein de questions sans réponses&nbsp;: qu'est-ce que `public`, `class`, `String[]`, pourquoi `System.out`&nbsp;? Et j'en passe.

Pour le moment ce n'est pas important de savoir cela. L'objet de cette formation est de savoir coder un algorithme en Java, et de lancer le produit de nos cerveaux fertiles… Le reste viendra dans d'autres formations.

## Variable, constante, expressions, déclarations, blocs _(expressions, statements, blocks)_

### Qu'est-ce que qu'une variable, constante, une expression&nbsp;?

Observons la phrase `soit i = 1.`. On y trouve&nbsp;:
1. Un symbole destiné à recevoir une valeur&nbsp;: `i`, probablement entier.
2. Une valeur `1`, littérale donc ne pouvant être changée.
3. Un opérateur d'assignation `=`.
4. Et un mot-clé `soit` qui dénote un ordre (l'assignation qui suit).

Dans le cadre d'un langage de programmation, l'entité `i` est matérialisée par un emplacement en mémoire qui peut être modifié par une assignation. On dit que c'est une variable, et on garde ce terme également dans le cadre de l'algorithmique.

La valeur `1` est littérale donc ne peut être modifiée, en revanche elle peut être lue et évaluée. Nous avons donc deux valeurs, avec des rôles un différents.

Cette distinction est très importante car elle dicte ce qui peut être fait sur chaque type de valeur.

> On parle de **variable** pour tout objet matérialisé par un emplacement mémoire et qui peut recevoir une valeur plusieurs fois au cours de son existence. `i` est une variable dans l'exemple précédent.

> On parle de **constante** pour un objet matérialisé par un emplacement mémoire, et dont la valeur ne peut changer. La définition d'une constante peut ne pas différer énormément de celle d'une variable. Par exemple `soit constante PI = 3.1415926535.`.

Par commodité, j'emploierai, pour désigner les variables et les constantes, le terme de _left value_, même s'il est plutôt propre au langage C.

Souvent, on veut assigner à une _left value_ le résultat d'un calcul. Tout ce qui peut être évalué (donc assigné comme valeur à une _left value_) est appelé **expression**.

> Plus précisément, une expression est une suite de symboles (variables, opérateurs, …) qui peut être évaluée de façon à être assignée à une _left value_. On considère que l'expression est maximale (dans l'expression `i == 2`, on ne considère pas `i` ou `2` comme des expressions, seulement `i == 2` en entier).

Par commodité et opposition aux _left values_, on pourra parler de _right values_ pour les expressions même si d'une part ce terme est plutôt spécifique au C et si d'autre part la _right value_ représente le résultat de l'expression.

Ainsi&nbsp;:
1. `i + 1` est une expression.
2. En Java, `j = i + 1` est une expression, j est une variable, 1 est une constante littérale.

### Qu'est-ce que qu'une déclaration&nbsp;?

> Une **déclaration** est l'équivalent programmatique d'une phrase affirmative, ou bien un ordre simple. Toute phrase du langage qui ne présente pas de structure est une déclaration. On peut également dire qu'une déclaration est une unité d'exécution complète.

> On utilise aussi le terme **instruction**.

Ainsi&nbsp;:
1. `j = i + 1;` est une déclaration en Java.
2. `Écrire("Bonjour")` est une déclaration en algorithmique, donc l'équivalent Java serait `System.out.println('Bonjour')`.
3. `Si a < 5 Alors Écrire("Petit") Sinon Écrire("Grand") FinSi` est une déclaration en algorithmique (la découper est impossible).

En Java, une déclaration se termine toujours par un point-virgule (_semicolon_, `;`).

### Qu'est-ce que qu'un bloc&nbsp;?

> En Java, un bloc est une suite de déclarations délimités par une paire d'accolades `{` et `}`.

> Pour avoir une définition qui ne dépende pas du langage, donc valide aussi en algorithmique, on pourrait dire qu'un bloc est une succession de déclarations qui, si elles sont exécutées, le sont toutes, les unes à la suite des autres, sans saut ni interruption.

Par exemple&nbsp;:

```
01  Si (a < 5) Alors
02    Écrire("Petit")
03    a <- a + 1
04  Sinon
05    Écrire("Grand")
06    a <- a - 1
07  FinSi
```

Ce morceau de code comporte deux blocs&nbsp : les lignes `03` et `04` d'une part, et les lignes `05` et `06` d'autre part. Ce sont les deux seuls endroits où l'on trouve des lignes consécutives qui sont exécutées toutes à la suite, ou pas du pas du tout.

## Variables et types de données _(Variables and data types)_

### Les types primitifs

Le tableau suivant résume les différents types de données dits &laquo;&nbsp;primitifs&nbsp;&raquo;&nbsp;:

| Type             | Usage                                                            | Valeurs                                                              | Place mémoire | Déclarations                                                                   |
|------------------|------------------------------------------------------------------|----------------------------------------------------------------------|---------------|--------------------------------------------------------------------------------|
| `boolean`        | Valeurs de vérité                                                | `true` et `false`                                                    | 1 octet       | `boolean isOdd;`<br/> `boolean isEven = true;`<br/> `boolean isNasty = false;` |
| `byte`           | Entiers très courts                                              | -128 à 127                                                           | 1 octet       | `byte a;`<br/> `byte b = 5;`                                            |
| `short`          | Entiers courts                                                   | -2<sup>15</sup> à 2<sup>15</sup>-1                                   | 2 octet       | `short a;`<br/> `short b = 25_000;`                                            |
| `int`            | Entiers                                                          | -2<sup>31</sup> à 2<sup>31</sup>-1                                   | 4 octet       | `int a;`<br/> `int b = 25_814_703;`                                            |
| `long`           | Entiers long                                                     | -2<sup>63</sup> à 2<sup>63</sup>-1                                   | 8 octet       | `long a;`<br/> `long b = 123_456_789_012L;`<br/> `long b = 123_456;`                |
| `float`          | Nombres à virgule flottante<br/>(6 à 7 chiffres significatifs)   | 1,40239846&times;10<sup>-45</sup> à 3,40282347&times;10<sup>38</sup>        | 4 octet       | `float a;`<br/> `float b = 5;` <br/> `float b = 1.02F;`                                            |
| `double`         | Nombres à virgule flottante<br/>(15 à 16 chiffres significatifs) | 4,9406564584124654&times;10<sup>-324</sup> à 1,7976931348623157&times;10<sup>308</sup> | 8 octet       | `double a;`<br/> `double b = 5D;`<br/> `double b = 1.02D;`                                            |
| `char`           | Caractère unicodes                                               | `\u0000` à `\uffff`       | 1 octet       | `char a;`<br/> `char b = 'k';`                                            |

Vous remarquerez que certaines déclarations comme `long b = 123_456_789_012L;` forcent explicitement le typage de la valeur par l'ajout d'un suffixe. C'est dû au fait que, lors d'une déclaration avec assignation, Java effectue les opérations suivantes, dans cet ordre&nbsp;:

1. Évaluation de la _right-value_, avec attribution d'un type qui dépend de la forme du nombre.
2. Conversion du type de la valeur de droite vers le type déclaré, éventuellement avec perte de précision.
3. Attribution de la valeur correctement typée.

Prenons l'exemple de la déclaration `long b = 123_456_789_012;`. Que fait Java&nbsp;? Il lit `123_456_789_012` et ne détecte aucune virgule, donc essaie de créer un `int` avec la valeur `123_456_789_012`. Cette valeur est plus grande que 2<sup>31</sup>-1, donc la création d'un `int` avec cette opération est impossible. Le compilateur renvoit une erreur.

Dans la déclaration `long b = 123_456_789_012L;`, le suffixe `L` indique à Java que le nombre qui précède doit être considéré comme un `long`. Aucune erreur n'est générée.

La section sur les opérateurs, et plus précisément celle sur l'opérateur de **transtypage** (_cast_) décrit plus précisément les différents processus.

&#x26A0; Les types entiers sont fiables et peuvent être utilisés pour stocker des valeurs correspondant à n'importe quelle valeur métier, dès lors que cette valeur reste dans les bornes prévues par le type.
En revanche, les types à virgule flottante ne sont pas fiables et ne doivent pas être utilisés pour stocker des valeurs pour lesquelles la précision est essentielle comme par exemple des valeurs monétaires.

🧐 Mais pourquoi&nbsp;?
<details>
<summary>Réponse</summary>

Parce que le codage d'un nombre qui s'écrit simplement en décimal peut demander beaucoup plus de bits que ce que le type permet.
    
Par exemple, (0.3)<sub>10</sub> = (0.0100 1100 1100 1100 …)<sub>2</sub>.

Un `float` ne stocke que 24 chiffres significatifs, donc la valeur stockée en binaire ne sera pas exactement 0.3 mais une approximation à 2<sup>-24</sup>, c'est-à-dire à un milliardième près.

Sur des calculs longs, ces erreurs peuvent s'accumuler et générer des aberrations.
</details>

Pour stocker des valeurs monétaires, on peut&nbsp;:

1. Les stocker en centimes (pourquoi le faire, pourquoi ne pas le faire&nbsp;?).
2. Utiliser un type dont la précision est réputée absolue (comme `BigDecimal`, que nous verrons dans la formation &laquo;&nbsp;Introduction à la programmation orientée objet avec Java&nbsp;&raquo;).

### Le type `String`

Ce type permet de représenter les chaînes de caractères comme par exemple `bonjour`&nbsp;:

```Java
String myString = "A pretty value";
```

Ce n'est pas un type primitif mais un type d'objet (voir la formation &laquo;&nbsp;Introduction à la programmation orientée objet avec Java&nbsp;&raquo; pour tout ce que cela implique).

Une chaîne de caractère est délimitée par des guillemets anglais `"`. Il peut être également intéressant de savoir que&nbsp;:

1. Des caractères particuliers peuvent y être représentés à l'aide d'un caractère d'échappement, l'antislash `\`&nbsp;:
    1. Le guillemet français s'écrit `\"`
    2. Le retour chariot s'écrit `\r`, la fin de ligne `\n`, le retour à la ligne s'écrivait `\r` sous Mac, il s'écrit `\n` sous Linux sous Mac, et `\r\n` sous Windows.
    3. Bien entendu, l'antislash s'écrit `\\`.
2. Des chaînes de caractères peuvent être concaténées avec l'opérateur `+`, comme dans `"Bonjour " + "toi"`.

🧐 Comment écririez-vous le point 1.2. ci-dessus dans une chaîne de caractère, en Java&nbsp;? (Interdiction de copier et coller dans votre IDE&nbsp;!)

<details>
<summary>Réponse</summary>

```Java
"Le retour chariot s'écrit `\\r`, la fin de ligne `\\n`, le retour à la ligne s'écrivait `\\r` sous Mac, il s'écrit `\\n` sous Linux sous Mac, et `\\r\\n` sous Windows."
```
</details>

### Le type `Enum`

Dans cette section vous est présentée de façon très sommaire un type Java particulier, dont le rôle est de vous permettre d'utiliser des énumérations, c'est-à-dire des listes immuables d'objets dont l'état ne change jamais, comme par exemple&nbsp;:

- Les doigts de la main (pouce, index, majeur, annulaire, didi),
- Les jours de la semaine (lundi, mardi, mercredi, jeudi, vendredi),
- les mois de l'année…

En revanche l'énumération ne _doit pas être utilisée_ pour dénoter des objets complexes ou des listes qui changent&nbsp;:

- La liste des employés d'une entreprise&nbsp;: la liste est changeante et de plus, un employé possède un état (âge, emploi) qui peut changer.
- La liste des prénoms&nbsp;: certes un prénom n'a pas de qualité mutable (le genre d'un prénom peut être considéré comme immuable pour les besoins de notre formation), mais la liste des prénoms change. Enzo n'existait pas en 1930, et dans 10 ans nous serons contents d'ajouter Céramyke à la liste des prénoms.

Une enum se déclare dans un fichier à son nom, de cette manière&nbsp;:

```Java
package fiddle;

public enum DoigtsDeLaMain {
    POUCE, INDEX, MAJEUR, ANNULAIRE, AURICULAIRE;
}
```

On s'en sert ainsi&nbsp;:

```Java
// Du code
DoigtsDeLaMain a = DoigtsDeLaMain.POUCE;
```

Une Enum est un _right-value_, et il est possible de déclarer une _left-value_ et de lui assigner une valeur définie par le type. Il est même possible d'aller un tout petit peu plus loin, mais ça sera pour plus tard (voir la formation &laquo;&nbsp;Introduction à la programmation orientée objet avec Java&nbsp;&raquo;).

### Les tableaux

Les tableaux sont très importants pour contenir une large collection d'objets. Nous n'entrerons pas en profondeur dans ce type particulier (pour cela il faudra voir la formation &laquo;&nbsp;Introduction à la programmation objet avec Java&nbsp;&raquo;).

On peut voir un tableau comme une colonne dans un tableur. Il se déclare de l'une des deux manières suivantes&nbsp;:

```Java
<type>[] <arrayName> = new <type>[<array length>];

int[] array = new int[5]; // 5 elements
```

```Java
<type>[] <arrayName> = {<values>};

String[] jours= {"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"};
```

Un élément d'un tableau ne peut être accédé que par son _index_ (sa position dans le tableau)&nbsp;:
```Java
int[] array = new int[5]; // 5 elements

array[0] = 1;

System.out.println(array[0]); // prints 1
System.out.println(array[1]); // prints 0
System.out.println(array[2]); // prints 0
```

Lorsqu'un tableau est initialisé, les valeurs de toutes ses cases le sont aussi. C'est pour cela qu'il est possible d'y accéder. et que dans notre exemple `array[1]` vaut 0.

Il est possible de connaître la taille d'un tableau grâce à son attribut `length`&nbsp;:

```Java
int[] array = new int[5]; // 5 elements

System.out.println(array.length); // prints 5
```

Il est très fortement recommandé de ne jamais faire référence à la taille d'un tableau autrement que par l'attribut `length`&nbsp;:

```Java
int[] array = new int[5]; // 5 elements

for (int i=0; i<5; i++) {// No, bad code
    System.out.println("array[" + i + "] = " + array[i]);
}

for (int i=0; i<array.length; i++) {// Yes, good code
    System.out.println("array[" + i + "] = " + array[i]);
}
```
🧐 Pourquoi est-ce recommandé&nbsp;?
<details>
<summary>Réponse</summary>

Si la valeur de la taille du tableau change, il faut la changer partout où elle est utilisée. Donc utiliser l'attribut `length` permet d'éviter ce tracas.
</details>

### Portée des variables

Dans cette formation, nous ne considérons que les variables locales, c'est-à-dire définies à l'intérieur de la méthode `main` ou (voir plus loin) d'une fonction&nbsp;:

```Java
public static void main(String[] args) {
    int i = 0;
}

public static someFunction() {
    int j = 0;
}
```

Chaque variable est définie (peut être assignée, utilisée) exclusivement à l'intérieur du bloc dans lequel elle est définie. Ainsi, dans l'exemple ci-dessus, `i` est utilisable dans la fonction `main`, mais pas dans `someFunction`. `j`, en revanche, n'est utilisable que dans `someFunction`.

Nous rappelons qu'un bloc est un ensemble d'instructions qui sont toutes exécutées ou pas exécutées, et délimitées par des accolades. Donc, dans l'exemple ci-dessous, les variables `message` et `otherMessage` ne sont utilisables qu'à l'intérieur du bloc à l'intérieur duquel elles sont déclarées.

```Java
public static void main(String[] args) {
    if (args == null) {
        int message = "Pas d'arguments";
    } else {
        int otherMessage = "Présence d'arguments";
        System.out.println(message); // ERROR ! message isn't defined
    }
    System.out.println(message); // ERROR ! message isn't defined
    System.out.println(otherMessage); // ERROR ! otherMessage isn't defined
}
```

> Les portions de code où une variable est utilisable (peut figurer comme left ou right value) sont appelées la portée de cette variable.

### Nommage des variables

Les variables doivent contenir exclusivement certains caractères&nbsp;:

1. Le premier caractère doit être une lettre ou un des caractères `_`ou `$`.
2. Les caractères suivants doivent être des lettres, des chiffres, ou un des caractères `_` ou `$`.

De plus&nbsp;:

1. Les variables devraient être écrites en _camel case_&nbsp;: première lettre en minuscule, et chaque début de mot est marqué non pas par une espace (c'est interdit dans les noms de variables) mais par une capitale, comme dans ces exemples&nbsp;: `nombreJours`, `isOdd`. Ce n'est pas obligatoire mais c'est une convention. Il est très déconseillé d'utiliser le _snake case_ pour les noms de variables (les mots sont séparés par des underscores comme dans `usual_suspects`).
2. Les variables devraient avoir des noms informatifs, qui désignent précisément leur rôle. Privilégier `age` à `a` par exemple.

## Opérateurs _(Operators)_

### Bien comprendre les opérateurs

> Un opérateur est une fonction notée par un symbole.

Java traite de la même manière fonctions et opérateurs, en l'essence il s'agit du même type d'objets. La seule différence réside dans la notation&nbsp;:

1. Une fonction `f` qui est appliquée aux arguments `a` et `b` sera notée `f(a, b)` comme par exemple `min(2, 5) = 5`.
2. Un opérateur `op` qui est appliqué aux arguments (opérandes) `a` et `b` sera noté `a op b` comme par exemple `5 - 2 = 3`.

Plus généralement, on définit des opérateurs unaires, qui s'appliquent à un argument, des opérateurs binaires, qui s'appliquent à deux arguments, et des opérateurs ternaires qui s'appliquent à trois arguments. Le nombre d'opérandes auxquels s'applique un opérateur s'appelle l'**arité** (_arity_).

> L'arité d'un opérateur est le nombre d'opérandes qu'il reçoit.

Il peut y avoir une ambiguité sur l'ordre dans lequel les opérateurs sont appliqués. Cette ambiguité est levée en attribuant une priorité aux opérateurs.

La façon dont Java interprète une expression avec opérateurs dépend des caractéristiques des opérateurs. Nous savons que l'arithmétique de base impose un ordre dans la résolution des opérations&nbsp;: l'expression 5&times;2+3 désigne la somme de 3 et du produit de 5 et 2, plutôt que le produit de 5 et de la somme de 2 et 3. Cet ordre est appelé **priorité** (_precedence_) des opérateurs.

> La priorité des opérateurs indique l'ordre relatif dans lequel ils doivent être résolus. C'est équivalent à l'ajout de paires de parenthèses dans l'expression. Les paires de parenthèses sont ajoutées autour des opérandes des opérateurs dans l'ordre décroissant de leur priorité.

Ainsi, 5&times;2+3 peut être réécrite (5&times;2)+3.

En Java, la priorité d'un opérateur est donnée par un nombre d'autant plus grand que la priorité est grande. Par exemple `*` a une priorité de 12 et `+` a une priorité de 11, donc dans l'expression Java `2 + 2 * 2 + 2 * 2`, les parenthèses sont ajoutées d'abord autour des 



L'autre notion importante est l'associativité&nbsp;:

> L'associativité des opérateurs désigne dans quel ordre des opérateurs de même priorité doivent être lus en l'absence de parenthèse.

Par exemple, l'expression -&thinsp;-&thinsp;-4 désigne l'opposé de l'opposé de l'opposé de 4. Dans votre tête, pour résoudre cette phrase, vous avez positionné des parenthèses ainsi&nbsp;: opposé de (opposé de (opposé de 4)). Dans l'expression cela revient à écrire -(-(-4)), qui devient -4. Dans cette exemple l'associativité est dite de droite à gauche.

> L'associativité à droite (_right to left_) désigne le fait que les opérations sont groupées à partir de la droite, comme dans cet exemple&nbsp;: 2&#x5E;3&#x5E;4&#x5E;5 = 2&#x5E;(3&#x5E;(4&#x5E;5)))

> L'associativité à gauche (_left to right_) désigne le faut que les opérations sont groupées à partir de la gauche, comme dans cet exemple&nbsp;: 2&times;3&divide;4&times;5 = ((2&times;3)&divide;4)&times;5

### Opérateurs arithmétiques

Les opérateurs arithmétiques s'appliquent aux variables numériques ou expressions.

| Symbole | Sens | Arité | Exemples | Priorité | Associativité |
|---------|------|-------|----------|----------|---------------|
| `++` (postfixe)     | Ajoute 1 à la variable.<br/>Renvoie la valeur avant incrément.    | 1      | `int a=0;a++;` a vaut 1<br/> `int a=0;int i=a++;` a vaut 1, i vaut 0        | 15         | Gauche |
| `--` (postfixe)     | Enlève 1 à la variable.<br/>Renvoie la valeur avant décrément.    | 1      | `int a=0;a--;` a vaut -1<br/> `int a=0;int i=a--;` a vaut -1, i vaut 0        | 15         | Gauche |
| `++` (préfixe)     | Ajoute 1 à la variable.<br/>Renvoie la valeur après incrément.    | 1      | `int a=0;++a;` a vaut 1<br/> `int a=0;int i=++a;` a vaut 1, i vaut 1        | 14         | Droite |
| `--` (préfixe)     | Enlève 1 à la variable.<br/>Renvoie la valeur après décrément.    | 1      | `int a=0;--a;` a vaut 1<br/> `int a=0;int i=--a;` a vaut -1, i vaut -1        | 14         | Droite |
| `+` (préfixe)     | Expression                | 1      | `int a=5;int b=+a;` a vaut 5, b vaut 5         | 14         | Droite |
| `-` (préfixe)     | Opposé de l'expression    | 1      | `int a=5;int b=-a;` a vaut 5, b vaut -5        | 14         | Droite |
| `*`              | Multiplie les opérandes   | 2      | `int a=5 * 3;` a vaut 15                         | 12         | Gauche |
| `/`              | Divise le premier opérande par le deuxième   | 2      | `int a =5/3;` a vaut 1<br/> `double a=5/3` a vaut 1<br/> `double a=5.0/3` a vaut 1.666…        | 12         | Gauche |
| `%`              | Reste de la division du premier opérande par le deuxième   | 2      | `int a =5%3;` a vaut 2<br/> `double a=5.1/3` a vaut 2.1                | 12         | Gauche |
| `+`              | Somme des deux opérandes   | 2      | `int a =5+3;` a vaut 8                | 11         | Gauche |
| `-`              | Soustrait le deuxième opérande au premier   | 2      | `int a =5-3;` a vaut 2                | 11         | Gauche |

### Opérateurs relationnels

Les opérateurs relationnels renvoient `true` si les deux opérandes vérifient la relation dénotée, et `false` sinon.

| Symbole | Sens | Arité | Exemples | Priorité | Associativité |
|---------|------|-------|----------|----------|---------------|
| `<`          | Infériorité stricte | 2 | `2 < 3` vaut `true`<br/> `2 < 2` vaut `false` | 9 | Gauche |
| `<=`         | Infériorité large   | 2 | `2 <= 3` vaut `true`<br/> `2 <= 2` vaut `true` <br/> `2 <= 1` vaut `false` | 9 | Gauche |
| `>`          | Supériorité stricte | 2 | `3 > 2` vaut `true`<br/> `2 > 2` vaut `false` | 9 | Gauche |
| `>=`         | Supériorité large   | 2 | `3 >= 2` vaut `true`<br/> `2 >= 2` vaut `true`<br/> `1 >= 2` vaut `false` | 9 | Gauche |
| `==`         | Égalité             | 2 | `3 == 2` renvoie `false`<br/> `2 == 2` vaut `true` | 8 | Gauche |
| `!=`         | Inégalité           | 2 | `3 != 2` renvoie `true`<br/> `2 != 2` vaut `false` | 8 | Gauche |

🧐 Que veut dire &laquo;&nbsp;`<` est associative à gauche&nbsp;?&nbsp;&raquo;.

### Les opérateurs d'assignation

Les opérateurs d'assignation renvoient la valeur de la variable (_left value_) après assignation.

| Symbole | Sens | Arité | Exemples | Priorité | Associativité |
|---------|------|-------|----------|----------|---------------|
| `=`         | Assignation           | 2 | `a = 2 * 3` vaut 6, a vaut 6 | 1 | Droite |
| `+=`        | `a+=b` équivaut à `a=a+b` | 2 | `int a=0;b=a+=2;` a vaut 2 et b vaut 2 | 1 | Droite |
| `-=`        | `a-=b` équivaut à `a=a-b` | 2 | `int a=2;b=a-=2;` a vaut 0 et b vaut 0 | 1 | Droite |
| `*=`        | `a*=b` équivaut à `a=a*b` | 2 | `int a=2;b=a*=3;` a vaut 6 et b vaut 6 | 1 | Droite |
| `/=`        | `a/=b` équivaut à `a=a/b` | 2 | `int a=4;b=a/=2;` a vaut 2 et b vaut 2 | 1 | Droite |
| `%=`        | `a%=b` équivaut à `a=a%b` | 2 | `int a=5;b=a%=2;` a vaut 1 et b vaut 1 | 1 | Droite |

### Les opérateurs booléens

| Symbole | Sens | Arité | Exemples | Priorité | Associativité |
|---------|------|-------|----------|----------|---------------|
| `!`  | Négation   | 1 | `!true` vaut `false`<br/> `! (a<b)` vaut `a>=b` | 13 | Droite |
| `&&` | Et logique | 2 | `true && true` vaut `true`<br/> `false && a` vaut `false` quelle que soit la valeur de `a` | 4  | Gauche |
| `\|\|` | Ou logique | 2 | `false \|\| false` vaut `false`<br/> `true \|\| a` vaut `true` quelle que soit la valeur de `a` | 3  | Gauche |
| `?:` | Si le premier opérande est vrai, renvoit le deuxième, sinon, renvoit le troisième | 3 | `2<3?2:3` vaut `2`<br/> `2>3?2:3` vaut `3` | 2  | Droite |

### L'opérateur de transtypage (_cast_)

| Symbole | Sens | Arité | Exemples | Priorité | Associativité |
|---------|------|-------|----------|----------|---------------|
| `(<type>)`  | Transtypage de l'opérande vers le type `<type>` | 1 | `(double) 1` est un `double` de valeur 1 | 13 | Droite |

Le type d'arrivée doit être compatible avec le type de départ&nbsp;:
```Java
int a = (int) 2.1; // OK, a vaut 1
int b = (int) 'a'; // OK, mais inutile, b vaut 97
boolean c = (boolean) 0; KO, impossible de convertir un entier en booléen
```

Les _cast_ vers les types plus larges sont implicites (ne nécessitent pas l'usage de l'opérateur)&nbsp;: `byte`&xrarr;`short`&xrarr;`char`&xrarr;`int`&xrarr;`long`&xrarr;`float`&xrarr;`double`.

&#x26A0; La conversion de variables de types `int` ou `long` en `float` peut résulter en l'obtention d'une valeur très différente de celle de départ (plusieurs dizaines de milliers).

🧐 Mais pourquoi&nbsp;?
<details>
<summary>Réponse</summary>
    
Prenons l'exemple de la conversion de `l`, de type `long`, vers `f` de type `float`&nbsp;:
```Java
long l = 123_456_789_012L;
float f = (float) l;
```

Ici, `f` vaut 123456790528 et non 123456789012. Alors pourquoi&nbsp;?

La réponse courte est &laquo;&nbsp;Parce que les `float` ont une précision de 6 ou 7 chiffres&nbsp;&raquo;.

Rentrons dans le détail. Un `float` est stocké sur 4 octets, soit 32 bits&nbsp;:
- 1 bit code le signe _s_.
- 8 bits codent l'exposant _x_.
- 23 bits codent la mantisse _m_.

La valeur du `float` vaut 2<sup>_x_−128</sup>(1−2_s_)(_m_+2<sup>24</sup>).

Dans notre example&nbsp;:
(123456798012)<sub>10</sub> = (0001 1100 1011 1110 1001 1001 0001 1010 0001 0100)<sub>2</sub>
Ce dernier nombre possède une **forme normalisée**, qui permet de calculer la façon dont le nombre sera stocké, et qui est&nbsp;:
2<sup>36</sup>&times;(1-2·0)&times;(1.1100 1011 1110 1001 1001 0001 1010 0001 0100)<sub>2</sub>.

Ce n'est pourtant pas ainsi que le nombre est stocké. Premièrement, à moins d'être nulle, la mantisse commence toujours par un 1, donc il est inutile de le stocker&nbsp;:

1.1100 1011 1110 1001 1001 0001 1010 0001 0100 &xrarr; 1100 1011 1110 1001 1001 0001 1010 0001 0100

Comme la mantisse est stockée sur 23 bits, le 1 qu'on vient d'enlever correspond donc à un 24-ième bit hypothétique, qui correspond au 2<sup>24</sup> de la formule 2<sup>_x_−128</sup>(1−2_s_)(_m_+2<sup>24</sup>).

La mantisse est stockée sur 23 bits, or celle fournie ici est stockée sur 36, je dois donc en tronquer 13&nbsp;:
1100 1011 1110 1001 1001 0001 1010 0001 0100 &xrarr; 1100 1011 1110 1001 1001 001<sup>*</sup>

<sup>*</sup> Pourquoi le 0 est devenu un 1&nbsp;? Pour la même raison que (1.09)<sub>10</sub> est tronqué en (1.1)<sub>10</sub>, (1.01)<sub>2</sub> est tronqué en (1.1)<sub>2</sub>.

Notre mantisse a été tronquée de ses 13 derniers chiffres, donc on devra la multiplier par 2<sup>13</sup>. L'exposant vaut donc 128 + 13 + 22 = 163. Dans la norme IEEE-754, l'entier `123 456 789 012` est donc représenté en binaire par `0 10100011 11001011111010011001001`.

Il n'était pas besoin d'aller jusqu'à la représentation binaire du nombre pour comprendre la raison de cette perte de précision&nbsp;: la mantisse ne peut coder que 24 bits significatifs (le premier étant un 1 implicite). En revanche vous savez maintenant à quel point la perte de précision peut affecter vos calculs.
</details>

Les _cast_ vers les types plus restreints doivent être explicités (avec l'opérateur de _cast_)&nbsp;: `double`&xrarr;`float`&xrarr;`long`&xrarr;`int`&xrarr;`char`&xrarr;`short`&xrarr;`byte`.

&#x26A0; Transtyper un nombre dans une type plus restreint peut conduire à des valeurs aberrantes.

🧐 Mais pourquoi&nbsp;?
<details>
<summary>Réponse</summary>

```Java
long a = 256L*256L*256L*256L*2L;
int b = (int) a; // b vaut 0
```

Nous savons déjà que les `int` de Java sont compris entre -2<sup>31</sup> (noté `10000000 00000000 00000000 00000000`) et 2<sup>31</sup>-1 (noté `01111111 11111111 11111111 11111111`).
    
Les `long` de Java sont compris entre -2<sup>63</sup> et 2<sup>63</sup>-1.
    
Dans notre exemple, `a` est codé `00000000 00000000 00000000 00000001 00000000 00000000 00000000 00000000`. Pour convertir cet entier dans un entier codé sur 4 octets, Java se contente de tronquer les 4 octets de poids fort, il reste donc `00000000 00000000 00000000 00000000`.
</details>

&#x26A0; On voit que `boolean` ne fait pas partie des types qui peuvent être transtypés, automatiquement ou de manière forcée, dans le langage Java.

🧐 Vous avez déclaré un booléen `a` dont la valeur dépend de circonstances que vous ne contrôlez pas (par exemple, `a` vaut `true` si la température du CPU est supérieure à 50 degrés). Vous aimeriez bien définir un entier `b` dont la valeur représente celle du booléen, comme en SQL où `TRUE` est converti en `1` et `FALSE` en `0`. Comment faire&nbsp;?

<details>
<summary>Réponse</summary>
    
```Java
int b = a ? 1 : 0;
```
</details>

🧐 Problème inverse&nbsp;: vous avez déclaré un entier `b` qui vaut `1` si la température du CPU est supérieure à 50 degrés. Vous aimeriez bien avoir un booléen `c` qui représente la valeur de `b` (qui vaut `true` si `b` vaut `1`…).

<details>
<summary>Réponse</summary>
    
```Java
boolean c = (b == 1); // Ces parenthèses ne sont pas nécessaires sauf pour la lisibilité
```
</details>

### Vue d'ensemble de tous les opérateurs Java

| Niveau de priorité | Opérateur | Type | Associativité |
|--------------------|-----------|------|---------------|
| 15 | `()`<br/>`[]`<br/>`·` | Parenthèses<br/>Sélection d'élément dans un tableau<br/>Sélection de membre | Gauche |
| 14 | `++`<br/>`--` | Incrément suffixe<br/>Décrément suffixe | Droite |
| 13 | `++`<br/>`--`<br/>`+`<br/>`-`<br/>`!`<br/>`~`<br/>`(type)` | Pré incrément unaire<br/>Pré décrément unaire<br/>Plus unaire<br/>Opposé<br/>Négation logique<br/>Complément bit à bit<br/>Transtypage | Droite |
| 12 | `*`<br/>`/`<br/>`%` | Multiplication<br/>Division<br/>Modulus | Gauche |
| 11 | `+`<br/>`-` | Addition<br/>Subtraction | Gauche |
| 10 | `<<`<br/>`>>`<br/>`>>>` | Décalage vers la gauche bit à bit<br/>Décalage vers la droite bit à bit avec conservation du signe<br/>Décalage vers la droite avec remplissage par des 0 | Gauche |
| 9	 | `<`<br/>`<=`<br/>`>`<br/>`>=`<br/>`instanceof` | Inférieur strict<br/>Inférieur ou égal<br/>Supérieur strict<br/>Supérieur ou égal<br/>Comparaison des types d'objet | Gauche |
| 8	 | `==`<br/>`!=` | Égalité<br/>Inégalité | Gauche |
| 7	 | `&` | ET bit à bit | Gauche |
| 6	 | `^` | OU exclusif bit à bit | Gauche |
| 5	 | `\|` | OU bit à bit | Gauche |
| 4	 | `&&` | ET logique | Gauche |
| 3	 | `\|\|` | OU logique | Gauche |
| 2	 | `? :` | Opérateur conditionnel ternaire | Droite |
| 1	 | `=`<br/>`+=`<br/>`-=`<br/>`*=`<br/>`/=`<br/>`%=` | Assignation<br/>Assignation additive<br/>Assignation soustractive<br/>Assignation multiplicative<br/>Assignation &laquo;&nbsp;divisive&nbsp;&raquo;<br/>Assignation modulo<br/> | Droite |

## Structures de contrôle _(Control flow statements)_

Nous l'avons vu dans la partie présentant la notion d'algorithme&nbsp;: le langage natif fonctionne à l'aide d'étiquettes et de branchements&nbsp;:
```
00 A = 5
01 B = A + 2
02 COMPARER A ET B
03 SUPERIEUR : 
04   ALLER EN 09
05 INFERIEUR :
06   ALLER EN 11
07 EGAL :
08   ALLER EN 13
09 ECRIRE "A est plus grand que B"
10 ALLER EN 14
11 ECRIRE "A est plus petit que B"
12 ALLER EN 14
13 ECRIRE "A est égal à B"
14 % Commentaire qui ne sert à rien
```

> Un **branchement** est une instruction qui permet de se déplacer au sein du code.

🧐 Ce code permet de faire une simple comparaison et d'afficher le résultat. Comment écririez-vous, en utilisant uniquement les mots clés et la syntaxe de l'exemple ci-dessus, un code qui écrit tous les nombres pairs entre 1 et 100&nbsp;?
<details>
<summary>Réponse</summary>

```
00 A = 0
01 B = 100
02 A = A + 1 % Ajouter 1 à A
03 COMPARER A et B 
04 INFERIEUR :
05   ALLER EN 08
06 EGAL :
07   ALLER EN 18
08 C = A MODULO 2
09 COMPARER C ET 0
10 EGAL :
11   ALLER EN 14
12 SUPERIEUR :
13   ALLER EN 02
14 ECRIRE A
15 ECRIRE " est pair\n"
17 ALLER EN 02
18 % Fin du programme
```
C'est difficile à écrire (il faut réfléchir aux branchements) et c'est illisible.
</details>

🧐 Quels sont les gros inconvénients d'un langage de programmation dont le contrôle du flot d'instructions est fait au moyen de branchements&nbsp;?
<details>
<summary>Réponse</summary>

1. C'est illisible parce que très verbeux. La mémoire de travail des humains n'est pas adaptée à la lecture de ce genre de code. Quand vous lisez la ligne 12, est-ce que vous vous rappelez comment vous êtes arrivé·e là&nbsp;?
2. C'est très difficile à débugger car l'état du système (valeurs des variables) peut être modifié dans des portions de code très distantes de la portion que vous lisez. La logique du flot d'instruction n'est pas apparente.
3. Vous devrez écrire beaucoup de fois la même chose à quelques variations près. Donc vous perdrez du temps à écrire un code redondant et à reconnaître le rôle de portions de codes qui se ressemblent, mais sont différentes.
4. Toute modification du code devant résoudre un bug ou bien faire évoluer une fonctionnalité nécessite la réécriture de larges portions du code, ce qui est source d'erreurs.

Bref, c'est horrible et les gens qui programmaient en assembleur sont des héros.
</details>

Pour ces raisons, les branchements sont interdits et ont été remplacés par des structures de contrôle du flot d'instructions, ce qui a donné naissance à la programmation structurée.

En Java, elles sont peu nombreuses mais omniprésentes.

### Structures conditionnelles `if ...`

Elles sont les équivalents programmatiques de `SI ... ALORS ... SINON ...` et ses variantes.

#### `if` monoligne

```Java
if (<booleanExpression>)
    <codeIfTrue>

if (a <= b)
    System.out.println(a + " <= " + b);
```

`<booleanExpression>` est ici une expression dont la valeur est booléenne, comme par exemple `5 <= 3`. `<codeIfTrue>` est une instruction (donc atomique).

&#x26A0; Dans cette version du `if`, `<codeIfTrue>` est une instruction unique. Pour exécuter plusieurs instructions à la suite d'un `if`, vous devez les encadrer dans des accolades&nbsp;:

```Java
int a = 5;
int b = 1;
if (a <= b)
    System.out.println(a + " <= " + b);
System.out.println("Je m'affiche même si a > b");
```

Un petit mot sur l'**indentation**…

> L'indentation c'est la disposition du code qui fait apparaître des décalages au niveau de la marge.

Elle ne sert pas au compilateur. Le code précédent aurait tout aussi bien pu être écrit ainsi&nbsp;:
```Java
int a = 5;int b = 1;if (a <= b)System.out.println(a + " <= " + b);System.out.println("Je m'affiche même si a > b");
```

Java s'appuie sur les `;` pour décider qu'une déclaration prend fin. Ainsi le code suivant compile&nbsp;:
```Java
int
a
=
5
;
```
Les caractères espace (espace, tabulation, saut de ligne) n'ont de sens que pour séparer deux groupes de caractères qui pourraient désigner des identifiants ou des mots-clés&nbsp;: par exemple `int i = 0;` n'a pas le même sens que `inti = 0;`. En revanche, dans `if (a<5) System.out.println("a est petit");` l'espace entre `if` et la suite, ou bien entre `(a<5)` et la suite, n'ont pas d'intérêt syntaxique.

#### `if` multiligne

```Java
if (<booleanExpression>) {
    <codeIfTrue>
}

if (a <= b) {
    System.out.println(a + " <= " + b);
    System.out.println(a + " est plus petit que " + b);
}
```

Dans cette version du `if`, `<codeIfTrue>` est un nombre arbitraire d'instructions qui seront toutes exécutées.

Dans les faits, je trouve le `if` multiligne plus propre, parce que les parenthèses rendent la structure plus lisible et plus facile à modifier.

#### `if ... else ...`

```Java
if (<booleanExpression>)
    <codeIfTrue>
else
    <codeIfFalse>

if (a <= b)
    System.out.println("a <= b");
else
    System.out.println("a > b");
```

Cette version est hideuse et ne devrait pas être utilisée. Préférez la version multiligne&nbsp;:

```Java
if (<booleanExpression>) {
    <codeIfTrue>
} else {
    <codeIfFalse>
}

if (a <= b) {
    System.out.println("a <= b");
} else {
    System.out.println("a > b");
}
```

Dans les deux versions, `<codeIfTrue` est exécuté si l'expression booléenne `<booleanExpression>` est évaluée à `true`. Sinon, c'est `<codeIfFalse>` qui est exécuté.

#### `if ... else if ... [else ...]`

Cette version permet de gérer un nombre quelconque de conditions booléennes et d'exécuter un code spécifique pour chacune d'elle. Je ne présente plus la version monoligne, mais elle existe.

```Java
if (booleanExpression1) {
    <codeIfTrue1>
} else if (<booleanExpression2) {
    <codeIfTrue2>
}

if (a < b) {
    System.out.println(a + " < " + b);
} else if (a > b) {
    System.out.println(a + " > " + b);
}
// Si a == b rien ne se passe
```

Il est possible&nbsp;:

1. D'ajouter autant de contrôles `else if (<booleanExpression>) { ... }` que souhaité.
2. D'ajouter un `else` qui sera exécuté si aucun `else if` précédent ne l'a été.

```Java
if (booleanExpression1) {
    <codeIfTrue1>
} else if (<booleanExpression2) {
    <codeIfTrue2>
} else {
    <executedIfNoConditionMatched>
}

if (a < b) {
    System.out.println(a + " < " + b);
} else if (a > b) {
    System.out.println(a + " > " + b);
} else {
    System.out.println(a + " == " + b);
}
```

### Un `if` spécifique&nbsp;: l'instruction `switch`

Parfois on souhaite exécuter une instruction différente pour chaque valeur possible d'une expression&nbsp;:

```Java
int choice = getChoice();
if (choice == 0) {
    System.out.println("Vous avez choisi les légumes !");
} else if (choice == 1) {
    System.out.println("Vous avez choisi les fruits !");
} else if ... {
    ...
} else {
    System.out.println("Vous avez choisi rien !");
}
```

Si l'expression est d'un type simple (`byte`, `short`, `int`, `Enum` ou `String`) il existe une manière simple et structurée d'obtenir le même résultat&nbsp;:

```Java
switch (<expression>) {
    case <value1>: 
        // code if value1
    case <value2>:
        // code if value2
    default:
        // code always executed

   int a = 0;
   switch (2*a+1) {
        case 0: System.out.println("0");
        case 1: System.out.println("1");
        default: System.out.println("Is also printed");
    }
}
```

Parfois il est souhaitable d'interrompre le parcours du bloc, en particulier pour n'exécuter la condition `default` que dans les cas où aucun `case` n'a été exécuté. Dans ce cas, insérez l'instruction `break` à la fin de chaque `case`. L'instruction `break` permet de sortir de la structure de contrôle dans laquelle elle se trouve.

```Java
switch (<expression>) {
    case <value1>: 
        // code if value1
        break;
    case <value2>:
        // code if value2
        break;
    default:
        // code always executed

   int a = 0;
   switch (2*a+1) {
        case 0: System.out.println("0");break;
        case 1: System.out.println("1");break;
        default: System.out.println("Is NOT printed");
    }
}
```

### Les boucles `for`

La plupart du temps, un programme doit traiter des collections d'objets, sous une forme ou sous une autre. La boucle `for` permet d'itérer sur des collections.

```Java
for (int|long <var name> = <begin value>; <condition> ; <instruction>) {
    <some code>
}

for (int i = 0; i < 5; i++) {
    System.out.println("i = " + i);
}
```
`int|long` est le type de la variable qui varie, et vaut soit `int`, soit `long`. `<var name> = <begin value>` est l'initialisation de la variable. Tant que `<condition>` est respectée, `<some code>` est exécuté puis `<instruction>` est exécutée. `<instruction>` doit être une assignation ou être vide, et en général on lui réserve le rôle le rôle de modifier la valeur de `<var name>`, et `<condition>` est généralement une condition portant sur `<var name>`.

Le code de l'exemple se lit &laquo;&nbsp;pour `i` allant de 0 à 5, par pas de 1, écrire dans la console ... &nbsp;&raquo;

Il est possible d'utiliser la boucle `for` de multiples manières&nbsp;:

```Java
for (int i = 5; i > 0; i--) {
    System.out.println("i = " + i);
}

for (int i = 27; i > 1; i = i % 2 == 0 ? i/2 : 3*i + 1) {
    System.out.println("i = " + i);
}

for (int i = 0; i == 0;) {
    System.out.println("It'll be going on for a while");
}
```
🧐 Que font ces trois blocs&nbsp;?

<details>
<summary>Réponse</summary>
Le premier décompte de 5 à 1.

Le deuxième&nbsp;:
1. Vérifie si `i` est plus grand que 1.
2. Affiche la valeur de `i` si c'est le cas.
3. Change la valeur de `i`&nbsp;: si `i` est pair, il est divisé par 2, sinon, il est multiplié par 3.

Le troisième affiche en continu `"It'll be going on for a while"`.
</details>

Il est également possible d'itérer sur des tableaux et des collections. La syntaxe est la suivante&nbsp;:
```Java
for (<Type> <varName>:<array or collection>) {
    <code>
}

String[] jours= {"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"};
        
for (String j:jours) {
    System.out.println(j);
}
```

### Boucles `while`

La boucle `while` répond au besoin&nbsp;: tant qu'une condition est vérifiée, exécuter des traitements. La syntaxe est la suivante&nbsp;:

```Java
while (<condition>) {
    <code>
}

int i=1;
while (i % 17 > 0) {
    i += 2;
}
// here, i % 17 == 0
```

La condition est vérifiée _avant_ l'exécution du code. Il est donc possible pour un bloc `while` de ne jamais exécuter le code qu'il contient.

🧐 Imaginons que vous souhaitiez inverser la vérification de la condition et l'exécution du code. Par exemple, vous souhaitez, avec une boucle while (c'est la seule que vous avez le droit d'utiliser dans cette partie), ajouter 2 à i jusqu'à ce que `i % 17 == 0`. Comment feriez-vous&nbsp;?

<details>
<summary>Réponse</summary>

```Java
int i=1;
boolean cond = true;
while (cond) {
    i +=2;
    cond = (i % 17 > 0);
}
```
</details>

C'est astucieux, mais pas extrêmement lisible. For heureusement, Java nous propose la syntaxe `do ... while ...` qui fait exactement la même chose mais de manière plus lisible&nbsp;:

```Java
do {
    <code>
} while (<cond>)

int i=1;
do {
    i +=2;
} while (i % 17 > 0);
```

Est-ce qu'il existe des cas (des valeurs initiales de la variable `i`) pour lesquels les deux boucles (`while` standard et `do ... while`) donnent un résultat différent (une valeur de `i` différente après le passage dans la boucle)&nbsp;?

<details>
<summary>Réponse</summary>

Oui&nbsp;! Essayons les deux boucles avec i = 17.
```Java
int i=17;
while (i % 17 > 0) { // Condition not verified, early exit
    i += 2;
}
// i == 17
```

```Java
int i=17;
do {
    i +=2;
} while (i % 17 > 0); // i == 19 at first iteration
// i = 51
```
</details>

### Les instructions `break` et `continue`

#### `break`

Nous avons vu dans la partie sur l'instruction `switch` que `break` permettait d'en sortir. Situé à l'intérieur d'un bloc `while` ou `for`, l'instruction `break` permet de sortir prématurément du bloc&nbsp;:

```Java
while (<cond>) {
    <do stuff>
    if (<alternative cond>) {
        break;
    }
    <do other stuff>
}

int i=20;
while (i % 17 > 0) {
    if (i % 3 == 0) {
        break;
    }
    i += 2;
}
// here, i is 24
```

Ce dernier exemple illustre le fonctionnement de `break` mais il peut être réécrit sans utiliser l'instruction `break` et de manière beaucoup plus lisible. Comment&nbsp;?

<details>
<summary>Réponse</summary>

```Java
int i=20;
while ((i % 17 > 0) && (i % 3 > 0)) {
    i += 2;
}
// here, i is 24
```
</details>

Mais alors, quel est l'intérêt de l'instruction `break` dans la boucle `while`&nbsp;?

<details>
<summary>Réponse</summary>
Dans une boucle while la condition est vérifiée avant de faire les traitements, alors qu'une instruction `break` peut être placée n'importe où dans le code de la boucle `while`.
</details>

Personnellement, je recommande chaudement d'éviter l'instruction `break` mais de savoir que ça existe.

#### `continue`

L'instruction `continue` s'utilise comme l'instruction `break`, et permet de passer directement à l'itération suivante&nbsp;:
```Java
for (<...>) {
    <code always executed>
    if (<cond>) {
        continue;
    }
    <code executed only if not cond>
}

for (int i = 0; i < 10; i++) {
    if (i % 2 == 0) {
        continue;
    }
    System.out.println(i);// only executed when i == 1, 3, 5, 7, 9
}
```

Là encore, je pense que l'instruction `continue` est une mauvaise pratique, à éviter autant que possible.

## Fonctions

### Motivation et présentation

Les structures de contrôle réduisent le nombre de lignes nécessaire pour écrire certains bouts de codes qui, sous une forme ou sous une autre, reviennent très souvent. Mais il y a encore mieux à faire&nbsp;: éviter d'avoir à réécrire des bouts de code identiques à quelques valeurs de variables près. Cet enjeu est crucial&nbsp;: un projet comme Mozila Firefox comporte 21 millions de lignes (en comparaison, un projet Insee fait entre 10 000 et 200 000 lignes). Si un tel code ne permet pas d'identifier clairement le but et le contexte de chacune de ses parties, autant le jeter à la poubelle immédiatement.

La notion de fonction répond à cet impératif&nbsp;: proposer un bout de code qui puisse être appelé de n'importe où, dans un but précis, avec des paramètres.

Prenons un exemple&nbsp;: l'algorithme de Collatz.
```Java
int i = 3;
int steps = 0;
while (i != 1) {
    steps++;
    if (i % 2 == 0) {
        i /= 2;
    } else {
        i = 3 * i + 1;
    }
}
// i = 3
// i = 10
// i = 5
// i = 16
// i = 8
// i = 4
// i = 2
// i = 1
// steps = 7
```
Vous souhaitez avoir un tableau qui donne, pour chaque entier entre 1 et 100 000, le nombre d'étapes (la valeur de la variable `steps` dans notre exemple) nécessaire pour atteindre 1. Par exemple à l'index `i` de notre tableau nous trouvons le nombre d'étapes requis pour que `i` atteigne 1 en appliquant l'algorithme de Collatz.

C'est assez facile&nbsp;:

```Java
int[] numberOfStepsRequired = new int[100_000];

for (int i = 0; i < numberOfStepsRequired.length; i++)
{
    int numberForCollatz = i + 1;
    numberOfStepsRequired[i] = 0;
    while (numberForCollatz != 1)
    {
        numberOfStepsRequired[i] = numberOfStepsRequired[i] + 1;
        if (numberForCollatz % 2 == 0)
        {
            numberForCollatz /= 2;
        } else
        {
            numberForCollatz = 3 * numberForCollatz + 1;
        }
    }
}

for (int i = 0; i < numberOfStepsRequired.length; i++)
{
    System.out.println(numberOfStepsRequired[i]);
}
```

C'est facile, mais ça n'est pas très lisible. Essentiellement ce code fait trois choses&nbsp;:

1. Il sait appliquer l'algorithme de Collatz à un nombre.
2. Il le fait sur les nombres compris entre 1 et 100_000.
3. Il stocke le résultat (nombre d'étapes) pour chaque nombre, dans un tableau.

On pourrait le réécrire de manière algorithmique comme ceci&nbsp;:
```
N = 100 000
TABLEAU = un tableau avec N cases
Pour tous les nombres ENTIER entre 1 et N
    NOMBRE_ETAPE = ETAPE_COLLATZ(ENTIER)
    TABLEAU[ENTIER] = NOMBRE_ETAPE
```

La seule chose qui nous manque pour le faire en Java c'est la fonction. Certaines fonctions se contentent de faire un traitement sans renvoyer de valeur&nbsp;:

```Java
public static void <function name>(<args>) {
    <code>
}

public static void estPair(int i) {
    if (i % 2 == 0) {
        System.out.println(i + " est pair !");
    } else {
        System.out.println(i + " est impair !");
    }
}
```

D'autres fonctions font un traitement et renvoient une valeur, réutilisable dans la partie du programme qui a appelé la fonction&nbsp;:
```Java
public static <return type> <function name>(<args>) {
    <code>
    return <value>;
}

public static boolean estPair(int i) {
    return (i % 2 == 0);
}
```

On pourrait réécrire le code de notre exemple de cette manière&nbsp;:

```Java
public static int collatzSteps(int number)
{
    int returned = 0;
    while (number != 1)
    {
        returned++;
        if (number % 2 == 0)
        {
            number /= 2;
        } else
        {
            number = 3 * number + 1;
        }
    }
    return returned;
}

public static int[] collatzForNumberBetween(int startIncluded, int endExcluded)
{
    int[] numberOfStepsRequired = new int[endExcluded - startIncluded];

    for (int i = 0; i < numberOfStepsRequired.length; i++)
    {
        System.out.println("Testing " + (i + startIncluded));
        numberOfStepsRequired[i] = collatzSteps(i + startIncluded);
    }
    return numberOfStepsRequired;
}

public static final void main(String[] args)
{

    int start = 100;
    int end = 1000;
    int[] numberOfStepsRequired = collatzForNumberBetween(start, end);
    for (int i = 0; i < numberOfStepsRequired.length; i++)
    {
        System.out.println((i + start) + " -> " + numberOfStepsRequired[i]);
    }
}
```

La seule partie du code qui doit changer, suivant les besoins, est la valorisation de `start` et `end` dans la fonction `main`.

### Aller plus loin sur les fonctions

#### Éléments caractéristiques&nbsp;: la **signature**

Java autorise que plusieurs fonctions aient le même nom. Pour savoir quelle fonction est utilisée dans un contexte, Java s'appuie sur le nom de la fonction, et sur le type et le nombre de ses arguments (ces trois éléments sont appelés **signature** de la fonction)&nbsp;:
```Java
public static int someFunction() { ... } // Error

public static void someFunction() { ... } // Error

public static int someOtherFunction() { ... }

public static int someOtherFunction(int a) { ... }

public static void someOtherFunction(String s) { ... }
```

Les deux fonctions `someFunction` ont ici la même signature, donc elles ne peuvent pas se trouver dans la même classe (sans quoi Java ne sait pas les distinguer). En revanche, lors d'un appel à `someOtherFunction`, Java (et vous-même) pouvez savoir de quelle fonction il est question&nbsp;:
```Java
someOtherFunction(4); // number 2

someOtherFunction("4"); // number 3

someOtherFunction(); // number 1
```

#### Passage des paramètres

Observons la fonction codée pour notre exemple sur l'algorithme de Collatz&nbsp;:
```Java
public static int collatzSteps(int number)
{
    int returned = 0;
    while (number != 1)
    {
        returned++;
        if (number % 2 == 0)
        {
            number /= 2;
        } else
        {
            number = 3 * number + 1;
        }
    }
    return returned;
}
```

Cette fonction prend un argument (`number` de type `int`), et en lignes 4, 6, 8 et 11 le modifie. On pourrait donc s'attendre à ce que le programme ne fonctionne pas&nbsp;:
```Java
int i = 4;
int steps = collatzSteps(i); // modifies the argument many times!
System.out.println(i); // prints 4, but why?
```

En fait, Java recopie les valeurs des types primitifs passées aux fonctions. Dans notre exemple, lors de l'appel à `collatzSteps(i)`, Java fournit une copie de `i` à la fonction.

Mais&nbsp! La réalité est plus compliquée que cela. Voyons le bout de code suivant&nbsp;:
```Java
public static void someFunction(int[] array) {
    array[0] = 1;
}

public static void someOtherFunction(int[] array) {
    array = new int[100];
}

public static final void main(String[] args)
{
    int[] array = new int[10];
    someFunction(array);
    System.out.println(array[0]); // prints 1, not 0
    someOtherFunction(array);
    System.out.println(array[0]); // prints 1, not 0
}
```

La raison à ce comportement étrange est que Java comporte deux sortes de types&nbsp;: les types primitifs (`short`, … , `double`) et les types par références. Les types primitifs sont stockés par Java en tant que valeurs. Ils ne prennent pas beaucoup de place mémoire, donc les dupliquer pour les passer à une fonction ne coûte pas cher. En revanche, un tableau ou une chaîne de caractères peut occuper plusieurs kilo-octets, voire méga-octets. Une fonction appelée plusieurs milliers de fois dans une simple exécution du programme ne peut dupliquer l'intégralité de ses arguments, sinon notre tableau de un Mo dupliqué 1000 fois nécessiterait l'écriture en mémoire d'un Go.

Pour résoudre ce problème Java passe les variables d'autres types sous forme de référence, c'est-à-dire un nom et un emplacement mémoire. On peut représenter ainsi la façon dont Java gère les types primitifs et par référence&nbsp;:

![alt text](module-beginners-algorithmics/img/value-vs-reference.svg "Valeurs et références")

Au lieu de dupliquer l'objet avant de le passer à un fonction, Java se contente de dupliquer la référence (nom différent, même adresse)&nbsp;:

![alt text](module-beginners-algorithmics/img/fonction-invokation.svg "Copie des références à l'invocation d'une fonction")

Lorsque le tableau est accédé, c'est en réalité la valeur à l'adresse sous-jacente qui est accédée&nbsp;:

![alt text](module-beginners-algorithmics/img/fonction-side-effect.svg "Accès à un objet passé en argument d'une fonction")

Enfin, lorsqu'une nouvelle valeur est assignée au tableau, plusieurs choses se passent&nbsp;:

![alt text](module-beginners-algorithmics/img/fonction-assignment.svg "Assignation d'une nouvelle valeur à un objet passé en argument d'une fonction")

D'abord, une nouvelle valeur est créée (en right-value de l'assignation). Cette valeur est d'un type non primitif, donc c'est sa référence qui est assignée à la variable.

&#x26A0; Ce fonctionnement est assez complexe, c'est pourquoi il ne faut jamais assigner de valeur à un argument passé en paramètre d'une fonction. Mieux vaut le dupliquer comme dans cet exemple&nbsp;:

```Java
public static int collatzSteps(int n)
{
    int returned = 0;
    int number = n;
    while (number != 1)
    {
        returned++;
        if (number % 2 == 0)
        {
            number /= 2;
        } else
        {
            number = 3 * number + 1;
        }
    }
    return returned;
}
```

Cette duplication n'est pas faite pour des raisons de compilation, mais bien pour des raisons de lisibilité et de sécurité.

## Pour un code de qualité

### Nommage des variables

### Conventions stylistiques

### Découpage et factorisation du code

## Pour aller plus loin…

[www.codewars.com](www.codewars.com) propose des petits exercices dont la difficulté est adaptable, pour (presque) tous les langages&nbsp;!

## Exercices

### Exercice 1&nbsp;: afficher les éléments négatifs d'un tableau

```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    // compléter le code pour afficher tous les éléments négatifs du tableau array
}
```

<details>
<summary>Réponse</summary>

```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    for (int i: array) {
        if (i < 0) {
            System.out.println(i);
        }
    }   
}
```
</details>

### Exercice 2&nbsp;: trouver le plus petit élément

```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    // compléter le code pour afficher le plus petit élément du tableau array
}
```

<details>
<summary>Réponse</summary>

Solution 1&nbsp;: le minimum est initialisé avec la plus grande valeur possible pour un entier.
Intérêt&nbsp;: ne plante jamais, même si le tableau est vide.
Inconvénient&nbsp;: un test supplémentaire est requis si la valeur 0x7fffffff est renvoyée par la fonction.

```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    int min = 0x7fffffff;
    for (int i:array) {
        if (i < min) {
            min = i;
        }
    }
}
```

Solution 2&nbsp;: le minimum est initialisé avec le premier élément du tableau.
Intérêt&nbsp;: plante si le tableau est vide (donc pas besoin de tester ce cas).
Inconvénient&nbsp;: plante si le tableau est vide (donc gestion d'exception).

```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    int min = array[0];
    for (int i=1; i < array.length; i++) {
        if (min < array[i]) {
            min = array[i];
        }
    }
}
```
</details>

### Exercice 3&nbsp;: inverser l'ordre des éléments d'un tableau

```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    // Créer le tableau avec les éléments dans le sens inverse
}
```

<details>
<summary>Réponse</summary>

```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    int n = array.length;
    int[] returned = new int[n];
    for (int i = 0 ; i < n ; i++) {
        returned[i] = array[n - 1 - i];
    }
    for (int i : returned) {
        System.out.println(i);
    }
}
```
</details>
    

### Exercice 4&nbsp;: afficher les éléments du tableau sans boucle for

```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    // compléter le code pour afficher les éléments du tableau sans boucle for
}
```

<details>
<summary>Réponse</summary>

Solution 1&nbsp;:
```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    int i = 0;
    while (i < array.length) {
        System.out.println(array[i++]);
    }
}
```

Solution 2&nbsp;:
```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    int j=0;
    do {
        System.out.println(array[j++]);
    } while (j < array.length);
}
```
</details>

### Exercice 5&nbsp;: afficher les deux plus petits élements du tableau

```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    // compléter le code pour afficher les deux plus petits élements du tableau
}
```

<details>
<summary>Réponse</summary>

L'idée ici est de toujours conserver les valeurs minimum dans `min1` et `min2` tels que `min1 <= min2`. Ayant valorisé `min1 = array[0]` et `min2 = array[1]`, (et après éventuelle permutation des deux valeurs pour respecter l'ordre), chaque nouvelle valeur du tableau est comparée à `min2`. Puis, si `min2 < min1`, ces deux valeurs sont permutées.

Ce n'est pas la façon la plus simple, mais elle réutilise des portions de code déjà écrites.
```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    int min1 = array[0];
    int min2 = array[1];
    if (min1 > min2) {
        int min3 = min2;
        min2 = min1;
        min1 = min3;
    }
    for (int i = 2; i < array.length; i++) {
        int min3 = array[i];
        if (min3 < min2) {
            min2 = min3;
        }
        if (min1 > min2) {
            int min4 = min2;
            min2 = min1;
            min1 = min4;
        }
    }
    System.out.println(min1);
    System.out.println(min2);    
}
```
</details>


### Exercice 6&nbsp;: trier un tableau

```Java
public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    // compléter le code pour trier le tableau
}
```

<details>
<summary>Réponse</summary>

L'idée est de repérer le plus petit élément du tableau puis de le positionner en `array[0]` (donc d'échanger sa place avec le premier élément du tableau).

Puis, de repérer le deuxième plus petit élément du tableau (donc le plus petit élément du tableau à partir de l'index 1) et de l'échanger avec le deuxième élément du tableau.

Et ainsi de suite.

Nous devons donc, pour tous les entiers `i` entre 0 et (array.length - 1) - 1&nbsp;:
1. Trouver le plus petit élement du sous-tableau `array[i:(array.length-1)]` (fonction `minIndex`).
2. L'échanger avec l'élément `array[i]` (fonction `swap`).

```Java
public static int minIndex(int[] array, int startIndex) {
    int minIndex = startIndex;
    for (int i = startIndex; i < array.length; i++) {
        if (array[i] < array[minIndex]) {
            minIndex = i;
        }
    }
    return minIndex;
}

public static void swap(int[] array, int index1, int index2) {
    int temp = array[index1];
    array[index1] = array[index2];
    array[index2] = temp;
}

public static void main(String[] args) {
    int[] array = {-1, 5, -12, 89, 58, -13, 6};
    for (int i = 0; i < array.length; i++) {
        int index = minIndex(array, i);
        swap(array, index, i);
    }    
}
```
</details>


&laquo;&nbsp;&nbsp;&raquo;
🧐
&#x26A0;
